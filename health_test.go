// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader_test

import (
	"log"
	"strings"
	"testing"

	. "github.com/antichris/go-feedreader"
	"github.com/stretchr/testify/require"
)

func Test_healthsrvc_Show(t *testing.T) {
	t.Parallel()
	b := &strings.Builder{}

	svc := NewHealth(log.New(b, "", 0))
	err := svc.Show(ctx)

	require.NoError(t, err)
}
