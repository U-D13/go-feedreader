// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//go:build wireinject

//go:generate go run github.com/google/wire/cmd/wire gen -header_file=internal/_resources/license-header.go
//go:generate go fix -fix buildtag .
//go:generate go run mvdan.cc/gofumpt -w wire_gen.go

package feedreader

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/feed"
	gfeed "github.com/antichris/go-feedreader/goa/gen/feed"
	gtoken "github.com/antichris/go-feedreader/goa/gen/token"
	guser "github.com/antichris/go-feedreader/goa/gen/user"
	"github.com/antichris/go-feedreader/mail"
	"github.com/antichris/go-feedreader/token"
	"github.com/antichris/go-feedreader/user"
	"github.com/antichris/go-feedreader/user/registration"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/google/wire"
)

// NewFeed returns the Feed service implementation.
func NewFeed(
	*log.Logger,
	auth.Service,
) (gfeed.Service, func()) {
	panic(wire.Build(
		feedSet,
	))
}

// NewToken returns the Token service implementation.
func NewToken(
	*log.Logger,
	auth.Service,
	token.Service,
	user.Storage,
) gtoken.Service {
	panic(wire.Build(
		tokenSet,
	))
}

// NewUser returns the User service implementation.
func NewUser(
	*log.Logger,
	auth.Service,
	user.TxableStorage,
	registration.Mailer,
) guser.Service {
	panic(wire.Build(
		userSet,
	))
}

func BuildServices(
	*log.Logger,
	sqlc.Driver,
	sqlc.TxableDB,
	token.Key,
	registration.FrontendURL,
	mail.Mailer,
) (Services, func(), error) {
	panic(wire.Build(
		wire.Struct(new(Services), "*"),
		wire.Bind(new(user.Storage), new(user.TxableStorage)),
		feedSet,
		tokenSet,
		userSet,
		auth.New,
		token.New,
		sqlc.NewTxable,
		registration.NewMailer,
	))
}

type Services struct {
	Feed  gfeed.Service
	Token gtoken.Service
	User  guser.Service
}

var (
	feedSet = wire.NewSet(
		wire.Bind(new(gfeed.Service), new(*feedsrvc)),
		wire.Struct(new(feedsrvc), "*"),
		feed.New,
		provideDefaultFileURLSource,
	)
	tokenSet = wire.NewSet(
		wire.Bind(new(gtoken.Service), new(*tokensrvc)),
		wire.Struct(new(tokensrvc), "*"),
		wire.Value(badCredsDelay),
	)
	userSet = wire.NewSet(
		wire.Bind(new(guser.Service), new(*usersrvc)),
		wire.Struct(new(usersrvc), "*"),
		provideEmailTokenGenerator,
	)
)

func provideDefaultFileURLSource() URLSource {
	return func(ctx context.Context) string {
		return getFeedURL(os.DirFS("."), feedURLFile)
	}
}

const badCredsDelay = 5 * time.Second

func provideEmailTokenGenerator() mailTokenGenerator {
	return readerMailTokenGen{token.MailTokenReader}
}
