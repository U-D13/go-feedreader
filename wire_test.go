// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader_test

import (
	"context"
	"io"
	"log"
	"sync"
	"testing"

	. "github.com/antichris/go-feedreader"
	"github.com/antichris/go-feedreader/goa/gen/feed"
	"github.com/antichris/go-feedreader/user/registration"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestBuildServices(t *testing.T) {
	t.Parallel()
	l := log.New(io.Discard, "", 0)
	registerMockAdapter(gomock.NewController(t))

	for n, tt := range map[string]struct {
		d    sqlc.Driver
		url  registration.FrontendURL
		wErr string
	}{"unrecognized driver": {
		d:    "foo",
		wErr: `unrecognized DB driver: "foo"`,
	}, "bad FrontendURL": {
		d:    dbDriver,
		url:  ":",
		wErr: `parse ":": missing protocol scheme`,
	}, "mock driver": {
		d: dbDriver,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got, cl, err := BuildServices(
				l, tt.d, sqlc.TxableDB(nil), "", tt.url, nil,
			)
			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			defer cl()

			// TODO Implement integration tests for [BuildServices].
			ctx, c := context.WithCancel(context.Background())
			c()
			_, err = got.Feed.Index(ctx, &feed.IndexPayload{})
			r.EqualError(err, (&feed.InternalError{}).Error())
		})
	}
}

const dbDriver = "mock"

func registerMockAdapter(c *gomock.Controller) {
	registerOnce.Do(func() {
		mocks.RegisterMockAdapter(c, dbDriver)
	})
}

var registerOnce = sync.Once{}
