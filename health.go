// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader

import (
	"context"
	"log"

	"github.com/antichris/go-feedreader/goa/gen/health"
)

var _ health.Service = (*healthsrvc)(nil)

// Health service example implementation.
// The example methods log the requests and return zero values.
type healthsrvc struct {
	logger *log.Logger
}

// NewHealth returns the Health service implementation.
func NewHealth(logger *log.Logger) health.Service {
	return &healthsrvc{logger}
}

// Responds with a 204 No Content success code as long as the server is up and
// running.
func (s *healthsrvc) Show(ctx context.Context) (err error) {
	s.logger.Print("health.Show")
	return
}
