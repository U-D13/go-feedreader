// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//go:build wireinject

//go:generate go run github.com/google/wire/cmd/wire gen -header_file=../internal/_resources/license-header.go
//go:generate go fix -fix buildtag .
//go:generate go run mvdan.cc/gofumpt -w wire_gen.go

package token

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"hash"

	"github.com/google/wire"
)

// New token Service instance.
func New(
	Key,
) Service {
	panic(wire.Build(
		serviceSet,
		encoderSet,
	))
}

// // New token Service instance using the given Encoder.
// func NewWithEncoder(
// 	Encoder,
// ) Service {
// 	panic(wire.Build(
// 		serviceSet,
// 	))
// }

// New token Encoder instance.
func NewEncoder(
	Key,
) Encoder {
	panic(wire.Build(
		encoderSet,
	))
}

var (
	serviceSet = wire.NewSet(
		wire.Bind(new(Service), new(*service)),
		wire.Struct(new(service), "*"),
		wire.Value(DefaultDuration),
	)
	encoderSet = wire.NewSet(
		wire.Bind(new(Encoder), new(*encoder)),
		wire.Struct(new(encoder), "*"),
		wire.Value(base64.RawURLEncoding),
		provideHMAC,
	)
)

func provideHMAC(k Key) hash.Hash {
	return hmac.New(sha256.New, []byte(k))
}

const (
	hmacSize             = 32
	signatureSize        = 43
	hmacEncodingOverhead = signatureSize - hmacSize

	maxRawPayload = 2 + // 2 braces
		3* // 3 sets of
			(2+ // 2 quotes
				3+ // 3 letters
				1+ // 1 colon
				10) + // up to 10 digits
		2 // 2 commas
	maxEncodedPayload = maxRawPayload*4/3 + maxRawPayload*4%3
	maxTokenSize      = maxEncodedPayload + 1 + signatureSize
)
