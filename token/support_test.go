// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package token_test

import . "github.com/antichris/go-feedreader/token"

const (
	k         Key   = "foo"
	zeroToken Token = "eyJ1aWQiOjAsImlhdCI6MCwiZXhwIjowfQ.9D73HGkBB4ym7ldA_xla8QA_cJ64LeM_q-VaD0vaumE"
	maxToken  Token = "eyJ1aWQiOjQyOTQ5NjcyOTUsImlhdCI6MjE0NzQ4MzY0NywiZXhwIjoyMTQ3NDgzNjQ3fQ.2snWq5kefcIB3bK_wErFQB3Nu-OPXYsGIGMLNijWgv8"

	maxUint32 = 1<<32 - 1
	maxInt32  = maxUint32 >> 1
)

var maxTokenData = TokenData{
	UID: maxUint32,
	Iat: maxInt32,
	Exp: maxInt32,
}
