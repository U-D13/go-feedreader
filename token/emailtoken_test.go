// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package token

import (
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"io"
	"testing"
	"time"

	"github.com/antichris/go-feedreader/terror"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestGen(t *testing.T) {
	t.Parallel()
	for _, tt := range []struct {
		size int
		rErr error
		wErr string
	}{{
		wErr: io.ErrShortBuffer.Error(),
	}, {
		size: 8,
		wErr: io.ErrShortBuffer.Error(),
	}, {
		size: 11,
		rErr: errSnap,
		wErr: errSnap.Error(),
	}, {
		size: 11,
	}, {
		size: 12,
	}, {
		size: 32,
	}} {
		tt := tt
		t.Run("", func(t *testing.T) {
			t.Parallel()
			rl := tt.size - encodedTSSize

			m := newMockReader(t)
			m.On("Read", mock.MatchedBy(func(p []byte) bool {
				return len(p) == rl
			})).
				Return(rl, tt.rErr).
				Once()

			r := mailTokenReader{m}

			p := make([]byte, tt.size)
			now := time.Now()
			n, err := r.Read(p)

			rq := require.New(t)
			if tt.wErr != "" {
				rq.EqualError(err, tt.wErr)
				return
			}
			rq.NoError(err)
			rq.Equal(tt.size, n)

			if !m.AssertExpectations(t) {
				t.FailNow()
			}

			b := make([]byte, rawTSSize)
			n, err = base64.RawURLEncoding.Decode(b, p[rl:])
			rq.NoError(err)
			rq.Equal(rawTSSize, n)

			ts := time.Unix(0, int64(binary.BigEndian.Uint64(b)))
			rq.WithinDuration(now, ts, 10*time.Microsecond)
		})
	}
}

func TestB64RandReader_Read(t *testing.T) {
	t.Parallel()
	for _, tt := range []struct {
		rErr error
		want string
		wErr string
	}{
		{
			want: "__",
			rErr: errSnap,
			wErr: errSnap.Error(),
		},
		{
			want: "____",
			rErr: errSnap,
			wErr: errSnap.Error(),
		},
		{want: ""},
		{want: "_"},
		{want: "__"},
		{want: "___"},
		{want: "____"},
		{want: "_____"},
		{want: "______"},
		{want: "_______"},
	} {
		tt, size := tt, len(tt.want)
		t.Run(fmt.Sprintf("size=%v/wantErr=%s", size, tt.wErr), func(t *testing.T) {
			t.Parallel()
			r := b64RandReader{ffreader{tt.rErr}}

			p := make([]byte, size)
			n, err := r.Read(p)

			rq := require.New(t)
			if tt.wErr != "" {
				rq.EqualError(err, tt.wErr)
				return
			}
			rq.NoError(err)
			rq.Equal(size, n)
			rq.NotSubset(p, []byte{0})
			rq.Equal(tt.want, string(p))
		})
	}
}

const errSnap = terror.Error("snap")

func newMockReader(t mock.TestingT) *mockReader {
	m := &mockReader{}
	m.Test(t)
	return m
}

type mockReader struct{ mock.Mock }

// Read implements io.Reader
func (r *mockReader) Read(p []byte) (n int, err error) {
	a := r.Called(p)
	return a.Int(0), a.Error(1)
}

type ffreader struct{ error }

// Read implements io.Reader
func (r ffreader) Read(p []byte) (n int, err error) {
	if r.error != nil {
		err = r.error
		return
	}
	for n = 0; n < len(p); n++ {
		p[n] = 0xFF
	}
	return
}
