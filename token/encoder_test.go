// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package token_test

import (
	"testing"

	. "github.com/antichris/go-feedreader/token"
	"github.com/stretchr/testify/require"
)

func Test_encoder_Encode(t *testing.T) {
	t.Parallel()
	// TODO Test error states.
	for n, tt := range map[string]struct {
		t    TokenData
		want Token
	}{"zero value": {
		want: zeroToken,
	}, "maxed out": {
		t:    maxTokenData,
		want: maxToken,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s := NewEncoder(k)

			got, err := s.Encode(tt.t)

			r := require.New(t)
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_encoder_Decode(t *testing.T) {
	t.Parallel()
	const (
		sSig = "rSWXTj60Mhsv0uZ4m2dZFq8VhcCBQApuHhaUXT6cnA"
		sig  = "o" + sSig
		minP = "e30"
	)
	var (
		eUnsupp = ErrUnsupportedFormat
		eInvSig = ErrInvalidSignature
	)
	for n, tt := range map[string]struct {
		t    Token
		want TokenData
		wErr error
	}{
		"empty":                  {wErr: eUnsupp, t: ""},
		"no payload":             {wErr: eUnsupp, t: "." + sig},
		"no signature":           {wErr: eUnsupp, t: minP + "."},
		"more than 2 parts":      {wErr: eUnsupp, t: minP + ".^." + sig},
		"signature too short":    {wErr: eUnsupp, t: minP + "^." + sSig},
		"signature too long":     {wErr: eUnsupp, t: minP + ".^" + sig},
		"broken signature":       {wErr: eInvSig, t: minP + ".^" + sSig},
		"non-matching signature": {wErr: eInvSig, t: minP + ".0" + sSig},
		"empty payload": {
			t: minP + "." + sig,
		},
		"zero value": {
			t: zeroToken,
		},
		"maxed out": {
			t:    maxToken,
			want: maxTokenData,
		},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s := NewEncoder(k)

			got, err := s.Decode(tt.t)

			r := require.New(t)
			if tt.wErr != nil {
				r.ErrorIs(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Benchmark_encoder_Encode(b *testing.B) {
	s := NewEncoder(k)
	b.SetBytes(int64(len(maxToken)))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = s.Encode(maxTokenData)
	}
}

func Benchmark_encoder_Decode(b *testing.B) {
	s := NewEncoder(k)
	b.SetBytes(int64(len(maxToken)))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = s.Decode(maxToken)
	}
}
