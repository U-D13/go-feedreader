// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package token implements tools for working with rJWT (reduced JWT)
// tokens.
package token

import (
	"time"

	"github.com/antichris/go-feedreader/goa/gen/token"
	"github.com/antichris/go-feedreader/terror"
)

// TODO Implement a binary encoder.

type (
	// Token is an authorization token.
	Token     = token.OurToken
	TokenData = token.TokenData
)

type (
	Service interface {
		CreateFor(userID uint32, d time.Duration) (Token, error)
		GetUserID(Token) (uint32, error)
		Validate(Token) (bool, error)
		Encoder
	}
	// Encoder can encode and decode a token.
	Encoder interface {
		Encode(TokenData) (Token, error)
		Decode(Token) (TokenData, error)
	}
	Key string
)

const DefaultDuration = 24 * time.Hour

var _ Service = (*service)(nil)

type service struct {
	defDuration time.Duration
	Encoder
}

func (s *service) CreateFor(userID uint32, d time.Duration) (Token, error) {
	if d == 0 {
		d = s.defDuration
	}
	t := time.Now()
	return s.Encode(TokenData{
		UID: userID,
		// TODO Expand into 64 bit space, 2038-01-19T03:14:07Z is approaching.
		Iat: int32(t.Unix()),
		Exp: int32(t.Add(d).Unix()),
	})
}

func (s *service) GetUserID(t Token) (uint32, error) {
	td, err := s.Decode(t)
	if err != nil {
		return 0, err
	}
	if s.isExpired(td) {
		return 0, ErrExpired
	}
	return td.UID, nil
}

func (s *service) Validate(t Token) (bool, error) {
	td, err := s.Decode(t)
	if err != nil {
		return false, err
	}
	// TODO Consider more checks than just expiration.
	return !s.isExpired(td), nil
}

func (s *service) isExpired(t TokenData) bool {
	return t.Exp <= int32(time.Now().Unix())
}

// ErrExpired is returned when the expiration time of token has passed.
const ErrExpired = Error("expired token")

// ErrInvalidSignature is returned when the token signature cannot be
// validated.
const ErrInvalidSignature = Error("invalid token signature")

// ErrUnsupportedFormat is returned when the token does not match the
// format that is supported by this package.
const ErrUnsupportedFormat = Error("unsupported token format")

type Error = terror.Error
