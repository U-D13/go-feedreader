// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package token_test

import (
	"testing"
	"time"

	. "github.com/antichris/go-feedreader/token"
	"github.com/stretchr/testify/require"
)

func Test_service_CreateFor(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		uid  uint32
		d    time.Duration
		wD   time.Duration
		wErr error
	}{"default duration": {
		uid: 5,
		wD:  DefaultDuration,
	}, "5 minutes": {
		uid: 8,
		d:   5 * time.Minute,
		wD:  5 * time.Minute,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s := New(k)

			now := time.Now()
			got, err := s.CreateFor(tt.uid, tt.d)

			r := require.New(t)
			r.NoError(err)
			td, err := s.Decode(got)
			r.NoError(err)
			r.Equal(tt.uid, td.UID)
			r.WithinDuration(now, unixTime32(td.Iat), time.Second)
			r.WithinDuration(now.Add(tt.wD), unixTime32(td.Exp), time.Second)
		})
	}
}

func Test_service_GetUserID(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		t    Token
		want uint32
		wErr error
	}{"invalid": {
		t:    "",
		wErr: ErrUnsupportedFormat,
	}, "expired": {
		t:    zeroToken,
		wErr: ErrExpired,
	}, "maxed out": {
		t:    maxToken,
		want: maxTokenData.UID,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s := New(k)

			got, err := s.GetUserID(tt.t)

			r := require.New(t)
			if tt.wErr != nil {
				r.ErrorIs(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_service_Validate(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		t    Token
		want bool
		wErr error
	}{"invalid": {
		t:    "",
		wErr: ErrUnsupportedFormat,
	}, "expired": {
		t:    zeroToken,
		want: false,
	}, "maxed out": {
		t:    maxToken,
		want: true,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s := New(k)

			got, err := s.Validate(tt.t)

			r := require.New(t)
			if tt.wErr != nil {
				r.ErrorIs(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func unixTime32(timestamp int32) time.Time {
	x := time.Unix(int64(timestamp), 0)
	return x
}
