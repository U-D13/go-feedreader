// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/antichris/go-feedreader/token (interfaces: Service)

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"
	time "time"

	token "github.com/antichris/go-feedreader/goa/gen/token"
	gomock "github.com/golang/mock/gomock"
)

// MockService is a mock of Service interface.
type MockService struct {
	ctrl     *gomock.Controller
	recorder *MockServiceMockRecorder
}

// MockServiceMockRecorder is the mock recorder for MockService.
type MockServiceMockRecorder struct {
	mock *MockService
}

// NewMockService creates a new mock instance.
func NewMockService(ctrl *gomock.Controller) *MockService {
	mock := &MockService{ctrl: ctrl}
	mock.recorder = &MockServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockService) EXPECT() *MockServiceMockRecorder {
	return m.recorder
}

// CreateFor mocks base method.
func (m *MockService) CreateFor(arg0 uint32, arg1 time.Duration) (token.OurToken, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateFor", arg0, arg1)
	ret0, _ := ret[0].(token.OurToken)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateFor indicates an expected call of CreateFor.
func (mr *MockServiceMockRecorder) CreateFor(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateFor", reflect.TypeOf((*MockService)(nil).CreateFor), arg0, arg1)
}

// Decode mocks base method.
func (m *MockService) Decode(arg0 token.OurToken) (token.TokenData, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Decode", arg0)
	ret0, _ := ret[0].(token.TokenData)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Decode indicates an expected call of Decode.
func (mr *MockServiceMockRecorder) Decode(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Decode", reflect.TypeOf((*MockService)(nil).Decode), arg0)
}

// Encode mocks base method.
func (m *MockService) Encode(arg0 token.TokenData) (token.OurToken, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Encode", arg0)
	ret0, _ := ret[0].(token.OurToken)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Encode indicates an expected call of Encode.
func (mr *MockServiceMockRecorder) Encode(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Encode", reflect.TypeOf((*MockService)(nil).Encode), arg0)
}

// GetUserID mocks base method.
func (m *MockService) GetUserID(arg0 token.OurToken) (uint32, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUserID", arg0)
	ret0, _ := ret[0].(uint32)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetUserID indicates an expected call of GetUserID.
func (mr *MockServiceMockRecorder) GetUserID(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUserID", reflect.TypeOf((*MockService)(nil).GetUserID), arg0)
}

// Validate mocks base method.
func (m *MockService) Validate(arg0 token.OurToken) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Validate", arg0)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Validate indicates an expected call of Validate.
func (mr *MockServiceMockRecorder) Validate(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Validate", reflect.TypeOf((*MockService)(nil).Validate), arg0)
}
