// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package token

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"io"
	"time"
)

// MailTokenReader generates email validation tokens on every read.
// The read buffer (p) length must be at least 11 bytes to fit a
// base64-encoded 64 bit int for current timestamp.
var MailTokenReader io.Reader = mailTokenReader{B64RandReader}

type mailTokenReader struct{ r io.Reader }

func (r mailTokenReader) Read(p []byte) (n int, err error) {
	l := len(p)
	if l < encodedTSSize {
		return 0, io.ErrShortBuffer
	}
	now := time.Now()
	rl := l - encodedTSSize
	var raw []byte
	if rl < rawTSSize {
		// TODO Optimize to reduce allocations.
		raw = make([]byte, rawTSSize)
	} else {
		raw = p[:rawTSSize]
	}
	binary.BigEndian.PutUint64(raw, uint64(now.UnixNano()))
	base64.RawURLEncoding.Encode(p[rl:], raw)
	_, err = r.r.Read(p[:rl])
	return l, err
}

const (
	rawTSSize     = 8
	encodedTSSize = 11 // ((8*rawTimestampLength + 5) / 6
)

// B64RandReader generates base64-encoded cryptographically secure
// random bytes on every read.
var B64RandReader = b64RandReader{rand.Reader}

// XXX If performance requires, wrap `rand.Reader` in `bufio.Reader`.

type b64RandReader struct{ r io.Reader }

func (g b64RandReader) Read(p []byte) (n int, err error) {
	l := len(p)
	if l == 0 {
		// TODO Consider returning an error (e.g. [io.ErrShortBuffer]).
		return
	}
	// TODO Optimize to reduce allocations.

	e := base64.RawURLEncoding
	// The base64 encoding translates every 6 bits into 8 (LCM 24),
	// thus every 3 input bytes are encoded into 4 output bytes.
	r := l % 4
	if r == 0 { // The easy path.
		/// XXX Allocation.
		b := make([]byte, e.DecodedLen(l))
		if n, err = io.ReadFull(g.r, b); err != nil {
			return
		}
		e.Encode(p, b)

		n = l
		return
	}

	// If the output size is not a multiple of 4 bytes (32 bits) exactly,
	// we must generate and encode a few extra bytes to fill all the
	// available bits of the output.
	dl := l - r // Floor the output length to nearest multiple of 4.
	sl := e.DecodedLen(dl)
	rl := sl + r // Length of random bytes to read.
	/// XXX Allocation.
	b := make([]byte, rl+r+1) // Add space for the encoded extra bytes.
	if n, err = io.ReadFull(g.r, b[:rl]); err != nil {
		return
	}
	e.Encode(p, b[:sl])
	dst := b[rl:]           // Encoded extra bytes.
	e.Encode(dst, b[sl:rl]) // Encode the extra bytes.
	copy(p[dl:], dst)       // Copy the (first few) encoded extra bytes.

	n = l
	return
}
