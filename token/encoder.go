// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package token

import (
	"bytes"
	"encoding/base64"
	"hash"
	"sync"

	"github.com/antichris/go-feedreader/goa/gen/http/token/server"
	"github.com/goccy/go-json"
)

var _ Encoder = (*encoder)(nil)

// encoder encodes and decodes rJWT (reduced JWT) tokens.
//
// An rJWT is similar to full JWT in that its base64-encoded JSON
// payload and signature are separated by a dot ("."), but, unlike JWT,
// rJWT does not have a header since it is always signed using the
// HS256 algorithm.
type encoder struct {
	key  Key
	b64  *base64.Encoding
	hmac hash.Hash
	hmu  sync.Mutex `wire:"-"`
}

// Encode an rJWT token.
func (e *encoder) Encode(td TokenData) (t Token, err error) {
	b := encPool.Get().(*encBuf)
	defer b.free()

	// Raw JSON payload is written at the beginning of the shared buffer.
	if err = b.Encode(server.NewDescribeResponseBody(&td)); err != nil {
		return
	}
	raw := b.jsonBytes()
	// Base64 payload is written after the raw JSON in the shared buffer.
	e.b64.Encode(b.Token, raw)
	pl := e.b64.EncodedLen(len(raw))

	sig := pl + 1
	signed := b.Token[:sig+signatureSize]
	signed[pl] = '.'
	// Signature is written after the Base64 payload in the shared buffer.
	if err = e.base64Signature(signed[sig:], b.Token[:pl]); err != nil {
		return
	}

	t = Token(signed)
	return
}

// Decode an rJWT token.
func (e *encoder) Decode(t Token) (td TokenData, err error) {
	l := len(t)
	if l < signatureSize+4 || l > maxTokenSize {
		err = ErrUnsupportedFormat
		return
	}
	buf := encPool.Get().(*encBuf)
	defer buf.free()

	bt := buf.Token[:l]
	copy(bt, t)

	dot := bytes.IndexByte(bt, '.')
	if dot < 1 || dot != bytes.LastIndexByte(bt, '.') {
		err = ErrUnsupportedFormat
		return
	}
	p := bt[:dot]
	sig := bt[dot+1:]

	if len(sig) != signatureSize {
		err = ErrUnsupportedFormat
		return
	}

	sb := buf.Raw[:signatureSize]
	if err = e.base64Signature(sb, p); err != nil {
		return
	}
	if !bytes.Equal(sb, sig) {
		err = ErrInvalidSignature
		return
	}

	pb := buf.Raw[:e.b64.DecodedLen(len(p))]
	if _, err = e.b64.Decode(pb, p); err != nil {
		return
	}
	err = json.Unmarshal(pb, &td)
	return
}

// base64Signature of payload written into dst.
func (e *encoder) base64Signature(dst, payload []byte) error {
	const offset = hmacEncodingOverhead
	// Write raw signature bytes to dst at offset.
	if _, err := e.signature(dst[:offset], payload); err != nil {
		return err
	}
	// Encode signature reading from offset.
	e.b64.Encode(dst[:signatureSize], dst[offset:])
	return nil
}

// signature is appended to b and that is returned.
func (e *encoder) signature(b, payload []byte) ([]byte, error) {
	e.hmu.Lock()
	defer e.hmu.Unlock()
	defer e.hmac.Reset()
	e.hmac.Reset()
	if _, err := e.hmac.Write(payload); err != nil {
		return nil, err
	}
	return e.hmac.Sum(b), nil
}

// encPool is a pool of [*encBuf].
var encPool = sync.Pool{
	New: func() any {
		b := make([]byte, maxRawPayload+maxTokenSize)
		jb := bytes.NewBuffer(b[:0])
		return &encBuf{
			Raw:     b[:maxRawPayload],
			Token:   b[maxRawPayload:],
			Buffer:  jb,
			Encoder: json.NewEncoder(jb),
		}
	},
}

type encBuf struct {
	// Raw is unencoded token data JSON, [maxRawPayload] bytes at most.
	Raw []byte
	// Token is base64-encoded and signed rJWT, [maxTokenSize] bytes at
	// most.
	Token []byte
	*bytes.Buffer
	*json.Encoder
}

func (b *encBuf) jsonBytes() (p []byte) {
	p = b.Bytes()
	l := len(p)
	if p[l-1] == '\n' {
		// The standard library encoder sticks an extra newline at the
		// end, because that "makes the output look a little nicer".
		// We don't want "nice" looks, we want efficient size.
		l--
	}
	return p[:l]
}

// free cleans up b and returns it back to the pool for reuse.
func (b *encBuf) free() {
	b.Reset()
	encPool.Put(b)
}
