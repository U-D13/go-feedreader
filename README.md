# go-feedreader

[![godoc-badge]][godoc]
[![release-badge]][latest-release]
[![license-badge]][license]
[![goreport-badge]][goreport]

## RSS/Atom feed reader

Displays a list of items from an RSS or Atom feed to authenticated users. Become a user by registering.

A Go reimagining of <https://bitbucket.org/antichris/rss-reader/>.

> - - -
>
> ### ⚠️ Warning
>
> This is a very early pre-release code. Things may be very awkward, messy, broken, and/or missing in their entirety.
>
> The architecture, design, and APIs might change. The documentation *will* change.
>
> This has been pushed to a repository other than the Go module on purpose: to protect unsuspecting souls from stumbling into something they are not ready for (and that's not ready for them).
>
> Read more in the [development notes][dev-notes].
> - - -

If you wish to browse the reference documentation at this time, you can spin up a local version of `golang.org/x/tools/cmd/godoc` for that.


## Installation

Since this is not yet available from the module repository, you can't just `go install` it. Yet.

Instead, you'd have to either `git clone` this repository or download its source at <https://gitlab.com/U-D13/go-feedreader/-/archive/master/go-feedreader-master.tar.gz>.

### Local Go

If you have Go installed locally, you can then build the main server by doing

```sh
go build ./cmd/feedreader
```

in the directory where you've put the source code. If you prefer so, you can even install to your `GOBIN` directory with

```sh
go install ./cmd/feedreader
```

### Installation on Docker

If you don't have Go, but you do have Docker, you can build an Alpine-based image from the provided `Dockerfile` by running

```sh
docker build -t feedreader:latest .
```

### Docker Compose

The [`devops` directory](devops) has a bunch of sample Compose files. See the [`README.md` there][devops-readme] for more details.


## Operation

Before starting the server for the first time, it is recommended to apply the database migrations. Recommended, not required, as everything that can work without the database will happily do so. And, conversely, nothing that needs database will work until the initial migration is applied. So, *recommended*.

New releases in the future may require additional database migrations.

Server processes can be sent `SIGINT` or `SIGTERM` and they shall shutdown gracefully. Migrations also are able to handle these signals and stop safely.

 See [Configuration] for details on the available configuration options.

### Local Go

For the sake of brevity, we're assuming you've put the feedreader binary someplace in your `PATH` (e.g., your `GOBIN` via `go install`). If that's not the case, you should either invoke it as `./feedreader` (or with the relative or absolute path) or, if you didn't bother building (nothing wrong with that, it's just that a binary starts faster), run it like `go run ./cmd/feedreader`.

#### CLI help

This will list all available commands and their parameters:

```sh
feedreader help
```

#### Migrate

This would migrate the default database.

```sh
feedreader migrate
```

#### Start a full-stack server

This will start listening at the default port of the default host/IP with the default database parameters and use the default SMTP server settings.

```sh
feedreader serve all
```

#### Serve the API, web UI and its static content separately

The following would serve the web UI at `feedreader.localhost:8080`, its static files at `static.feedreader.localhost:8082`, and the API at `api.feedreader.localhost:8081`:

```sh
feedreader serve ui \
	--index-only \
	--host feedreader.localhost:8080 \
	--api-url //api.feedreader.localhost:8081/api \
	--static-path //static.feedreader.localhost:8082
```

```sh
feedreader serve api \
	--host api.feedreader.localhost:8081 \
	--ui-url http://feedreader.localhost:8080
```

```sh
feedreader serve ui \
	--files-only \
	--host static.feedreader.localhost:8082 \
	--ui-origin http://feedreader.localhost:8080 \
	--static-path /
```

Currently it is not (yet) possible to serve different content at different hostnames on the same port, nor to listen at multiple different TCP bindings (host/IP + port) within the same process, so separate ones must be started.

#### Start a server with TLS

You would need to pass a certificate and private key, both in PEM format:

```sh
feedreader serve all \
	--host feedreader.localhost:4430 \
	--api-url //api.feedreader.localhost:4430/api \
	--ui-url https://feedreader.localhost:4430 \
	--ui-origin https://feedreader.localhost:4430 \
	--secure \
	--cert-file feedreader.crt \
	--key-file feedreader.key
```

This would start a full-stack server listening for HTTPS traffic at <https://feedreader.localhost:4430> with the rest of the parameters all at their default values.


### Operation on Docker

Due to how Docker containers are isolated from the host system, manually running containerized Feed Reader could be a bit of a pain. But if you can automate that by using, for example, Docker Compose or Kubernetes, it's really not that bad at all.

Since containerized Feed Reader is essentially the same application as a binary that's deployed directly in a host OS, it can take all of the same [configuration] parameters. The differences arise out of the isolation that is applied and must be worked with, e.g., binding ports, mounting files and directories, and accessing the host's TCP services.

<details><p>

#### CLI help on Docker

```sh
docker run --rm feedreader:latest help
```

#### Migrate on Docker

This would migrate the default SQLite database in a file that's mapped to `feedreader.db` in the current working directory:

```sh
docker run \
	-it --rm \
	-v "$(pwd)/feedreader.db:/feedreader.db" \
	feedreader:latest \
	migrate
```

#### Start a full-stack server on Docker

The following will start a daemonized server reachable at `localhost:8080` in a container `feedreader`, that would connect to your Docker host for SMTP at the default port (as defined by the Feed Reader [configuration]):

```sh
docker run \
	-d \
	--name feedreader \
	-p 8080:80 \
	-v "$(pwd)/feedreader.db:/feedreader.db" \
	--add-host=host.docker.internal:host-gateway \
	feedreader:latest \
	serve all \
		--host :80 \
		--smtp-host host.docker.internal
```

> - - -
>
> #### 👉 Note
>
> If the SMTP server listening on your host doesn't support `STARTTLS` encryption mode, the [`smtp.PlainAuth`] used by the Feed Reader will abort connections, and sending mail will always fail.
>
> Either set up an SMTP server with `STARTTLS` support, or use an external, 3rd-party one.
> - - -

#### Serve the API, web UI and its static content separately on Docker

The following would serve the web UI at `feedreader.localhost:8080`, its static files at `static.feedreader.localhost:8082`, and the API at `api.feedreader.localhost:8081`:

```sh
docker run \
	-d \
	--name feedreader-ui \
	-p 8080:80 \
	feedreader:latest \
	serve ui \
		--index-only \
		--host :80 \
		--api-url //api.feedreader.localhost:8081/api \
		--static-path //static.feedreader.localhost:8082
```

```sh
docker run \
	-d \
	--name feedreader-api \
	-p 8081:80 \
	-v "$(pwd)/feedreader.db:/feedreader.db" \
	--add-host=host.docker.internal:host-gateway \
	feedreader:latest \
	serve api \
		--host :80 \
		--smtp-host host.docker.internal \
		--ui-url http://feedreader.localhost:8080
```

```sh
docker run \
	-d \
	--name feedreader-static \
	-p 8082:80 \
	feedreader:latest \
	serve ui \
		--files-only \
		--host :80 \
		--ui-origin http://feedreader.localhost:8080 \
		--static-path /
```

#### Start a server with TLS on Docker

```sh
docker run \
	-d \
	--name feedreader-secure \
	-p 4430:443 \
	-v "$(pwd):/certs:ro" \
	-v "$(pwd)/feedreader.db:/feedreader.db" \
	--add-host=host.docker.internal:host-gateway \
	feedreader:latest \
	serve all \
		--host :443 \
		--smtp-host host.docker.internal \
		--api-url //feedreader.localhost:4430/api \
		--ui-url https://feedreader.localhost:4430 \
		--ui-origin https://feedreader.localhost:4430 \
		--secure \
		--cert-file /certs/feedreader.crt \
		--key-file /certs/feedreader.key
```

To simplify things here, the entire current directory is mounted in the container as read-only `certs`, but this is not mandatory; you can mount each file separately, if you wish, one by one.

</details><p>


## Configuration

At this time, all configuration is done with CLI parameters.

For an extensive listing, see

```sh
feedreader help
```

Or, if you're [using Docker][docker-install],

```sh
docker run --rm feedreader:latest help
```

### API token key

To preserve the validity of API access tokens across server restarts, it is recommended to set the HMAC key for API token signing to a random string that retains the same value when the server is restarted.

```sh
feedreader serve all -k DYKXDWF4h9UbVmdq0oHhsOCD69Wnadlw9kCobz5eNi5acrtsUCPiEa1mSaklgpXz
```

Changing its value invalidates all previously issued access tokens.

If not specified, Feed Reader generates a unique, base64-encoded, cryptographically secure 384-bit random string on each run. As a result, unless you explicitly set it, the value will change, invalidating access tokens with each restart.

You can look at the CLI help of `serve api` or `serve all` for a value to copy and use.


### Database

Currently, three storage driver adapters are available:

- `sqlite` (default) — SQLite
- `pgsql` — PostgreSQL
- `mysql` — MySQL

The DSN must be specified in a scheme-less URI form, e.g.,

```go
// instead of "scheme://user:pass@host:port/path?query"
"user:pass@host:port/path?query"
```

#### MySQL

Instead of just `host:port`, the DSN for a MySQL database uses `network(address)` (as described in [`net.Dial`]).

MySQL also requires adding a `parseTime=true` parameter.

```go
"user:pass@tcp(dbhost)/database?parseTime=true"
// which is the same as
"user:pass@tcp(dbhost:mysql)/database?parseTime=true"
// or
"user:pass@tcp(dbhost:3306)/database?parseTime=true"
```

Note that the parentheses are defined in POSIX.1-2017 as [control operators][cc] that require [quoting], which applies to commands in shells and scripts, as well as in Compose files (as of Compose V2).


### SMTP

The default SMTP parameters fit a setup where an [Inbucket] server is listening on the same host as Feed Reader; with host `localhost`, port `2500`, and empty authentication parameters.

If you run a containerized Feed Reader instance, the SMTP server must support `STARTTLS` encryption, as the SMTP server, otherwise local to your host, is no longer on `localhost` for the instance.

<details><summary>In this scenario, an Inbucket instance should be started with TLS enabled and a valid certificate and private key.</summary><p>

The Subject Alternative Name of the SMTP server certificate should also include the `host.docker.internal` DNS name.

```sh
docker run \
	-d \
	--name inbucket \
	-p 2500:2500 \
	-p 9000:9000 \
	-e INBUCKET_SMTP_TLSENABLED=true \
	-e INBUCKET_SMTP_TLSPRIVKEY=/cert.key \
	-e INBUCKET_SMTP_TLSCERT=/cert.pem \
	-v "$(pwd)/smtp.key:/cert.key:ro" \
	-v "$(pwd)/smtp.crt:/cert.pem:ro" \
	inbucket/inbucket:latest
```

If the SMTP server uses a certificate that is not signed by one of the CAs in the Mozilla CA certificate bundle, you must also mount the respective signing CA certificate in the Feed Reader container's `/etc/ssl/certs` directory.

Here's a comprehensive example of running in such a scenario:

```sh
docker run \
	-d \
	--name feedreader-secure \
	-p 4430:443 \
	-v "$(pwd)/ca.crt:/etc/ssl/certs/ca.crt:ro" \
	-v "$(pwd):/certs:ro" \
	-v "$(pwd)/feedreader.db:/feedreader.db" \
	--add-host=host.docker.internal:host-gateway \
	feedreader:latest \
	serve all \
		--host :443 \
		--api-url //feedreader.localhost:4430/api \
		--ui-url https://feedreader.localhost:4430 \
		--ui-origin https://feedreader.localhost:4430 \
		--secure \
		--cert-file /certs/feedreader.crt \
		--key-file /certs/feedreader.key \
		--smtp-host host.docker.internal
```

</details><p>

If you're going to run Feed Reader locally, but the SMTP server uses a certificate that is not signed by one of the CAs in your system's CA certificate store, set the `SSL_CERT_FILE` environment variable to the path (absolute or relative to current working directory) of the signing CA certificate.

Then, of course, there is always the option of using a truly external SMTP server.

### Feed URL

The feed URL at this point defaults to [The Register]'s software Atom feed, but that can be overridden in a `feed-url` file. The first non-empty line that does not start with a `#` is used as the feed URL.

<details><summary>Example</summary><p>

```ignore
# This is ignored:
# https://www.theregister.co.uk/software/headlines.atom
# This is used:
http://feeds.bbci.co.uk/news/world/rss.xml
# This is not:
https://www.wired.com/feed/rss
```

</details><p>

If Feed Reader is deployed via Docker, the `feed-url` file should be mounted at the root of the container file system.


## License

The source code of this project is released under the [Mozilla Public License Version 2.0][mpl]. See [LICENSE].


[dev-notes]: docs/dev-notes.md
	"Feed Reader development notes"

[`smtp.PlainAuth`]: https://pkg.go.dev/net/smtp#PlainAuth
	"smtp package - net/smtp - Go Packages"

[devops-readme]: devops/README.md
	"DevOps resources README"

[`net.Dial`]: https://pkg.go.dev/net#Dial
	"net package - net - Go Packages"

[cc]: https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_113
	"POSIX.1-2017: Base Definitions § 3.113"
[quoting]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02
	"POSIX.1-2017: Shell & Utilities § 2.2"

[Inbucket]: https://inbucket.org/
	"Inbucket"

[The Register]: https://www.theregister.com/
	"The Register: Enterprise Technology News and Analysis"

[mpl]: https://www.mozilla.org/en-US/MPL/2.0/
    "Mozilla Public License, version 2.0"

[docker-install]: #installation-on-docker
	"go-feedreader ❭ Installation ❭ Installation on Docker"
[configuration]: #configuration
	"go-feedreader ❭ Configuration"

[license]: LICENSE

[godoc]: https://pkg.go.dev/github.com/antichris/go-feedreader
[latest-release]: https://github.com/antichris/go-feedreader/releases/latest
[goreport]: https://goreportcard.com/report/github.com/antichris/go-feedreader

[godoc-badge]: https://godoc.org/github.com/antichris/go-feedreader?status.svg
[release-badge]: https://img.shields.io/github/release/antichris/go-feedreader
[license-badge]: https://img.shields.io/github/license/antichris/go-feedreader
[goreport-badge]: https://goreportcard.com/badge/github.com/antichris/go-feedreader?status.svg
