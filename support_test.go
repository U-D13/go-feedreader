// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader_test

import (
	"context"
	"io"
	"log"
	"testing"

	. "github.com/antichris/go-feedreader"
	"github.com/antichris/go-feedreader/auth"
	authMocks "github.com/antichris/go-feedreader/auth/mocks"
	"github.com/antichris/go-feedreader/goa/gen/token"
	. "github.com/antichris/go-feedreader/terror"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"goa.design/goa/v3/security"
)

type (
	feedsrvc  = Xfeedsrvc
	tokensrvc = Xtokensrvc
	usersrvc  = Xusersrvc
)

const feedURL = XfeedURL

var (
	getFeedURL      = XgetFeedURL
	feedUnauthErr   = XfeedUnauthErr
	feedInternalErr = XfeedInternalErr

	tokenInternalErr = XtokenInternalErr
	tokenBadReqErr   = XtokenBadReqErr
	tokenUnauthErr   = XtokenUnauthErr
	errInvalidCreds  = XerrInvalidCreds

	userInternalErr = XuserInternalErr
	userBadReqErr   = XuserBadReqErr
	userUnauthErr   = XuserUnauthErr
	errUserNotFound = XerrUserNotFound
)

func requireAuthServiceAPIKeyAuthCall(
	t *testing.T,
	newAuther func(a auth.Service) Auther,
) {
	t.Helper()

	key := "<key>"

	type k struct{}
	wantCtx := context.WithValue(ctx, k{}, key)
	wantErr := errSnap

	a := authMocks.NewMockService(gomock.NewController(t))
	// TODO Consider asserting specific error wrappers instead of any.
	mErrWrap := gomock.AssignableToTypeOf((auth.ErrorWrapper)(nil))
	a.EXPECT().
		APIKeyAuth(ctx, key, mErrWrap, mErrWrap).
		Return(wantCtx, wantErr)

	svc := newAuther(a)
	gotCtx, gotErr := svc.APIKeyAuth(ctx, key, &security.APIKeyScheme{})

	r := require.New(t)
	r.ErrorIs(gotErr, wantErr)
	r.Same(wantCtx, gotCtx)
}

// Auther defines the authorization functions to be implemented by the service.
type Auther interface {
	// APIKeyAuth implements the authorization logic for the APIKey security
	// scheme.
	APIKeyAuth(
		ctx context.Context, key string, s *security.APIKeyScheme,
	) (context.Context, error)
}

const errSnap = Error("snap")

const (
	tk     = token.OurToken("<token>")
	email  = "<email>"
	userID = 21
)

var ctx = context.Background()

func ctxWithUserID(ctx context.Context) context.Context {
	return auth.SetContextUserID(ctx, userID)
}

func newLogger(w io.Writer) *log.Logger {
	return log.New(w, "", 0)
}
