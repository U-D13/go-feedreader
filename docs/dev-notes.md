# Development notes

> - - -
> As noted in the [main readme][README], all of this is a work in progress.
> - - -

In no particular order.


## Architecture

### A monolith

I don't really see any added value in splitting this into independent microservices at the moment. Besides a valuable experience of the numerous unpleasant learning opportunities from the complexity bloat that doing so would entail.

Right now the HTTP API is more or less RESTful. Nudging the microservices apart would unavoidably call for some more complex indirection to keep it that way. Or a redesign, which might be not such a bad thing but would take time and work.

Bottom line: there are no operational requirements currently that would benefit from going distributed micro. Therefore, not a priority.

### RESTish

Clients, such as a GUI or a CLI frontend, interact with the backend via a mostly RESTful API. Resources can be queried or manipulated by sending stateless HTTP requests with appropriate method verbs; the responses have meaningful status codes, obviating the need for message bodies in some cases.

The API doesn't qualify as canonically RESTful, as the responses are minimal and don't include links or other metadata (e.g., HAL), as would be required for full HATEOAS capability. Instead, it is documented via an OpenAPI (formerly, Swagger) description.

The OpenAPI description is provided at the API root as a JSON or YAML document (`/api/openapi.json` or `/api/openapi.yaml`, respectively). Human-readable OpenAPI documentation is rendered at the API root. By default, it is accessible at one of these URLs, depending on the server ID parameter:

- `dev` server: <http://feedreader.localhost:8080/api>
- `devs` server: <https://feedreader.localhost:4430/api>
- `prod` server: <https://feedreader.localhost/api>


## Security

### Authentication

The API endpoints that require authentication, take [rJWT] tokens. Such a token can be acquired by providing valid credentials in a token creation request (a `POST` to the `/api/token` endpoint). If invalid credentials have been provided, the response is delayed for 5 seconds.

The type of token used by the API is to be treated as an opaque implementation detail by all external clients. The current use of rJWT may be supplanted by another, incompatible and/or encrypted, token format. One idea is to use raw binary encoding or `protobuf`.

### rJWT

An rJWT ("reduced JWT") is similar to a full [JWT] in that its base64-encoded JSON payload and signature are separated by a dot, but, unlike JWT, an rJWT does not have a header and it is always signed with the `HS256` (HMAC using SHA-256) algorithm.

This results in a smaller token string: with the current payload structure, it can be at most 114 bytes long. A JWT with the same payload would take 151 bytes, almost 1/3 more.


## Framework

### Transport

The service ↔ transport glue is implemented as a [Goa] design.

Not having to write all that verbose and boilerplate-y request routing, service dispatch, and entity/data mapping code might have saved some time. I can't tell for sure as I didn't have to deal with any of it; Goa handled all of it for me.

On the other hand, the current design feels clumsy and awkward, since, in more instances than not, a Goa service implementation is just a wrapper around the real core service, which operates with the intended data and error types.

#### Gripes with Goa

1. There is no timestamp attribute type (mapping to `time.Time` fields in Go `struct`s).

	Despite both [protobuf's `Timestamp`][protobuf.Timestamp] and Go's [`time.Time`] having built-in support for mapping between each of them and an [RFC 3339 §5.6] string for JSON, which is also defined as the `date-time` format in the [OpenAPI specification][oas] ([v3.0][oas-3.0] too) and the [JSON Schema Validation vocabulary][json-schema].

	A [format very similar to RFC 3339][js-date] is also supported by the `Date` object prototype in all major JavaScript environments (see the [`Date` constructor][js-date-ctor] and [`Date.prototype.toJSON()`][date-toJSON]), although JavaScript, drawing inspiration from Java of all things, drops the ball here and has the resolution limited to mere milliseconds.

2. There is no support for defining [authentication schemes] besides `Basic`, a hard-coded JWT flavor of `Bearer`, and `OAuth`.

	This has been currently hacked around by using an `apiKey` `in`-`header` [security scheme].

3. There is no (un-)wrapping support for custom error types.

4. API server host/domain names must be hardcoded in the design spec.

	This prohibits alternative configurations without re-generating and recompiling the entire server.

Some of this may be worked around with custom plugins for Goa. An option that could be investigated at some point later.

My limited experience in designing services with Goa could also be a significant factor contributing to the observable awkwardness. Once more urgent issues are resolved, improving the Goa design specification is another task to be looked into.


### Storage

What little storage adapter SQL logic there is, it is generated with `sqlc`. I don't really see how I could be any happier than I am about that right now. ORMs tend towards the suckier side, and GORM is just like that. Ent does look better from afar, but `sqlc` feels sufficient at the moment.

A NoSQL storage adapter could be implemented at some point to expose the flaws in the current abstraction design and expedite their mitigation.

At the moment, all of the implemented storage adapters (and their respective driver dependencies) get imported and built, but it should eventually be relied on the build tags to only build specific adapters, thus reducing the size of the deployable binary.

#### Migration

Current implementation uses [`golang-migrate`] only to migrate up.


## Other features

Things that are, might, should, or will be implemented or supported.

### Server

#### Configuration

The configuration is currently implemented via CLI parameters. Support for environment variables is the next priority, followed by a configuration file. As a matter of fact, that would also be the order of precedence when all of those options are available: hard-coded defaults < config file < environment < CLI params.

#### CLI

The `cmd` packages are quite a mess at this time, and some overhaul is anticipated.

#### Web UI server

Also a mess, also would benefit from some extensive refactoring (more like a partial rewrite, I suspect).

#### gRPC

The binary `protobuf`-based gRPC protocol is a less resource-intensive alternative to the text-based JSON-over-HTTP for RPC APIs. It should get support at some point in the near future.


### Client

#### Library

The Goa-generated package for each public API service is at `github.com/antichris/go-feedreader/goa/gen/http/`*service*`/client`.

#### CLI

Also generated by Goa, lives at `github.com/antichris/go-feedreader/cmd/feedreader-cli`.

#### Web UI

The Angular frontend mostly works now. Its source will be released at some point later, when it is in a more solid state.

There may or may not be alternative implementations eventually, such as VueJS or maybe even Flutter. An interesting sidequest could be implementing an SSR UI client in native Go. Or Fresh on Deno for even more funsies.

The web UI could probably also be beaten into a proper PWA eventually, but that would already be an altogether different application (or, at least, architecture).

#### Desktop

A native desktop client could be a fun challenge.

I'm leaning towards wxWidgets or GTK. Although, from what I've seen so far, Go bindings for the former are somewhat sketchy, and I've only heard of projects migrating from the latter to Qt, not the other way around.

An Electron-type setup is unlikely to be ever considered, as that is by no means what I'd call "native". All it is is web assets bundled with a JS runtime and a *blasted web browser*. Hey, why don't you throw in an Android emulator while you're at it, hmm? /s


## Testing

### Coverage

Strive for that perfect 100%. With mutation testing, of course, as a 100% coverage without mutation testing is meaningless. At this time, though, only the smallest packages (e.g., `auth`, `terror`) have seen (and passed) some mutation testing.

![Coverage tree-map](coverage.svg)

There are some spots where it is very difficult or even impossible to get coverage. Some of those should and will be redesigned into oblivion. Some will inevitably stick around, like those "this cannot happen" panics.

The `cmd` packages at this point have a sketchy test coverage at best. This will be mitigated going forward as their design is improved.


### Table-driven

Unless it makes more sense to do otherwise, table-driven tests approximate this pattern:

```go
t.Parallel()
// [shared constants]
// [shareable (thread-safe) values]
for n, tt := range map[string]struct {
	// <test case-unique fields>
}{"<test case name>": {
	// <test case defining field values>
}, "...": {
	// [... rinse and repeat...]
}, "nominal": {
	// <the ideal "happy-path" case>
}} {
	tt := tt
	t.Run(n, func(t *testing.T) {
		t.Parallel()
		// [non-thread-safe values]
		// [dependency and mock object initialization]
		// [mock expectation setup]
		// execution
		// assertions on results/output
	})
}

```

More complex tests with many assertions should get some more descriptive assertion messages to help pinpoint the failure (if one occurs). Right now this is an aspirational goal, and it doesn't have a high priority, but that first part could change in the future.


### Mocks

Right now tests use both [`testify/mock`] and [`gomock`] mocks. Because, although it is very easy to generate mocks from existing interfaces with `gomock`, slapping together a quick and dirty one by hand takes way less ceremony with `testify/mock`. Both packages complement each other in practice, as they both have their own specific strengths and weaknesses.


[README]: ../README.md
	"The Feed Reader README"

[rJWT]: #rjwt
	"Development notes ❭ Security ❭ rJWT"
[JWT]: https://jwt.io/
	"JSON Web Tokens - jwt.io"

[Goa]: https://goa.design/
	"Goa :: Design first."
[protobuf.Timestamp]: https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.Timestamp
	"Package google.protobuf  |  Protocol Buffers  |  Google Developers"
[`time.Time`]: https://pkg.go.dev/time#Time.MarshalJSON
	"time package - time - Go Packages"
[RFC 3339 §5.6]:https://www.rfc-editor.org/rfc/rfc3339.html#section-5.6
	"RFC 3339: Date and Time on the Internet: Timestamps"
[oas]: https://spec.openapis.org/oas/latest.html#data-types
	"OpenAPI Specification v3.1.0 | Introduction, Definitions, & More"
[oas-3.0]: https://spec.openapis.org/oas/v3.0.3.html#data-types
	"OpenAPI Specification v3.0.3 | Introduction, Definitions, & More"
[json-schema]: https://json-schema.org/draft/2020-12/json-schema-validation.html#name-dates-times-and-duration
	"JSON Schema Validation: A Vocabulary for Structural Validation of JSON"
[js-date]: https://tc39.es/ecma262/#sec-date-time-string-format
	"ECMAScript® 2023 Language Specification"
[js-date-ctor]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date#date_string
	"Date() constructor - JavaScript | MDN"
[date-toJSON]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toJSON#browser_compatibility
	"Date.prototype.toJSON() - JavaScript | MDN"
[authentication schemes]: https://www.iana.org/assignments/http-authschemes/http-authschemes.xhtml
	"Hypertext Transfer Protocol (HTTP) Authentication Scheme Registry"
[security scheme]: https://swagger.io/specification/#security-scheme-object
	"OpenAPI Specification - Version 3.0.3 | Swagger"

[`golang-migrate`]: https://github.com/golang-migrate/migrate
	"golang-migrate/migrate: Database migrations. CLI and Golang library."

[`testify/mock`]: https://pkg.go.dev/github.com/stretchr/testify/mock
	"mock package - github.com/stretchr/testify/mock - Go Packages"
[`gomock`]: https://pkg.go.dev/github.com/golang/mock/gomock
	"gomock package - github.com/golang/mock/gomock - Go Packages"
