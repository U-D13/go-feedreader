// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"log"

	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/goa/gen/user"
	userPkg "github.com/antichris/go-feedreader/user"
	"github.com/antichris/go-feedreader/user/registration"
	"goa.design/goa/v3/security"
)

var _ user.Service = (*usersrvc)(nil)

// User service example implementation.
// The example methods log the requests and return zero values.
type usersrvc struct {
	logger *log.Logger
	auth   auth.Service
	sto    userPkg.TxableStorage
	mailer registration.Mailer
	mtg    mailTokenGenerator
}

// APIKeyAuth implements the authorization logic for service "User" for the
// "rJWT" security scheme.
func (s *usersrvc) APIKeyAuth(
	ctx context.Context, key string, scheme *security.APIKeyScheme,
) (context.Context, error) {
	return s.auth.APIKeyAuth(ctx, key, userUnauthErr, userBadReqErr)
}

// Check whether an email address can be used to create a new user.
func (s *usersrvc) IsEmailAvailableForNew(
	ctx context.Context, p string,
) (err error) {
	s.logger.Print("user.IsEmailAvailableForNew")
	_, err = s.sto.GetByEmail(ctx, p)
	if err != nil {
		if err == userPkg.ErrNotFound {
			return nil
		}
		return
	}
	return &user.ConflictError{}
}

// Create a new user.
func (s *usersrvc) Create(ctx context.Context, p *user.CreateUser) (err error) {
	s.logger.Print("user.Create")
	h, err := s.auth.HashPassword([]byte(p.Password))
	if err != nil {
		return
	}

	// TODO More fluent transaction design (e.g. with closure).
	st, err := s.sto.WithSQLTx(ctx, nil)
	if err != nil {
		return
	}
	defer rollback(st)

	token, err := s.mtg.Generate()
	if err != nil {
		return
	}

	if err = st.Create(
		ctx,
		p.Email, string(h), p.FirstName, p.LastName, token,
	); err != nil {
		// TODO Respond with `ConflictError` on UNIQUE violation.
		return
	}

	u := userPkg.User{
		FirstName: p.FirstName,
		LastName:  p.LastName,
		Email:     p.Email,
	}
	// TODO Make mailing asynchronous.
	if err = s.mailer.Mail(u.EmailAddr(), u.FirstName, token); err != nil {
		err = fmt.Errorf("confirm registration: %w", err)
		return
	}

	return st.Commit()
}

// Activate user account using an email confirmation token.
func (s *usersrvc) Activate(
	ctx context.Context, p *user.ActivatePayload,
) (err error) {
	s.logger.Print("user.Activate")

	st, err := s.sto.WithSQLTx(ctx, nil)
	if err != nil {
		return
	}
	defer rollback(st)

	u, err := s.sto.GetByEmailToken(ctx, *p.Token)
	if err != nil {
		if errors.Is(err, userPkg.ErrNotFound) {
			err = errUserNotFound
			return
		}
		return
	}
	if u.Status == userPkg.Active {
		// TODO Yell for reusing used token.
		return
	}
	if err = s.sto.SetStatus(ctx, u.ID, userPkg.Active); err != nil {
		return
	}
	return st.Commit()
}

// Show the record of the requesting user.
func (s *usersrvc) Introspect(
	ctx context.Context, p *user.IntrospectPayload,
) (res *user.ShowUser, err error) {
	s.logger.Print("user.Introspect")
	id, err := s.contextUserID(ctx)
	if err != nil {
		return
	}
	return s.show(ctx, id)
}

// Show the record of a given user.
func (s *usersrvc) Show(
	ctx context.Context, p *user.ShowPayload,
) (res *user.ShowUser, err error) {
	s.logger.Print("user.Show")
	id, err := s.contextUserID(ctx)
	if err != nil {
		return
	}
	if uint32(p.ID) != id {
		err = unauthorizedErr("You can only view your own user data")
		return
	}
	return s.show(ctx, id)
}

func (s *usersrvc) contextUserID(ctx context.Context) (id uint32, err error) {
	id, err = auth.ContextUserID(ctx)
	if err != nil {
		s.logger.Print(err.Error())
		err = userInternalErr(err)
	}
	return
}

func (s *usersrvc) show(
	ctx context.Context, id uint32,
) (res *user.ShowUser, err error) {
	u, err := s.sto.GetByID(ctx, int32(id))
	if err != nil {
		if errors.Is(err, userPkg.ErrNotFound) {
			err = errUserNotFound
			return
		}
		return
	}
	res = &user.ShowUser{}
	res.CreateFromUser(&u)
	res.Status = uint32(u.Status)
	res.CreatedAt = user.ItemDate(u.CreatedAtString())
	res.UpdatedAt = user.ItemDate(u.UpdatedAtString())
	return
}

var errUserNotFound = &user.NotFoundError{Message: userPkg.ErrNotFound.Error()}

func userUnauthErr(e error) error { return unauthorizedErr(e.Error()) }

func userBadReqErr(e error) error {
	return &user.BadRequestError{Message: e.Error()}
}

func userInternalErr(e error) error {
	return &user.InternalError{Message: e.Error()}
}

func unauthorizedErr(ms string) error {
	return &user.UnauthorizedError{Message: ms}
}

// rollback the tx.
//
// If invoked deferred during a panic, this re-panics with the recovered
// value.
// Otherwise, if the rollback fails for a reason other than
// [sql.ErrTxDone], this panics with that error.
//
// If a panic was already recovered before failing rollback, this panics
// with an error that includes the string representation of the
// recovered panic value in its message and wraps the rollback error.
func rollback(tx userPkg.Tx) {
	r := recover()
	err := tx.Rollback()
	if err != nil {
		if errors.Is(err, sql.ErrTxDone) {
			return
		}
		if r != nil {
			panic(fmt.Errorf("rollback after %v: %w", r, err))
		}
		panic(err)
	}
	if r != nil {
		panic(r)
	}
}

// mailTokenGenerator generates email confirmation tokens.
type mailTokenGenerator interface {
	// Generate the email confirmation tokens
	Generate() (token string, err error)
}

var _ mailTokenGenerator = readerMailTokenGen{}

// readerMailTokenGen is an implementation of [mailTokenGenerator] that
// reads 32 bytes from an [io.Reader] and returns the string they form
// as an email confirmation token.
type readerMailTokenGen struct{ r io.Reader }

func (g readerMailTokenGen) Generate() (token string, err error) {
	b := make([]byte, 32)
	if _, err = g.r.Read(b); err != nil {
		return
	}
	token = string(b)
	return
}
