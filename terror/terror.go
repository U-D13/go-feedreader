// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package terror implements a type of string error that is suitable for
// constant definitions.
//
// I got fed up with copying the type definition and implementation,
// hence this package(let).
//
// [Error.Unique] makes it possible to turn constant [Error]'s into
// plain old (typed) string pointer errors that behave pretty much
// identically to the (unexported) errorString struct pointers returned
// by the standard library [errors.New].
package terror

// Error is a type of string that formats as its value and can be used
// in constant definitions.
//
// Be aware that identical string constants yield identical error
// constants.
type Error string

// Error implements the error interface.
func (e Error) Error() string { return string(e) }

// Unique returns a pointer to a unique copy of the error message string.
//
// This can be useful when refactoring from [Error] constants to
// variables: after replacing "const" with "var", simply slap a Unique()
// call at the end.
func (e Error) Unique() *Error { return (*Error)(&e) }
