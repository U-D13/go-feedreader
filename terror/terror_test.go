// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package terror_test

import (
	"fmt"

	. "github.com/antichris/go-feedreader/terror"
)

func ExampleError() {
	const (
		snap = "snap"
		// err is a constant.
		err = Error(snap)
	)
	fmt.Println(
		// Can be cast to string to get its message.
		string(err) == err.Error(),
		// Can be compared to a string constant directly.
		err == snap,
		err == "snap",
		// Consequentially, identical string yields identical error.
		err == Error("snap"),
	)

	// Output:
	// true true true true
}

func ExampleError_Unique() {
	const (
		snap = "snap"
		err1 = Error(snap)
		err2 = Error("snap")
	)
	fmt.Println(
		// Identical strings yield identical errors.
		err1 == err2,
		// But when Unique is called, the results really are unique.
		err1.Unique() == err2.Unique(),
		//lint:ignore SA4000 Expressions appear identical, but values are not.
		err1.Unique() == err1.Unique(),
	)

	// Each new call of Unique yields a new unique value.
	err3 := err2.Unique()
	err4 := err2.Unique()
	err5 := err4.Unique()
	fmt.Println(
		err3 == err4,
		err4 == err5,
		err3, err4, err5,
	)

	// The underlying string value can still be compared and found equal.
	fmt.Println(*err5 == err1)

	// Output:
	// true false false
	// false false snap snap snap
	// true
}
