// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package mail provides tools for sending emails.
package mail

// Mailer sends emails.
type Mailer interface {
	// SendMail with the given subject, plain-text and/or HTML bodies
	// to the given recipients.
	SendMail(to []string, subject string, plainBody, htmlBody []byte) error
	// Send the given [Email] e to its recipients.
	Send(e Email) error
}
type (
	// EmailAddr is an email address string.
	EmailAddr string
	// User is the username used in authentication on an SMTP server.
	User string
	// Pass is the password used in authentication on an SMTP server.
	Pass string
	// Host is the host name of an SMTP server.
	Host string
	// Port can be a number or an Internet service name as defined in
	// `/etc/services` (see services(5) manpage for details).
	//
	// For example:
	//
	//  Port("587")
	//  Port("submission")
	Port string
)
