// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail

import (
	"bytes"
	"io"
	"net/smtp"
)

// Composer writes an [Email] to an [io.Writer].
type Composer interface {
	// Compose writes [Email] e to [io.Writer] w.
	Compose(w io.Writer, e Email) error
}

type (
	// mailer is an implementation of [Mailer]
	mailer struct {
		DefaultFrom EmailAddr
		serverAddr
		smtp.Auth
		Composer
	}
	// serverAddr is a mail server address including a port, as
	// in "mail.example.com:smtp".
	serverAddr string
)

// SendMail with the given subject, plain-text and/or HTML bodies
// to the given recipients.
func (m *mailer) SendMail(
	to []string, subject string, plainBody, htmlBody []byte,
) error {
	e := Email{
		Subject: subject,
		Text:    plainBody,
		HTML:    htmlBody,
	}
	if err := e.SetTo(to); err != nil {
		return err
	}
	return m.Send(e)
}

// Send the given [Email] e to its recipients.
func (m *mailer) Send(e Email) error {
	if e.From == nil {
		if err := e.SetFrom(string(m.DefaultFrom)); err != nil {
			return err
		}
	}
	b := &bytes.Buffer{}
	if err := m.Compose(b, e); err != nil {
		return err
	}
	// TODO Use CA cert from config.
	return sendMail(
		string(m.serverAddr),
		m.Auth,
		e.From.Address,
		e.ToAddresses(),
		b.Bytes(),
	)
}

// sendMail connects to the server at addr, switches to TLS if possible,
// authenticates with the optional mechanism a if possible, and then
// sends an email from address from, to addresses to, with message msg.
// The addr must include a port, as in "mail.example.com:smtp".
//
// This is an alias for [smtp.SendMail], see that for details.
var sendMail = smtp.SendMail
