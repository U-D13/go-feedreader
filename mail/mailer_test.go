// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail_test

import (
	"io"
	"net/mail"
	"net/smtp"
	"testing"

	"github.com/antichris/go-feedreader/internal/util/testu"
	. "github.com/antichris/go-feedreader/mail"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func Test_mailer_SendMail(t *testing.T) {
	t.Parallel()
	const (
		host = "localhost"
		port = "1025"
		user = "mailer"
		pass = "password"
		addr = host + ":" + port

		badAddr = "foo"

		fromName = "Mailer"
		fromAddr = "mailer@feedreader"
		fromRaw  = fromName + " <" + fromAddr + ">"

		toName = "Foo Bar"
		toAddr = "user@localhost"
		toRaw  = toName + " <" + toAddr + ">"

		to2Name = "Qux Corge"
		to2Addr = "qux@localhost"
		to2Raw  = to2Name + "<" + to2Addr + ">"

		subject = "Subject"
	)
	var (
		auth  = smtp.PlainAuth("", user, pass, host)
		msg   = []byte("msg")
		plain = []byte("plain 🫠")
		html  = []byte("<h1>HTML 👍</h1>")

		from = mail.Address{Name: fromName, Address: fromAddr}

		to  = mail.Address{Name: toName, Address: toAddr}
		to2 = mail.Address{Name: to2Name, Address: to2Addr}

		tos     = []string{toAddr}
		toSlice = Addresses{{Address: toAddr}}
	)
	newEmail := func(to ...*mail.Address) Email {
		return Email{
			From:    &from,
			To:      to,
			Subject: subject,
			Text:    plain,
			HTML:    html,
		}
	}
	newMailer := func(defaultFrom EmailAddr, c Composer) Mailer {
		return XnewMailer(defaultFrom, host, port, user, pass, c)
	}
	for n, tt := range map[string]struct {
		from    string
		to      []string
		wEmail  Email
		compErr error
		wTo     []string
		sendErr error
		wErr    string
	}{"empty To": {
		wErr: "no addresses",
	}, "bad to": {
		to:   []string{badAddr},
		wErr: "missing '@' or angle-addr",
	}, "empty from": {
		to:   tos,
		wErr: "no address",
	}, "bad from": {
		from: badAddr,
		to:   tos,
		wErr: "missing '@' or angle-addr",
	}, "compose error": {
		from:    fromRaw,
		to:      tos,
		wEmail:  newEmail(toSlice...),
		compErr: errSnap,
		wErr:    string(errSnap),
	}, "send error": {
		from:    fromRaw,
		to:      tos,
		wEmail:  newEmail(toSlice...),
		wTo:     tos,
		sendErr: errSnap,
		wErr:    string(errSnap),
	}, "nominal": {
		from:   fromRaw,
		to:     []string{toRaw},
		wEmail: newEmail(&to),
		wTo:    tos,
	}, "multiple recipients": {
		from:   fromRaw,
		to:     []string{toRaw, to2Raw},
		wEmail: newEmail(&to, &to2),
		wTo:    []string{toAddr, to2Addr},
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			c := newMockComposer(t)
			c.OnCompose(tt.wEmail).
				WriteMessage(msg).
				Return(tt.compErr)
			sendMail, m := newMockSendMail(t)
			m.On(addr, auth, fromAddr, tt.wTo, msg).
				Return(tt.sendErr)

			defaultFrom := EmailAddr(tt.from)
			mailer := newMailer(defaultFrom, c)

			var err error
			XwithSendMail(sendMail, func() {
				err = mailer.SendMail(tt.to, subject, plain, html)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.ErrorContains(err, tt.wErr)
				return
			}
			r.NoError(err)
			mock.AssertExpectationsForObjects(t, c, m)
		})
	}
}

func Test_mailer_SendMail_unmocked(t *testing.T) {
	t.Parallel()
	const (
		host = "localhost"
		port = "1025"
		user = "mailer"
		pass = "password"
		addr = host + ":" + port

		from    = "Mailer <mailer@feedreader>"
		subject = "Subject"
	)
	var (
		to = []string{
			"Foo Bar <user@localhost>",
			"Qux Corge <qux@localhost>",
		}
		plain = []byte("plain 🫠")
		html  = []byte("<h1>HTML 👍</h1>")
	)
	sendMail, m := newMockSendMail(t)
	m.On(addr, mock.Anything, "mailer@feedreader", []string{
		"user@localhost", "qux@localhost",
	}, mock.Anything).
		Return(nil)

	mailer := NewMailer(from, host, port, user, pass)

	var err error
	XwithSendMail(sendMail, func() {
		err = mailer.SendMail(to, subject, plain, html)
	})

	require.NoError(t, err)
	m.AssertExpectations(t)
}

func newMockComposer(t *testing.T) *mockComposer {
	return &mockComposer{testu.NewMock(t), t}
}

var _ Composer = (*mockComposer)(nil)

type mockComposer struct {
	*mock.Mock
	t *testing.T
}

func (m *mockComposer) Compose(w io.Writer, e Email) error {
	m.t.Helper()
	return m.Called(w, e).Error(0)
}

func (m *mockComposer) OnCompose(wEmail Email) *composeCall {
	c := m.On("Compose", mock.Anything, mock.MatchedBy(func(e Email) bool {
		return assert.Equal(m.t, wEmail, e)
	}))
	return &composeCall{c, m.t}
}

type composeCall struct {
	*mock.Call
	t *testing.T
}

func (c *composeCall) WriteMessage(msg []byte) *composeCall {
	c.t.Helper()
	r := require.New(c.t)
	c.Run(func(args mock.Arguments) {
		w, ok := args.Get(0).(io.Writer)
		r.True(ok)
		_, err := w.Write(msg)
		r.NoError(err)
	})
	return c
}

func newMockSendMail(t *testing.T) (XsendMailFunc, *testu.FuncMock) {
	t.Helper()
	m := testu.NewFuncMock(t, "sendMail")
	sendMail := func(
		addr string, a smtp.Auth, from string, to []string, msg []byte,
	) error {
		return m.Called(addr, a, from, to, msg).Error(0)
	}
	return sendMail, m
}
