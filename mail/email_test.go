// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail_test

import (
	"net/mail"
	"testing"
	"time"

	. "github.com/antichris/go-feedreader/mail"
)

func TestEmail_Headers(t *testing.T) {
	t.Parallel()
	type (
		e = Email
		h = mail.Header
		w = wantHeader
	)
	const (
		bnd  = "boundary"
		text = "Text"
		html = "HTML"
		addr = "<@>"

		subject = "<subject>"
	)
	from := &mail.Address{Name: "user", Address: "user@host.domain"}

	wFrom := func(e Email) Email {
		e.From = from
		return e
	}
	wHFrom := func(w mail.Header) mail.Header {
		if w == nil {
			w = make(mail.Header, 1)
		}
		w[hFrom] = append(w[hFrom], from.String())
		return w
	}
	wPlain := func(w wantHeader) (r wantHeader) {
		if w.mediaType == r.mediaType {
			w.mediaType = mimePlain
		}
		w.typeParams = paramsWithUTF8(w.typeParams)
		w.Header = wHFrom(w.Header)
		return w
	}
	for n, tt := range map[string]struct {
		e    Email
		want wantHeader
	}{"no body": {
		e:    wFrom(e{}),
		want: wPlain(w{}),
	}, "plain": {
		e: wFrom(e{
			Text: []byte(text),
		}),
		want: wPlain(w{}),
	}, "HTML": {
		e: wFrom(e{
			HTML: []byte(html),
		}),
		want: w{
			mediaType:  mimeHTML,
			typeParams: paramsWithUTF8(nil),
			Header:     wHFrom(nil),
		},
	}, "multipart": {
		e: wFrom(e{
			Text: []byte(text),
			HTML: []byte(html),
		}),
		want: w{
			mediaType: mimeMultipartAlt,
			typeParams: typeParams{
				"boundary": bnd,
			},
			Header: wHFrom(nil),
		},
	}, "subject": {
		e: wFrom(e{
			Subject: subject,
		}),
		want: wPlain(w{Header: h{
			hSubject: {subject},
		}}),
	}, "unnamed To": {
		e: wFrom(e{
			To: Addresses{{Address: "user@host"}},
		}),
		want: wPlain(w{}),
	}, "named To": {
		e: wFrom(e{
			To: Addresses{{Name: "user1"}, {Name: "user2"}},
		}),
		want: wPlain(w{Header: h{
			hTo: {`"user1" ` + addr + `, "user2" ` + addr},
		}}),
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := tt.e.Headers(bnd)
			requireMessageHeaders(t, tt.want, got, time.Now())
		})
	}
}
