// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"io"
	"math/rand"
	"mime/multipart"
	"mime/quotedprintable"
	"net/http"
	"net/mail"
	"net/textproto"
	"sync"
	"time"
)

var _ Composer = MultipartComposer{}

// MultipartComposer is limited to a maximum of 2 alternative parts, a
// plaintext and an HTML one.
type MultipartComposer struct{ Boundaryer }

// Compose and write to [io.Writer] w text representation of the [Email]
// e.
//
// If both plaintext and HTML parts have content, a
// multipart/alternative message is written.
// If only the HTML part has content, a non-multipart text/html email is
// written.
// Otherwise a non-multipart text/plain message is written.
func (c MultipartComposer) Compose(w io.Writer, e Email) error {
	if e.HasPlain() && e.HasHTML() {
		return c.composeMulti(w, e)
	}
	return compose(w, e)
}

func (c MultipartComposer) composeMulti(w io.Writer, e Email) (err error) {
	m := multipart.NewWriter(w)
	if c.Boundaryer != nil {
		if err = m.SetBoundary(c.Boundary()); err != nil {
			return err
		}
	}
	boundary := m.Boundary()
	h := e.Headers(boundary)
	if err = writeHeader(w, h); err != nil {
		return
	}
	if err = writePart(m, mimePlain, e.Text); err != nil {
		return
	}
	if err = writePart(m, mimeHTML, e.HTML); err != nil {
		return
	}
	return m.Close()
}

func compose(w io.Writer, e Email) error {
	h := e.Headers("")
	textproto.MIMEHeader(h).Set(hCTE, eQuotedPrintable)
	if err := writeHeader(w, h); err != nil {
		return err
	}
	q := quotedprintable.NewWriter(w)
	if _, err := q.Write(e.Text); err != nil {
		return err
	}
	if _, err := q.Write(e.HTML); err != nil {
		return err
	}
	return q.Close()
}

func writeHeader(w io.Writer, h mail.Header) error {
	if err := http.Header(h).Write(w); err != nil {
		return err
	}
	_, err := w.Write([]byte(CRLF))
	return err
}

func writePart(
	m *multipart.Writer, mediaType string, p []byte,
) (err error) {
	w, err := m.CreatePart(textproto.MIMEHeader{
		hContentType: {mediaType + "; charset=utf-8"},
		hCTE:         {eQuotedPrintable},
	})
	if err != nil {
		return err
	}
	qw := quotedprintable.NewWriter(w)
	if _, err := qw.Write(p); err != nil {
		return err
	}
	return qw.Close()
}

const CRLF = "\r\n"

// Boundaryer generates unique values that are suitable for use as
// MIME message multipart boundaries.
type Boundaryer interface {
	// Boundary is a string that can be used to uniquely identify and
	// separate parts of a multipart MIME message.
	Boundary() string
}

var _ Boundaryer = (*Base64Boundaryer)(nil)

// Base64Boundaryer is an implementation of [Boundaryer] that generates
// base64 encoded pseudo-random MIME multipart boundary values.
type Base64Boundaryer struct {
	bytes.Buffer
	rand.Source
	mu sync.Mutex
}

// Boundary is a base64 encoded 128 bit value consisting of a random
// integer and current Unix timestamp in nanoseconds.
func (b *Base64Boundaryer) Boundary() string {
	now := time.Now()
	// TODO Consider using [sync.Pool] instead to avoid locking.
	b.mu.Lock()
	defer b.mu.Unlock()
	b.Reset()

	e := base64.NewEncoder(base64.RawURLEncoding, b)
	o := binary.BigEndian
	// There is no way an error can occur here.
	// [binary.Write] only generates an error if it has been passed a
	// funky type of data, but we only pass int64's here.
	// Other than that, it could pass the error of e.Write(), but, as
	// [base64.encoder.Write] never generates errors of its own, it
	// could only pass the error of its Writer, which is
	// [bytes.Buffer.Write] here, but that *always* returns a nil error.
	_ = binary.Write(e, o, b.Int63())
	_ = binary.Write(e, o, now.UnixNano())
	// Just as above, an error cannot occur because [bytes.Buffer.Write]
	// never returns an error.
	_ = e.Close()

	return b.String()
}
