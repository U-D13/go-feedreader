// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail

import (
	"fmt"
	"net/mail"
	"net/textproto"
	"strings"
	"time"

	"github.com/antichris/go-feedreader/terror"
)

type (
	Addresses = []Address
	Address   = *mail.Address
)

// Email represents an email message.
type Email struct {
	From    Address
	To      Addresses
	Subject string
	Text    []byte
	HTML    []byte
}

// HasPlain returns whether e has a plaintext part.
func (e Email) HasPlain() bool { return len(e.Text) != 0 }

// HasHTML returns whether e has an HTML part.
func (e Email) HasHTML() bool { return len(e.HTML) != 0 }

// Headers are the MIME headers for e.
func (e Email) Headers(boundary string) mail.Header {
	now := time.Now()
	h := textproto.MIMEHeader{}
	// XXX Set/Add methods alter the key case, this preserves.
	h[hMIMEVersion] = []string{"1.0"}
	h.Set(hContentType, e.ContentType(boundary))
	// TODO When From is nil, error out instead of letting it panic here.
	// The mailer implementation does take care ensuring it is always
	// set, but this would become a problem if e was used outside that.
	h.Set(hFrom, e.From.String())
	h.Set(hDate, now.Format(time.RFC1123Z))
	if len(e.Subject) != 0 {
		h.Set(hSubject, e.Subject)
	}
	if e.NamedTo() {
		h.Set(hTo, e.ToAddressList())
	}
	return mail.Header(h)
}

// ContentType is the MIME content type of e.
func (e Email) ContentType(boundary string) string {
	hasPlain, hasHTML := e.HasPlain(), e.HasHTML()
	// TODO Consider using `mime.FormatMediaType()`.
	const cs = "; charset=utf-8"
	switch {
	case hasPlain && hasHTML:
		return mimeMultipartAlt + "; boundary=" + boundary
	case hasHTML:
		return mimeHTML + cs
	}
	return mimePlain + cs
}

// NamedTo returns whether any of [Email].To addresses has a Name.
func (e Email) NamedTo() bool {
	for _, v := range e.To {
		if v.Name != "" {
			return true
		}
	}
	return false
}

// ToAddressList returns [Email].To addresses as RFC-5322 address-list
// string.
func (e Email) ToAddressList() string {
	b := &strings.Builder{}
	for i, v := range e.To {
		if i > 0 {
			b.WriteString(", ")
		}
		b.WriteString(v.String())
	}
	return b.String()
}

// ToAddresses returns a slice of [Email].To Address part values.
func (e Email) ToAddresses() []string {
	s := make([]string, 0, len(e.To))
	for _, v := range e.To {
		s = append(s, v.Address)
	}
	return s
}

// SetFrom parses and sets the given from string as the "From" [Address]
// for e.
func (e *Email) SetFrom(from string) error {
	a, err := mail.ParseAddress(from)
	if err != nil {
		return err
	}
	e.From = a
	return nil
}

// SetFrom parses and sets the given slice of to strings as the "To"
// [Address] for e.
func (e *Email) SetTo(to []string) error {
	l := len(to)
	if l == 0 {
		return ErrNoToAddresses
	}
	s := make(Addresses, 0, l)
	for _, v := range to {
		a, err := mail.ParseAddress(v)
		if err != nil {
			return fmt.Errorf("%w: %q", err, v)
		}
		s = append(s, a)
	}
	e.To = s
	return nil
}

// ErrNoToAddresses is returned when no recipient addresses are given.
const ErrNoToAddresses = terror.Error("cannot set 'To:': no addresses" +
	" given")

const (
	mimeHTML  = "text/html"
	mimePlain = "text/plain"

	mimeMultipartAlt = "multipart/alternative"

	eQuotedPrintable = "quoted-printable"
)

const (
	hContentType = "Content-Type"
	hCTE         = "Content-Transfer-Encoding"
	hDate        = "Date"
	hFrom        = "From"
	hMIMEVersion = "MIME-Version"
	hSubject     = "Subject"
	hTo          = "To"
)
