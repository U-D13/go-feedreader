// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail_test

import (
	"mime"
	"net/mail"
	"net/textproto"
	"testing"
	"time"

	. "github.com/antichris/go-feedreader/mail"
	"github.com/antichris/go-feedreader/terror"
	"github.com/stretchr/testify/require"
)

const errSnap = terror.Error("snap")

const (
	mimeHTML  = XmimeHTML
	mimePlain = XmimePlain

	mimeMultipartAlt = XmimeMultipartAlt

	eQuotedPrintable = XeQuotedPrintable
)

const (
	hContentType = XhContentType
	hCTE         = XhCTE
	hDate        = XhDate
	hFrom        = XhFrom
	hMIMEVersion = XhMIMEVersion
	hSubject     = XhSubject
	hTo          = XhTo
)

type (
	wantHeader struct {
		// The MIME media type expected in the Content-Type header.
		mediaType string
		// The exact MIME media type parameters expected in the
		// Content-Type header.
		typeParams
		// The other header fields expected to be present besides
		// "MIME-Version", "Content-Type" and "Date".
		mail.Header
		// Whether the MIME-Version header key is expected to have been
		// mangled into "Mime-Version".
		//
		// [mail.Header.Get] calls [textproto.MIMEHeader.Get] which,
		// along with the rest of [textproto.MIMEHeader] accessor and
		// mutator methods, use [textproto.CanonicalMIMEHeaderKey] to do
		// just that.
		//
		// RFC 2045 specifies the "MIME-Version" header using this exact
		// capitalization.
		canonicalized bool
	}
	typeParams = map[string]string
)

func paramsWithUTF8(p typeParams) typeParams {
	if p == nil {
		p = make(typeParams, 1)
	}
	p["charset"] = "utf-8"
	return p
}

func requireMessageHeaders(
	t *testing.T,
	want wantHeader,
	got mail.Header,
	now time.Time,
) {
	t.Helper()
	t.Run(hMIMEVersion, func(t *testing.T) {
		t.Parallel()
		t.Helper()
		const MIMEVer = "1.0"

		h := hMIMEVersion
		if want.canonicalized {
			h = textproto.CanonicalMIMEHeaderKey(h)
		}
		require.Subset(t, got, mail.Header{
			h: {MIMEVer},
		})
	})
	t.Run(hContentType, func(t *testing.T) {
		t.Parallel()
		t.Helper()
		v := got.Get(hContentType)
		m, p, err := mime.ParseMediaType(v)
		r := require.New(t)
		r.NoError(err, "%v: %q", hContentType, v)
		r.Equal(want.mediaType, m, "Media type in %q", v)
		r.Equal(want.typeParams, p, "Params in %q", v)
	})
	t.Run(hDate, func(t *testing.T) {
		t.Parallel()
		t.Helper()
		d, err := got.Date()
		r := require.New(t)
		r.NoError(err)
		r.WithinDuration(now, d, time.Second)
	})
	t.Run("other", func(t *testing.T) {
		t.Parallel()
		t.Helper()

		v := struct{}{}
		processed := map[string]struct{}{
			hMIMEVersion: v,
			hContentType: v,
			hDate:        v,
		}
		if want.canonicalized {
			for k, v := range processed {
				delete(processed, k)
				processed[textproto.CanonicalMIMEHeaderKey(k)] = v
			}
		}

		filter := func(got mail.Header) mail.Header {
			r := make(mail.Header, len(got)-len(processed))
			for k, v := range got {
				if _, ok := processed[k]; !ok {
					r[k] = v
				}
			}
			return r
		}

		require.Equal(t, want.Header, filter(got))
	})
}
