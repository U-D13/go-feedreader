// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail

import (
	"net/smtp"
	"sync"
)

const (
	XmimeHTML  = mimeHTML
	XmimePlain = mimePlain

	XmimeMultipartAlt = mimeMultipartAlt

	XeQuotedPrintable = eQuotedPrintable
)

const (
	XhContentType = hContentType
	XhCTE         = hCTE
	XhDate        = hDate
	XhFrom        = hFrom
	XhMIMEVersion = hMIMEVersion
	XhSubject     = hSubject
	XhTo          = hTo
)

var XnewMailer = newMailer

type Xmailer = mailer

// XwithSendMail executes f with the given sendMailFn.
//
// This is thread-safe.
func XwithSendMail(sendMailFn XsendMailFunc, f func()) {
	sendMailFnMu.Lock()
	defer sendMailFnMu.Unlock()
	defer func(v XsendMailFunc) { sendMail = v }(sendMail)
	sendMail = sendMailFn
	f()
}

type XsendMailFunc = func(
	addr string, a smtp.Auth, from string, to []string, msg []byte,
) error

var sendMailFnMu = sync.Mutex{}
