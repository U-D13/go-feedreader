// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mail_test

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"io"
	"math/rand"
	"mime/multipart"
	"net/mail"
	"testing"
	"time"

	. "github.com/antichris/go-feedreader/mail"
	"github.com/stretchr/testify/require"
)

func TestMultipartComposer_Compose(t *testing.T) {
	t.Parallel()
	const (
		bnd  = "boundary"
		cs   = "; charset=utf-8"
		text = "Text 👍\n"
		html = "HTML 👏\n"

		wText = "Text =F0=9F=91=8D\r\n"
		wHTML = "HTML =F0=9F=91=8F\r\n"
	)
	var (
		from = &mail.Address{Name: "user", Address: "user@host.domain"}

		ePlain = Email{
			From: from,
			Text: []byte(text),
		}
		eHTML = Email{
			From: from,
			HTML: []byte(html),
		}
		eMulti = Email{
			From: from,
			Text: []byte(text),
			HTML: []byte(html),
		}
	)
	wHFrom := func(w mail.Header) mail.Header {
		if w == nil {
			w = make(mail.Header, 1)
		}
		w[hFrom] = append(w[hFrom], from.String())
		return w
	}
	wQuotedPrintableCTE := func(w mail.Header) mail.Header {
		if w == nil {
			w = make(mail.Header, 1)
		}
		w[hCTE] = append(w[hCTE], eQuotedPrintable)
		return w
	}
	_, _ = ePlain, eHTML
	_, _ = wHFrom, wQuotedPrintableCTE
	type part struct {
		mail.Header
		content string
	}
	for n, tt := range map[string]struct {
		e           Email
		boundary    string
		writeErr    error
		errorOn     int
		errorOnCont string
		wHead       wantHeader
		wBody       string
		wParts      []part
		wErr        string
	}{"plain header write error": {
		e:           ePlain,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: hContentType,
		wErr:        errSnap.Error(),
	}, "plain text body write error": {
		e:           ePlain,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: wText,
		wErr:        errSnap.Error(),
	}, "plain HTML body write error": {
		e:           eHTML,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: wHTML,
		wErr:        errSnap.Error(),
	}, "bad boundary": {
		e:    eMulti,
		wErr: "invalid boundary length",
	}, "multipart/header write error": {
		e:           eMulti,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: hContentType,
		wErr:        errSnap.Error(),
	}, "multipart/text part create error": {
		e:           eMulti,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: hCTE,
		wErr:        errSnap.Error(),
	}, "multipart/text part write error": {
		e:           eMulti,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: wText,
		wErr:        errSnap.Error(),
	}, "multipart/HTML part write error": {
		e:           eMulti,
		boundary:    bnd,
		writeErr:    errSnap,
		errorOnCont: wHTML,
		wErr:        errSnap.Error(),
	}, "plain": {
		e:        ePlain,
		boundary: bnd,
		wHead: wantHeader{
			mediaType:  mimePlain,
			typeParams: paramsWithUTF8(nil),
			Header:     wQuotedPrintableCTE(wHFrom(nil)),
		},
		wBody: wText,
	}, "HTML": {
		e:        eHTML,
		boundary: bnd,
		wHead: wantHeader{
			mediaType:  mimeHTML,
			typeParams: paramsWithUTF8(nil),
			Header:     wQuotedPrintableCTE(wHFrom(nil)),
		},
		wBody: wHTML,
	}, "multipart": {
		e:        eMulti,
		boundary: bnd,
		wHead: wantHeader{
			mediaType: mimeMultipartAlt,
			typeParams: map[string]string{
				"boundary": bnd,
			},
			Header: wHFrom(nil),
		},
		wParts: []part{{
			Header: wQuotedPrintableCTE(mail.Header{
				hContentType: {mimePlain + cs},
			}),
			content: wText,
		}, {
			Header: wQuotedPrintableCTE(mail.Header{
				hContentType: {mimeHTML + cs},
			}),
			content: wHTML,
		}},
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			b := &bytes.Buffer{}
			w := &predicateErrWriter{
				predicate: func(p []byte) (err error) {
					if len(tt.errorOnCont) == 0 {
						return
					}
					if bytes.Contains(p, []byte(tt.errorOnCont)) {
						err = tt.writeErr
					}
					return
				},
				Writer: b,
			}
			bs := stubBoundaryer(func() string { return tt.boundary })

			now := time.Now()
			err := MultipartComposer{bs}.Compose(w, tt.e)

			r := require.New(t)
			if tt.wErr != "" {
				r.ErrorContains(err, tt.wErr)
				return
			}
			r.NoError(err)

			t.Run("message", func(t *testing.T) {
				t.Parallel()
				got, err := mail.ReadMessage(b)
				r.NoError(err)

				t.Run("header", func(t *testing.T) {
					t.Parallel()
					want := tt.wHead
					want.canonicalized = true
					requireMessageHeaders(t, want, got.Header, now)
				})
				t.Run("body", func(t *testing.T) {
					t.Parallel()
					r := require.New(t)
					if len(tt.wParts) == 0 {
						body, err := io.ReadAll(got.Body)
						r.NoError(err)
						r.Equal(tt.wBody, string(body))
						return
					}

					m := multipart.NewReader(got.Body, tt.boundary)
					parts := make([]part, len(tt.wParts))
					for i := 0; ; i++ {
						p, err := m.NextRawPart()
						if err == io.EOF {
							break
						}
						r.NoError(err)
						body, err := io.ReadAll(p)
						r.NoError(err)
						parts[i] = part{
							Header:  mail.Header(p.Header),
							content: string(body),
						}
						r.NoError(p.Close())
					}
					r.ElementsMatch(parts, tt.wParts)
				})
			})
		})
	}
}

func TestBase64Boundaryer_Boundary(t *testing.T) {
	t.Parallel()
	wRand := []byte{0x78, 0xfc, 0x2f, 0xfa, 0xc2, 0xfd, 0x94, 0x1}

	b := Base64Boundaryer{Source: rand.NewSource(0)}

	now := time.Now()
	got := b.Boundary()

	r := require.New(t)
	bs, err := base64.RawURLEncoding.DecodeString(got)
	r.NoError(err)
	r.Equal(wRand, bs[:8])

	ts := time.Unix(0, int64(binary.BigEndian.Uint64(bs[8:])))
	r.WithinDuration(now, ts, 15*time.Microsecond)
}

var _ Boundaryer = (stubBoundaryer)(nil)

type stubBoundaryer func() string

func (f stubBoundaryer) Boundary() string { return f() }

var _ io.Writer = (*predicateErrWriter)(nil)

type predicateErrWriter struct {
	predicate func(p []byte) (err error)
	io.Writer
}

func (b *predicateErrWriter) Write(p []byte) (n int, err error) {
	if err = b.predicate(p); err != nil {
		return
	}
	return b.Writer.Write(p)
}
