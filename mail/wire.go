// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//go:build wireinject

//go:generate go run github.com/google/wire/cmd/wire gen -header_file=../internal/_resources/license-header.go
//go:generate go fix -fix buildtag .
//go:generate go run mvdan.cc/gofumpt -w wire_gen.go

package mail

import (
	"math/rand"
	"net/smtp"
	"time"

	"github.com/google/wire"
)

// NewMailer instance.
func NewMailer(
	defaultFrom EmailAddr,
	_ Host,
	_ Port,
	_ User,
	_ Pass,
) Mailer {
	panic(wire.Build(
		wire.Bind(new(Mailer), new(*mailer)),
		mailerSet,
		composerSet,
	))
}

func newMailer(
	defaultFrom EmailAddr,
	_ Host,
	_ Port,
	_ User,
	_ Pass,
	_ Composer,
) *mailer {
	panic(wire.Build(
		mailerSet,
	))
}

var (
	mailerSet = wire.NewSet(
		wire.Struct(new(mailer), "*"),
		provideServerAddr,
		provideAuth,
	)
	composerSet = wire.NewSet(
		wire.Bind(new(Composer), new(MultipartComposer)),
		wire.Struct(new(MultipartComposer), "*"),
		boundaryerSet,
	)
	boundaryerSet = wire.NewSet(
		wire.Bind(new(Boundaryer), new(*Base64Boundaryer)),
		wire.Struct(new(Base64Boundaryer), "Source"),
		provideNanoNowRandSource,
	)
)

func provideServerAddr(h Host, p Port) serverAddr {
	return serverAddr(string(h) + ":" + string(p))
}

func provideAuth(u User, p Pass, h Host) smtp.Auth {
	return smtp.PlainAuth("", string(u), string(p), string(h))
}

func provideNanoNowRandSource() rand.Source {
	return rand.NewSource(time.Now().UnixNano())
}
