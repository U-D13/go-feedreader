// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package user

//go:generate go run golang.org/x/tools/cmd/stringer -type=Status
//go:generate go run mvdan.cc/gofumpt -w status_string.go

import (
	"context"
	"database/sql"
	"net/mail"
	"time"

	"github.com/antichris/go-feedreader/terror"
)

type (
	// Storage of [User] records.
	Storage interface {
		GetByID(ctx context.Context, id int32) (User, error)
		GetByEmail(ctx context.Context, email string) (User, error)
		GetByEmailAndStatus(
			ctx context.Context, email string, s Status,
		) (User, error)
		GetByEmailToken(ctx context.Context, token string) (User, error)
		Create(
			ctx context.Context,
			email, passwordHash, firstName, lastName, emailToken string,
		) error
		SetStatus(ctx context.Context, id int32, v Status) error
	}
	User struct {
		ID           int32
		Email        string
		PasswordHash string
		FirstName    string
		LastName     string
		EmailToken   *string
		Status       Status
		CreatedAt    time.Time
		UpdatedAt    time.Time
		AuthKey      string
	}
	// Status represents the user status.
	Status int // TODO Should user Status be int16, int32 or int64?

	// TxableStorage is storage that can initiate transactions.
	TxableStorage interface {
		Storage
		// WithSQLTx returns a [StorageTx] instance under an SQL
		// transaction.
		WithSQLTx( // TODO Consider a more generic (no SQL) approach.
			ctx context.Context, opts *sql.TxOptions,
		) (st StorageTx, err error)
		// // WithSQLTxExec executes fn with a [StorageTx] instance under
		// // an SQL transaction.
		// //
		// // If fn does not return an error, the transaction will be
		// // automatically committed.
		// // If the commit fails with [sql.ErrTxDone] (i.e. due to
		// // transaction already having been rolled back or committed),
		// // this error is silently discarded.
		// //
		// // If an error is returned by fn, the transaction is rolled
		// // back, and the error is passed on.
		// // If this rollback fails for a reason other than
		// // [sql.ErrTxDone], the original error by fn is returned,
		// // wrapped with a message that includes a string representation
		// // of the rollback error.
		// //
		// // If fn panics, the transaction is rolled back.
		// // If this rollback fails for a reason other than
		// // [sql.ErrTxDone], a new panic is issued with the rollback
		// // error wrapped with a message that includes a string
		// // representation of the recovered panic value.
		// // Otherwise, this re-panics with the recovered panic value.
		// WithSQLTxExec(
		// 	ctx context.Context, opts *sql.TxOptions, fn WithTxExecFunc,
		// ) (err error)
	}
	// StorageTx is storage under transaction and must be explicitly
	// requested to commit changes.
	// Once committed or rolled back, it cannot be used any further, and
	// all operations on the storage will fail with [sql.ErrTxDone].
	StorageTx interface {
		Storage
		Tx
	}
	// Tx is a transaction.
	Tx interface {
		// Commit commits the transaction.
		Commit() error
		// Rollback aborts the transaction.
		Rollback() error
	}
	// WithTxExecFunc = func(ctx context.Context, st StorageTx) (err error)
)

const ErrNotFound = terror.Error("user not found")

func (u User) CreatedAtString() string {
	return formatDateTime(u.CreatedAt)
}

func (u User) UpdatedAtString() string {
	return formatDateTime(u.UpdatedAt)
}

func (u User) FullName() string {
	if len(u.FirstName) == 0 || len(u.LastName) == 0 {
		return u.FirstName + u.LastName
	}
	return u.FirstName + " " + u.LastName
}

func (u User) EmailAddr() *mail.Address {
	return &mail.Address{
		Name:    u.FullName(),
		Address: u.Email,
	}
}

const (
	_ Status = iota - 1

	Deleted
	Inactive
	Active

	Default = Inactive
)

func formatDateTime(t time.Time) string {
	return t.Format(time.RFC3339)
}
