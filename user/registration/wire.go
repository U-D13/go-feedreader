// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//go:build wireinject

//go:generate go run github.com/google/wire/cmd/wire gen -header_file=../../internal/_resources/license-header.go
//go:generate go fix -fix buildtag .
//go:generate go run mvdan.cc/gofumpt -w wire_gen.go

package registration

import (
	"net/url"

	"github.com/antichris/go-feedreader/internal/util/strings"
	"github.com/antichris/go-feedreader/mail"
	"github.com/google/wire"
)

// NewMailer instance.
func NewMailer(
	mail.Mailer,
	FrontendURL,
) (Mailer, error) {
	panic(wire.Build(
		wire.Bind(new(Mailer), new(*mailer)),
		wire.Struct(new(mailer), "*"),
		provideActivationLinkSource,
	))
}

type FrontendURL string

func provideActivationLinkSource(
	u FrontendURL,
) (s ActivationLinkSource, err error) {
	urlPrefix, err := url.JoinPath(string(u), "confirm")
	if err != nil {
		return
	}
	urlPrefix = strings.EnsureTrailingSlash(urlPrefix)
	s = func(token string) string { return urlPrefix + token }
	return
}
