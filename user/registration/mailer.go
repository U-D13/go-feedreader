// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package registration

import (
	"bytes"
	"fmt"
	html "html/template"
	"io"
	text "text/template"

	"github.com/antichris/go-feedreader/mail"
)

type Mailer interface {
	Mail(email mail.Address, firstName, token string) error
}

var _ Mailer = mailer{}

type mailer struct {
	mail.Mailer
	getActivationLink ActivationLinkSource
}

func (m mailer) Mail(email mail.Address, firstName, token string) (err error) {
	data := struct {
		Name           string
		ActivationLink string
	}{
		Name:           firstName,
		ActivationLink: m.getActivationLink(token),
	}

	// TODO Optimize to reduce allocations.
	bt := &bytes.Buffer{}
	bh := &bytes.Buffer{}
	if err = render(bt, bh, data); err != nil {
		err = fmt.Errorf("render mail template: %w", err)
		return err
	}

	err = m.SendMail(
		[]string{email.String()},
		"Account registration at Feed Reader",
		bt.Bytes(),
		bh.Bytes(),
	)
	if err != nil {
		err = fmt.Errorf("send mail: %w", err)
	}
	return
}

func render(text, html io.Writer, data any) error {
	if err := tText.Execute(text, data); err != nil {
		return err
	}
	if err := tHTML.Execute(html, data); err != nil {
		return err
	}
	return nil
}

type ActivationLinkSource func(token string) (url string)

var (
	tText executer = textMust(text.New("").Parse(`Hello {{ .Name }},

Please follow the link below to confirm your registration:

{{ .ActivationLink }}`))

	tHTML executer = html.Must(html.New("").Parse(`<div>
<p>Hello {{ .Name }},</p>

<p>Please follow the link below to confirm your registration:</p>

<p><a href="{{ .ActivationLink }}">{{ .ActivationLink }}</a></p>
</div>`))
)

type executer interface {
	Execute(wr io.Writer, data any) error
}

func textMust(t *text.Template, err error) *text.Template {
	if err != nil {
		panic(err)
	}
	return t
}
