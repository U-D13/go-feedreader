// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package registration

import (
	"bytes"
	"fmt"
	html "html/template"
	"io"
	"net/url"
	"strings"
	"sync"
	"testing"
	text "text/template"

	"github.com/antichris/go-feedreader/mail/mocks"
	"github.com/antichris/go-feedreader/terror"
	"github.com/antichris/go-feedreader/user/testdata"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestNewMailer(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		url  FrontendURL
		wErr string
	}{"bad url": {
		url:  ":",
		wErr: `parse ":": missing protocol scheme`,
	}, "nominal": {
		url: "/",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			mm := mocks.NewMockMailer(gomock.NewController(t))
			m, err := NewMailer(mm, tt.url)
			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.IsType((*mailer)(nil), m)
		})
	}
}

func TestMailer_Mail(t *testing.T) {
	t.Parallel()
	u := testdata.User

	mu := sync.Mutex{}
	withMux := func(f func()) {
		mu.Lock()
		defer mu.Unlock()
		f()
	}
	withText := func(e executer) func(func()) {
		return func(f func()) {
			withMux(func() {
				defer func(v executer) { tText = v }(tText)
				tText = e
				f()
			})
		}
	}
	withHTML := func(e executer) func(func()) {
		return func(f func()) {
			withMux(func() {
				defer func(v executer) { tHTML = v }(tHTML)
				tHTML = e
				f()
			})
		}
	}
	for n, tt := range map[string]struct {
		mailer error
		tText  error
		tHTML  error
		want   error
	}{"tText error": {
		tText: errSnap,
		want:  errSnap,
	}, "tHTML error": {
		tHTML: errSnap,
		want:  errSnap,
	}, "mailer error": {
		mailer: errSnap,
		want:   errSnap,
	}, "nominal": {
		want: nil,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()

			mt := mockExecuter(t)
			mh := mockExecuter(t)

			var run func(func())
			switch {
			case tt.tHTML != nil:
				mh.OnExecute(mock.Anything, mock.Anything).
					Return(tt.tHTML)
				run = withHTML(mh)
			case tt.tText != nil:
				mt.OnExecute(mock.Anything, mock.Anything).
					Return(tt.tText)
				run = withText(mt)
			default:
				run = withMux
			}

			mm := mocks.NewMockMailer(gomock.NewController(t))
			mm.EXPECT().
				SendMail(
					[]string{u.EmailAddr().String()},
					"Account registration at Feed Reader",
					gotBytesAsString(bytesContainString(
						u.FirstName,
						*u.EmailToken,
					)),
					gotBytesAsString(bytesContainString(
						u.FirstName,
						"<a href=",
						strings.ToLower(url.QueryEscape(*u.EmailToken)),
						html.HTMLEscapeString(*u.EmailToken),
					)),
				).
				Return(tt.mailer).
				AnyTimes()

			m, err := NewMailer(mm, "")
			if err != nil {
				t.Fatalf("create mailer: %v", err)
			}

			run(func() {
				err = m.Mail(u.EmailAddr(), u.FirstName, *u.EmailToken)
			})

			r := require.New(t)
			mock.AssertExpectationsForObjects(t, mt, mh)
			if tt.want != nil {
				r.ErrorIs(err, tt.want)
				return
			}
			r.NoError(err)
		})
	}
}

func Test_textMust(t *testing.T) {
	t.Parallel()
	tpl := &text.Template{}
	tests := map[string]struct {
		err    error
		wPanic error
	}{"error": {
		err:    errSnap,
		wPanic: errSnap,
	}, "nominal": {}}
	for n, tt := range tests {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got *text.Template
			run := func() { got = textMust(tpl, tt.err) }

			r := require.New(t)
			if tt.wPanic != nil {
				r.PanicsWithValue(tt.wPanic, run)
				return
			}
			r.NotPanics(run)
			r.Equal(tpl, got)
		})
	}
}

func gotBytesAsString(m gomock.Matcher) gomock.Matcher {
	fn := gomock.GotFormatterFunc(func(got any) string {
		if b, ok := got.([]byte); ok {
			return string(b)
		}
		return fmt.Sprintf("%v", got)
	})
	return gomock.GotFormatterAdapter(fn, m)
}

func bytesContainString(s ...string) gomock.Matcher {
	ms := make([]gomock.Matcher, 1, len(s)+1)
	ms[0] = gomock.AssignableToTypeOf([]byte{})
	for _, v := range s {
		ms = append(ms, bytesContainMatcher{[]byte(v), true})
	}
	return gomock.All(ms...)
}

const errSnap = terror.Error("snap")

// func bytesContain(b ...[]byte) gomock.Matcher {
// 	ms := make([]gomock.Matcher, 1, len(b)+1)
// 	ms[0] = gomock.AssignableToTypeOf([]byte{})
// 	for _, v := range b {
// 		ms = append(ms, bytesContainMatcher{b: v})
// 	}
// 	return gomock.All(ms...)
// }

var _ gomock.Matcher = bytesContainMatcher{}

type bytesContainMatcher struct {
	b []byte
	s bool
}

// Matches implements gomock.Matcher
func (m bytesContainMatcher) Matches(x any) bool {
	b, ok := x.([]byte)
	if !ok {
		return false
	}
	return bytes.Contains(b, m.b)
}

// String implements gomock.Matcher
func (m bytesContainMatcher) String() string {
	if m.s {
		return fmt.Sprintf("bytes contain %#q", string(m.b))
	}
	return fmt.Sprintf("bytes contain %v", m.b)
}

func mockExecuter(t mock.TestingT) *executerMock {
	m := &executerMock{}
	m.Test(t)
	return m
}

type executerMock struct{ mock.Mock }

func (m *executerMock) Execute(wr io.Writer, data any) error {
	return m.Called(wr, data).Error(0)
}

func (m *executerMock) OnExecute(wr, data any) *mock.Call {
	return m.On("Execute", wr, data)
}
