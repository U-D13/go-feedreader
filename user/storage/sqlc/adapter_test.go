// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlc_test

import (
	"testing"
	"testing/fstest"

	"github.com/antichris/go-feedreader/internal/util/testu"
	. "github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

var _ = Driver("")

func TestDrivers(t *testing.T) {
	t.Parallel()
	want := []string{"a", "b", "c"}

	r := newAdapterRegistry(len(want))
	for _, v := range want {
		_ = r.Register(Driver(v), nil)
	}
	var got []string
	withAdapterRegistry(r, func() { got = Drivers() })
	require.Equal(t, want, got)
}

func TestGetAdapter(t *testing.T) {
	t.Parallel()
	a := mocks.NewMockAdapter(gomock.NewController(t))

	r := newAdapterRegistry(1)
	_ = r.Register(driver, a)

	for n, tt := range map[string]struct {
		d    Driver
		want Adapter
		wErr string
	}{"absent": {
		d:    "foo",
		wErr: `unrecognized DB driver: "foo"`,
	}, "nominal": {
		d:    driver,
		want: a,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got Adapter
			var err error
			withAdapterRegistry(r, func() { got, err = GetAdapter(tt.d) })

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func TestRegisterAdapter(t *testing.T) {
	t.Parallel()
	a := mocks.NewMockAdapter(gomock.NewController(t))

	ar := newAdapterRegistry(1)
	_ = ar.Register(driver, a)
	withRegistry := func(f func()) { withAdapterRegistry(ar, f) }

	for n, tt := range map[string]struct {
		d    Driver
		wErr string
	}{"taken": {
		d:    driver,
		wErr: `adapter already registered for "` + driver + `"`,
	}, "nominal": {
		d: "foo",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var err error
			var got Adapter
			withRegistry(func() { err = RegisterAdapter(tt.d, a) })

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)

			withRegistry(func() { got, err = GetAdapter(tt.d) })
			r.NoError(err)
			r.Equal(a, got)
		})
	}
}

func TestRegisterPlainAdapter(t *testing.T) {
	t.Parallel()
	a := mocks.NewMockAdapter(gomock.NewController(t))
	wCtor := func(db DB) (q Querier) { return }
	wSchema := fstest.MapFS{}

	ar := newAdapterRegistry(1)
	_ = ar.Register(driver, a)
	withRegistry := func(f func()) { withAdapterRegistry(ar, f) }

	for n, tt := range map[string]struct {
		d        Driver
		n        DSN
		wURL     string
		wErr     string
		wOpenErr string
	}{"taken": {
		d:    driver,
		wErr: `adapter already registered for "` + driver + `"`,
	}, "nominal": {
		d:        "foo",
		n:        "dsn",
		wOpenErr: `sql: unknown driver "foo" (forgotten import?)`,
		wURL:     "foo://dsn",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var err error
			var got Adapter
			withRegistry(func() {
				err = RegisterPlainAdapter(tt.d, wCtor, wSchema)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)

			withRegistry(func() { got, err = GetAdapter(tt.d) })
			r.NoError(err)

			_, err = got.Open(dsn)
			r.EqualError(err, tt.wOpenErr)
			r.True(testu.SameFunc(wCtor, got.Ctor()))
			r.Equal(wSchema, got.Schema())
			r.Equal(tt.wURL, got.MigrateURL(tt.n))
		})
	}
}

func Test_adapter_Open(t *testing.T) {
	t.Parallel()
	wDB := mocks.NewMockTxableDB(gomock.NewController(t))
	wErr := errSnap

	m := testu.NewMock(t)
	a := NewAdapter(func(n DSN) (TxableDB, error) {
		r := m.MethodCalled("Open", n)
		return r.Get(0).(TxableDB), r.Error(1)
	}, nil, nil, nil)
	m.On("Open", dsn).
		Return(wDB, wErr)

	got, err := a.Open(dsn)
	r := require.New(t)
	r.Equal(wErr, err)
	r.Equal(wDB, got)
	m.AssertExpectations(t)
}

func Test_adapter_Ctor(t *testing.T) {
	t.Parallel()
	want := func(db DB) (q Querier) { return }

	a := NewAdapter(nil, want, nil, nil)
	got := a.Ctor()
	require.True(t, testu.SameFunc(want, got))
}

func Test_adapter_Schema(t *testing.T) {
	t.Parallel()
	want := fstest.MapFS{}

	a := NewAdapter(nil, nil, want, nil)
	got := a.Schema()
	require.Equal(t, want, got)
}

func Test_adapter_MigrateURL(t *testing.T) {
	t.Parallel()
	const wUrl = "url"

	m := testu.NewFuncMock(t, "MigrateURL")
	a := NewAdapter(nil, nil, nil, func(n DSN) string {
		return m.Called(n).String(0)
	})
	m.On(dsn).
		Return(wUrl)

	got := a.MigrateURL(dsn)
	require.Equal(t, wUrl, got)
	m.AssertExpectations(t)
}
