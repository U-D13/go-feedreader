// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//go:build wireinject

//go:generate go run github.com/google/wire/cmd/wire gen -header_file=../../../internal/_resources/license-header.go
//go:generate go fix -fix buildtag .
//go:generate go run mvdan.cc/gofumpt -w wire_gen.go

package sqlc

import (
	"database/sql"
	"io/fs"

	"github.com/antichris/go-feedreader/user"
	"github.com/google/wire"
)

// New sqlc [user.Storage] instance.
func New(
	Driver,
	DB,
) (user.Storage, error) {
	panic(wire.Build(
		storageWQuerierSet,
	))
}

// NewTxable returns a new sqlc [user.TxableStorage] instance.
func NewTxable(
	Driver,
	TxableDB,
) (user.TxableStorage, error) {
	panic(wire.Build(
		wire.Bind(new(DB), new(TxableDB)),
		txableWQuerierSet,
	))
}

// NewTxableWithDSN returns a new sqlc [user.TxableStorage] instance.
func NewTxableWithDSN(
	Driver,
	DSN,
) (user.TxableStorage, error) {
	panic(wire.Build(
		querierWDsnSet,
		txableSet,
	))
}

func NewAdapter(
	open openFn,
	ctor ctorFn,
	schema fs.FS,
	migrateURL func(n DSN) string,
) Adapter {
	panic(wire.Build(
		wire.Bind(new(Adapter), new(*adapter)),
		wire.Struct(new(adapter), "*"),
	))
}

func injectNew(
	Querier,
) user.Storage {
	panic(wire.Build(
		storageSet,
	))
}

func createWithSQLTx(
	Driver,
	*sql.Tx,
) (user.StorageTx, error) {
	panic(wire.Build(
		wire.Bind(new(user.StorageTx), new(*storageTx)),
		wire.Struct(new(storageTx), "*"),

		wire.Bind(new(user.Tx), new(*sql.Tx)),
		wire.Bind(new(DB), new(*sql.Tx)),
		storageWQuerierSet,
	))
}

var (
	txableWQuerierSet = wire.NewSet(
		txableSet,
		querierSet,
	)
	txableSet = wire.NewSet(
		wire.Bind(new(user.TxableStorage), new(*txableStorage)),
		wire.Struct(new(txableStorage), "*"),
		structSet,
	)
	storageWQuerierSet = wire.NewSet(
		storageSet,
		querierSet,
	)
	storageSet = wire.NewSet(
		wire.Bind(new(user.Storage), new(*storage)),
		structSet,
	)
	structSet = wire.NewSet(
		wire.Struct(new(storage), "*"),
	)
	querierWDsnSet = wire.NewSet(
		openDB,
		wire.Bind(new(DB), new(TxableDB)),
		querierSet,
	)
	querierSet = wire.NewSet(
		GetAdapter,
		getCtor,
		constructQuerier,
	)
	ctorSet = wire.NewSet(
		GetAdapter,
		getCtor,
	)
)

func openDB(a Adapter, n DSN) (TxableDB, error) { return a.Open(n) }
func getCtor(a Adapter) ctorFn                  { return a.Ctor() }
func constructQuerier(c ctorFn, d DB) Querier   { return c(d) }
