// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package mocks provides GoMock implementations of [mysql.Querier] and
// [mysql.DBTX].
package mocks

//go:generate -command mg go run github.com/golang/mock/mockgen -destination=mocks.go -package=$GOPACKAGE -copyright_file=../../../../../internal/_resources/license-header
//go:generate mg github.com/antichris/go-feedreader/user/storage/sqlc/mysql/gen Querier,DBTX
//go:generate go run mvdan.cc/gofumpt -w mocks.go
