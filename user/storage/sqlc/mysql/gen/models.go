// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0

package gen

import (
	"database/sql"
	"time"
)

type User struct {
	ID           int32
	Email        string
	PasswordHash string
	FirstName    string
	LastName     string
	EmailToken   sql.NullString
	Status       int32
	CreatedAt    time.Time
	UpdatedAt    time.Time
	AuthKey      string
}
