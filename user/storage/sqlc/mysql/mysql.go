// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package mysql implements a MySQL adapter for [user.Storage].
package mysql

import (
	"context"
	"database/sql"

	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mysql/gen"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mysql/schema"

	_ "github.com/go-sql-driver/mysql"
)

func init() { initFn() }

var initFn = func() {
	if err := sqlc.RegisterPlainAdapter(
		"mysql", NewQuerier, schema.FS,
	); err != nil {
		panic(err)
	}
}

func NewQuerier(db sqlc.DB) sqlc.Querier {
	return querier{gen.New(db)}
}

var _ sqlc.Querier = querier{}

type querier struct{ q gen.Querier }

func (a querier) FindByEmail(
	ctx context.Context, email string,
) (sqlc.User, error) {
	return mapUserErr(a.q.FindByEmail(ctx, email))
}

func (a querier) FindByEmailAndStatus(
	ctx context.Context, arg sqlc.FindByEmailAndStatusParams,
) (sqlc.User, error) {
	return mapUserErr(
		a.q.FindByEmailAndStatus(ctx, gen.FindByEmailAndStatusParams{
			Email:  arg.Email,
			Status: arg.Status,
		}),
	)
}

func (a querier) FindByEmailToken(
	ctx context.Context, emailToken sql.NullString,
) (sqlc.User, error) {
	return mapUserErr(a.q.FindByEmailToken(ctx, emailToken))
}

func (a querier) FindByID(ctx context.Context, id int32) (sqlc.User, error) {
	return mapUserErr(a.q.FindByID(ctx, id))
}

func (a querier) InsertUser(
	ctx context.Context, arg sqlc.InsertUserParams,
) error {
	return a.q.InsertUser(ctx, gen.InsertUserParams{
		Email:        arg.Email,
		PasswordHash: arg.PasswordHash,
		FirstName:    arg.FirstName,
		LastName:     arg.LastName,
		EmailToken:   arg.EmailToken,
		Status:       arg.Status,
	})
}

func (a querier) SetStatus(
	ctx context.Context, arg sqlc.SetStatusParams,
) (int64, error) {
	return a.q.SetStatus(ctx, gen.SetStatusParams{
		Status: arg.Status,
		ID:     arg.ID,
	})
}

func mapUserErr(u gen.User, err error) (sqlc.User, error) {
	return sqlc.User{
		ID:           int32(u.ID),
		Email:        u.Email,
		PasswordHash: u.PasswordHash,
		FirstName:    u.FirstName,
		LastName:     u.LastName,
		EmailToken:   u.EmailToken,
		Status:       int32(u.Status),
		CreatedAt:    u.CreatedAt,
		UpdatedAt:    u.UpdatedAt,
		AuthKey:      u.AuthKey,
	}, err
}
