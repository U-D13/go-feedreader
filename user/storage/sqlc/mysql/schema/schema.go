// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package migrations exports an embedded file system that contains
// SQL migrations defining the schema for a MySQL user storage.
package schema

import "embed"

//go:embed *.sql
var FS embed.FS
