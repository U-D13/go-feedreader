-- name: InsertUser :exec
INSERT INTO user (
  email, password_hash, first_name, last_name, email_token, status, auth_key
) VALUES (
  ?, ?, ?, ?, ?, ?, ?
);

-- name: FindByID :one
SELECT * FROM user WHERE id=?;

-- name: FindByEmail :one
SELECT * FROM user WHERE email=? LIMIT 1;

-- name: FindByEmailAndStatus :one
SELECT * FROM user WHERE email=? AND status=? LIMIT 1;

-- name: FindByEmailToken :one
SELECT * FROM user WHERE email_token=? LIMIT 1;

-- name: SetStatus :execrows
UPDATE user SET status=? WHERE id=?;

-- -- TODO Maybe this is better than the two above?
-- -- name: SetStatusByEmailToken :execrows
-- UPDATE user SET status=? WHERE email_token=? LIMIT 1;
