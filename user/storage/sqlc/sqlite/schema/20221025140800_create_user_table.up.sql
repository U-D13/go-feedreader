CREATE TABLE IF NOT EXISTS user (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  email varchar(255) NOT NULL UNIQUE,
  password_hash varchar(255) NOT NULL,
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  email_token varchar(255) DEFAULT NULL,
  status smallint NOT NULL DEFAULT 1,
  created_at datetime NOT NULL DEFAULT(strftime('%Y-%m-%dT%H:%M:%fZ','now','utc')),
  updated_at datetime NOT NULL DEFAULT(strftime('%Y-%m-%dT%H:%M:%fZ','now','utc')),
  auth_key varchar(32) NOT NULL
);

CREATE INDEX IF NOT EXISTS "user:status,id" ON user (status, id);
CREATE INDEX IF NOT EXISTS "user:status,email" ON user (status, email);
CREATE INDEX IF NOT EXISTS "user:status,email_token" ON user (status, email_token);

CREATE TRIGGER IF NOT EXISTS update_user_at_now
BEFORE UPDATE ON user FOR EACH ROW BEGIN
  UPDATE user SET
    updated_at = strftime('%Y-%m-%dT%H:%M:%fZ','now','utc')
  WHERE id = OLD.id;
END
