// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlite

import (
	"context"
	"database/sql"
	"testing"

	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/antichris/go-feedreader/terror"
	"github.com/antichris/go-feedreader/user"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	sqlcMocks "github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/antichris/go-feedreader/user/storage/sqlc/sqlite/gen"
	"github.com/antichris/go-feedreader/user/storage/sqlc/sqlite/mocks"
	"github.com/antichris/go-feedreader/user/testdata"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_initFn(t *testing.T) {
	t.Parallel()
	const (
		adapter = "sqlite"
		wPanic  = `adapter already registered for "` + adapter + `"`
	)
	require.PanicsWithError(t, wPanic, func() {
		initFn()
	})
	t.Run("adapter", func(t *testing.T) {
		t.Parallel()

		a, err := sqlc.GetAdapter(adapter)
		require.NoError(t, err)

		t.Run("open", func(t *testing.T) {
			t.Parallel()
			wType := (*sql.DB)(nil)
			wPingErr := "unable to open database file"

			got, err := a.Open("/")

			r := require.New(t)
			r.NoError(err)
			r.IsType(wType, got)

			db := got.(*sql.DB)
			err = db.Ping()
			// TODO Find out why it is sometimes exactly [wPingErr],
			// but sometimes — wPingErr+": invalid argument".
			r.ErrorContains(err, wPingErr)
		})
		t.Run("ctor", func(t *testing.T) {
			t.Parallel()
			want := NewQuerier

			got := a.Ctor()
			require.True(t, testu.SameFunc(want, got))
		})
		t.Run("migrate URL", func(t *testing.T) {
			t.Parallel()
			const dsn = "dsn"
			want := "sqlite3://" + dsn

			got := a.MigrateURL(dsn)
			require.Equal(t, want, got)
		})
	})
}

func TestNewQuerier(t *testing.T) {
	t.Parallel()
	wType := querier{}
	db := sqlcMocks.NewMockDB(gomock.NewController(t))

	got := NewQuerier(db)
	require.IsType(t, wType, got)
}

// TODO Deduplicate test code.

func Test_querier_FindByEmail(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		email   string
		querier error
		want    sqlc.User
		wErr    string
	}{"error": {
		querier: errSnap,
		wErr:    errSnap.Error(),
	}, "nominal": {
		email: sqlcUser.Email,
		want:  sqlcUser,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			q, m := newMockedQuerier(t)
			m.EXPECT().
				FindByEmail(ctx, tt.email).
				Return(genUser, tt.querier)

			got, err := q.FindByEmail(ctx, tt.email)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_querier_FindByEmailAndStatus(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		email   string
		status  user.Status
		querier error
		want    sqlc.User
		wErr    string
	}{"error": {
		querier: errSnap,
		wErr:    errSnap.Error(),
	}, "nominal": {
		email: sqlcUser.Email,
		want:  sqlcUser,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			q, m := newMockedQuerier(t)
			m.EXPECT().
				FindByEmailAndStatus(ctx, gen.FindByEmailAndStatusParams{
					Email:  tt.email,
					Status: int64(tt.status),
				}).
				Return(genUser, tt.querier)

			got, err := q.FindByEmailAndStatus(
				ctx, sqlc.FindByEmailAndStatusParams{
					Email:  tt.email,
					Status: int32(tt.status),
				},
			)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_querier_FindByEmailToken(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		emailToken sql.NullString
		querier    error
		want       sqlc.User
		wErr       string
	}{"error": {
		querier: errSnap,
		wErr:    errSnap.Error(),
	}, "nominal": {
		emailToken: sqlcUser.EmailToken,
		want:       sqlcUser,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			q, m := newMockedQuerier(t)
			m.EXPECT().
				FindByEmailToken(ctx, tt.emailToken).
				Return(genUser, tt.querier)

			got, err := q.FindByEmailToken(ctx, tt.emailToken)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_querier_FindByID(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		id      int32
		querier error
		want    sqlc.User
		wErr    string
	}{"error": {
		querier: errSnap,
		wErr:    errSnap.Error(),
	}, "nominal": {
		id:   sqlcUser.ID,
		want: sqlcUser,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			q, m := newMockedQuerier(t)
			m.EXPECT().
				FindByID(ctx, int64(tt.id)).
				Return(genUser, tt.querier)

			got, err := q.FindByID(ctx, tt.id)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_querier_InsertUser(t *testing.T) {
	t.Parallel()
	type params = sqlc.InsertUserParams
	for n, tt := range map[string]struct {
		params  params
		querier error
		wErr    string
	}{"error": {
		querier: errSnap,
		wErr:    errSnap.Error(),
	}, "nominal": {
		params: params{
			Email:        sqlcUser.Email,
			PasswordHash: sqlcUser.PasswordHash,
			FirstName:    sqlcUser.FirstName,
			LastName:     sqlcUser.LastName,
			EmailToken:   sqlcUser.EmailToken,
			Status:       sqlcUser.Status,
		},
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			q, m := newMockedQuerier(t)
			m.EXPECT().
				InsertUser(ctx, gen.InsertUserParams{
					Email:        tt.params.Email,
					PasswordHash: tt.params.PasswordHash,
					FirstName:    tt.params.FirstName,
					LastName:     tt.params.LastName,
					EmailToken:   tt.params.EmailToken,
					Status:       int64(tt.params.Status),
				}).
				Return(tt.querier)

			err := q.InsertUser(ctx, tt.params)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
		})
	}
}

func Test_querier_SetStatus(t *testing.T) {
	t.Parallel()
	type params = sqlc.SetStatusParams
	for n, tt := range map[string]struct {
		params       params
		rowsAffected int64
		querier      error
		want         int64
		wErr         string
	}{"error": {
		querier: errSnap,
		wErr:    errSnap.Error(),
	}, "nominal": {
		params: params{
			Status: sqlcUser.Status,
			ID:     sqlcUser.ID,
		},
		rowsAffected: 1,
		want:         1,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			q, m := newMockedQuerier(t)
			m.EXPECT().
				SetStatus(ctx, gen.SetStatusParams{
					Status: int64(tt.params.Status),
					ID:     int64(tt.params.ID),
				}).
				Return(tt.rowsAffected, tt.querier)

			got, err := q.SetStatus(ctx, tt.params)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func newMockedQuerier(t *testing.T) (querier, *mocks.MockQuerier) {
	m := mocks.NewMockQuerier(gomock.NewController(t))
	return querier{m}, m
}

const errSnap = terror.Error("snap")

var ctx = context.Background()

var (
	tdUser  = testdata.User
	genUser = gen.User{
		ID:           int64(tdUser.ID),
		Email:        tdUser.Email,
		PasswordHash: tdUser.PasswordHash,
		FirstName:    tdUser.FirstName,
		LastName:     tdUser.LastName,
		EmailToken:   testdata.EmailTokenNS,
		Status:       int64(tdUser.Status),
		CreatedAt:    tdUser.CreatedAt,
		UpdatedAt:    tdUser.UpdatedAt,
		AuthKey:      tdUser.AuthKey,
	}
	sqlcUser = sqlc.User{
		ID:           tdUser.ID,
		Email:        tdUser.Email,
		PasswordHash: tdUser.PasswordHash,
		FirstName:    tdUser.FirstName,
		LastName:     tdUser.LastName,
		EmailToken:   testdata.EmailTokenNS,
		Status:       int32(tdUser.Status),
		CreatedAt:    tdUser.CreatedAt,
		UpdatedAt:    tdUser.UpdatedAt,
		AuthKey:      tdUser.AuthKey,
	}
)
