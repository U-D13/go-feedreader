// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlc_test

import (
	"database/sql"
	"testing"

	"github.com/antichris/go-feedreader/user"
	. "github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	t.Parallel()
	want := &Xstorage{}

	c := gomock.NewController(t)
	db := mocks.NewMockTxableDB(c)
	registerMockAdapterWithMockRegistry(c)

	for n, tt := range map[string]struct {
		d    Driver
		wErr string
	}{"unrecognized driver": {
		d:    "foo",
		wErr: `unrecognized DB driver: "foo"`,
	}, "mock driver": {
		d: driver,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got user.Storage
			var err error
			withMockAdapterRegistry(func() {
				got, err = New(tt.d, db)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.IsType(want, got)
		})
	}
}

func TestNewTxable(t *testing.T) {
	t.Parallel()
	want := &XtxableStorage{}

	c := gomock.NewController(t)
	db := mocks.NewMockTxableDB(c)
	registerMockAdapterWithMockRegistry(c)

	for n, tt := range map[string]struct {
		d    Driver
		wErr string
	}{"unrecognized driver": {
		d:    "foo",
		wErr: `unrecognized DB driver: "foo"`,
	}, "mock driver": {
		d: driver,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got user.TxableStorage
			var err error
			withMockAdapterRegistry(func() {
				got, err = NewTxable(tt.d, db)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.IsType(want, got)
		})
	}
}

func TestNewTxableWithDSN(t *testing.T) {
	t.Parallel()
	want := &XtxableStorage{}

	for n, tt := range map[string]struct {
		d    Driver
		open error
		wErr string
	}{"unrecognized driver": {
		d:    "foo",
		wErr: `unrecognized DB driver: "foo"`,
	}, "open DB error": {
		d:    driver,
		open: errSnap,
		wErr: errSnap.Error(),
	}, "nominal": {
		d: driver,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			ar := newAdapterRegistry(1)
			m := mocks.NewMockAdapter(gomock.NewController(t))
			_ = ar.Register(driver, m)

			x := m.EXPECT()
			x.Ctor().
				Return(func(db DB) (q Querier) { return }).
				AnyTimes()
			x.Open(dsn).
				Return(nil, tt.open).
				AnyTimes()

			var got user.TxableStorage
			var err error
			withAdapterRegistry(ar, func() {
				got, err = NewTxableWithDSN(tt.d, dsn)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.IsType(want, got)
		})
	}
}

func Test_createWithSQLTx(t *testing.T) {
	t.Parallel()
	want := &XstorageTx{}

	c := gomock.NewController(t)
	registerMockAdapterWithMockRegistry(c)

	for n, tt := range map[string]struct {
		d    Driver
		wErr string
	}{"unrecognized driver": {
		d:    "foo",
		wErr: `unrecognized DB driver: "foo"`,
	}, "nominal": {
		d: driver,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got user.StorageTx
			var err error
			withMockAdapterRegistry(func() {
				got, err = createWithSQLTx(tt.d, &sql.Tx{})
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.IsType(want, got)
		})
	}
}
