// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlc_test

import (
	"context"
	"database/sql"
	"testing"

	"github.com/antichris/go-feedreader/terror"
	"github.com/antichris/go-feedreader/user"
	. "github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/antichris/go-feedreader/user/testdata"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

type qRecorder = mocks.MockQuerierMockRecorder

func Test_storage_GetByID(t *testing.T) {
	t.Parallel()
	noEmailTokenUser := wUser   // Copy on assignment.
	noEmailTokenDBUser := qUser // Copy on assignment.
	noEmailTokenUser.EmailToken = nil
	noEmailTokenDBUser.EmailToken = sql.NullString{}

	tests := mergeTests(basicRetrievalTests, retrievalTests{
		"user without emailToken": {
			qUser: noEmailTokenDBUser,
			wUser: noEmailTokenUser,
		},
	})
	testRetrieval(t, tests, func(r *qRecorder) *gomock.Call {
		return r.FindByID(ctx, userID)
	}, func(s user.Storage) (user.User, error) {
		return s.GetByID(ctx, userID)
	})
}

func Test_storage_GetByEmail(t *testing.T) {
	t.Parallel()
	testRetrieval(t, basicRetrievalTests, func(r *qRecorder) *gomock.Call {
		return r.FindByEmail(ctx, email)
	}, func(s user.Storage) (user.User, error) {
		return s.GetByEmail(ctx, email)
	})
}

func Test_storage_GetByEmailAndStatus(t *testing.T) {
	t.Parallel()
	const status = user.Default
	testRetrieval(t, basicRetrievalTests, func(r *qRecorder) *gomock.Call {
		return r.FindByEmailAndStatus(ctx, FindByEmailAndStatusParams{
			Email:  email,
			Status: int32(status),
		})
	}, func(s user.Storage) (user.User, error) {
		return s.GetByEmailAndStatus(ctx, email, status)
	})
}

func Test_storage_GetByEmailToken(t *testing.T) {
	t.Parallel()
	testRetrieval(t, basicRetrievalTests, func(r *qRecorder) *gomock.Call {
		return r.FindByEmailToken(ctx, qEmailToken)
	}, func(s user.Storage) (user.User, error) {
		return s.GetByEmailToken(ctx, emailToken)
	})
}

func Test_storage_Create(t *testing.T) {
	t.Parallel()
	var (
		passHash  = wUser.PasswordHash
		firstName = wUser.FirstName
		lastName  = wUser.LastName
	)
	for n, tt := range map[string]struct {
		qErr error
		wErr error
	}{"error": {
		qErr: errSnap,
		wErr: errSnap,
	}, "nominal": {}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s, qr := newStorageWithMockQuerier(t)
			qr.InsertUser(ctx, InsertUserParams{
				Email:        email,
				PasswordHash: passHash,
				FirstName:    firstName,
				LastName:     lastName,
				EmailToken:   qEmailToken,
				Status:       int32(user.Default),
			}).Return(tt.qErr)

			err := s.Create(
				ctx,
				email, passHash, firstName, lastName, emailToken,
			)

			r := require.New(t)
			if tt.wErr != nil {
				r.EqualError(err, tt.wErr.Error())
				return
			}
			r.NoError(err)
		})
	}
}

func Test_storage_SetStatus(t *testing.T) {
	t.Parallel()
	const status = user.Default
	for n, tt := range map[string]struct {
		qR   int64
		qErr error
		wErr string
	}{"error": {
		qErr: errSnap,
		wErr: errSnap.Error(),
	}, "0 rows updated": {
		qR:   0,
		wErr: "status has not been updated",
	}, "nominal": {
		qR: 1,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s, qr := newStorageWithMockQuerier(t)
			qr.SetStatus(ctx, SetStatusParams{
				ID:     userID,
				Status: int32(status),
			}).Return(tt.qR, tt.qErr)

			err := s.SetStatus(ctx, userID, status)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
		})
	}
}

func Test_txableStorage_WithTx(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		begTx error
		wErr  string
	}{"error": {
		begTx: errSnap,
		wErr:  errSnap.Error(),
	}, "nominal": {}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			c := gomock.NewController(t)
			db := mocks.NewMockTxableDB(c)
			db.EXPECT().
				BeginTx(ctx, nil).
				Return(nil, tt.begTx)
			registerMockAdapterWithMockRegistry(c)

			var ts user.TxableStorage
			var err error
			withMockAdapterRegistry(func() {
				ts, err = NewTxable(driver, db)
			})
			r := require.New(t)
			r.NoError(err)

			withMockAdapterRegistry(func() {
				// TODO Consider testing validity of the returned object.
				_, err = ts.WithSQLTx(ctx, nil)
			})

			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
		})
	}
}

// func Test_txableStorage_WithTxExec(t *testing.T) {
// 	t.Parallel()
// 	for n, tt := range map[string]struct {
// 		begTx    error
// 		fn       error
// 		panic    interface{}
// 		commit   error
// 		rollback error
// 		wErr     string
// 		wPanic   string
// 	}{"begin Tx error": {
// 		begTx: errSnap,
// 		wErr:  errSnap.Error(),
// 	}, "commit error": {
// 		commit: errSnap,
// 		wErr:   errSnap.Error(),
// 	}, "commit panic": {
// 		panic:  errSnap,
// 		wPanic: errSnap.Error(),
// 	}, "rollback panic": {
// 		rollback: errSnap,
// 		wPanic:   errSnap.Error(),
// 	}, "commit and rollback panic": {
// 		panic:    errSnap,
// 		rollback: errSnap,
// 		wPanic:   "rollback after snap: snap",
// 	}, "rollback committed": {
// 		rollback: sql.ErrTxDone,
// 	}, "nominal": {}} {
// 		tt := tt
// 		t.Run(n, func(t *testing.T) {
// 			t.Parallel()
// 			c := gomock.NewController(t)
// 			db := mocks.NewMockTxableDB(c)
// 			st := storageMocks.NewMockStorageTx(c)
// 			db.EXPECT().
// 				BeginTx(ctx, nil).
// 				Return(st, tt.begTx)
// 			registerMockAdapter(c)

// 			ts := NewTxable(dbDriver, db)

// 			fn := func(ctx context.Context, st user.StorageTx) (err error) {
// 				if tt.panic != nil {
// 					panic(tt.panic)
// 				}
// 				return tt.fn
// 			}
// 			err := ts.WithSQLTxExec(ctx, nil, fn)

// 			r := require.New(t)
// 			if tt.wErr != "" {
// 				r.EqualError(err, tt.wErr)
// 				return
// 			}
// 			r.NoError(err)
// 		})
// 	}
// }

type retrievalTests map[string]struct {
	qUser User
	qErr  error
	wUser user.User
	wErr  error
}

const (
	userID = testdata.UserID
	email  = testdata.Email
)

var (
	ctx   = context.Background()
	wUser = testdata.User
	qUser = User{
		ID:           wUser.ID,
		Email:        wUser.Email,
		PasswordHash: wUser.PasswordHash,
		FirstName:    wUser.FirstName,
		LastName:     wUser.LastName,
		EmailToken:   qEmailToken,
		Status:       int32(wUser.Status),
		CreatedAt:    wUser.CreatedAt,
		UpdatedAt:    wUser.UpdatedAt,
		AuthKey:      wUser.AuthKey,
	}
	emailToken  = testdata.EmailToken
	qEmailToken = testdata.EmailTokenNS

	basicRetrievalTests = retrievalTests{"error": {
		qErr: errSnap,
		wErr: errSnap,
	}, "not found": {
		qErr: sql.ErrNoRows,
		wErr: user.ErrNotFound,
	}, "nominal": {
		qUser: qUser,
		wUser: wUser,
	}}
	errSnap = terror.Error("snap")
)

func testRetrieval(
	t *testing.T,
	tests retrievalTests,
	confExpect func(r *qRecorder) *gomock.Call,
	retrieve func(s user.Storage) (user.User, error),
) {
	for n, tt := range tests {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			s, qr := newStorageWithMockQuerier(t)
			confExpect(qr).
				Return(tt.qUser, tt.qErr)

			gotUser, gotErr := retrieve(s)

			r := require.New(t)
			if tt.wErr != nil {
				r.EqualError(gotErr, tt.wErr.Error())
				return
			}
			r.NoError(gotErr)
			r.Equal(tt.wUser, gotUser)
		})
	}
}

func newStorageWithMockQuerier(t *testing.T) (user.Storage, *qRecorder) {
	q := mocks.NewMockQuerier(gomock.NewController(t))
	return injectNew(q), q.EXPECT()
}

func mergeTests(dst, src retrievalTests) retrievalTests {
	type empty struct{}
	keys := make(map[string]empty, len(dst)+len(src))
	for k := range dst {
		keys[k] = empty{}
	}
	for k := range src {
		keys[k] = empty{}
	}
	r := make(retrievalTests, len(keys))
	for k, v := range dst {
		r[k] = v
	}
	for k, v := range src {
		r[k] = v
	}
	return r
}
