// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlc

import "sync"

type (
	Xstorage         = storage
	XtxableStorage   = txableStorage
	XstorageTx       = storageTx
	XadapterRegistry = adapterRegistry
)

var (
	XinjectNew           = injectNew
	XcreateWithSQLTx     = createWithSQLTx
	XnewAdapterRegistry  = newAdapterRegistry
	XwithAdapterRegistry = withAdapterRegistry
)

func newAdapterRegistry(cap int) *adapterRegistry {
	return &adapterRegistry{m: make(map[Driver]Adapter, cap)}
}

func withAdapterRegistry(r *adapterRegistry, fn func()) {
	adaptersMu.Lock()
	defer adaptersMu.Unlock()
	defer func(v *adapterRegistry) { adapters = v }(adapters)
	adapters = r
	fn()
}

var adaptersMu = sync.Mutex{}
