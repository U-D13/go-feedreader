// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlc

import (
	"context"
	"database/sql"
	"fmt"
	"io/fs"
	"sort"
	"sync"
	"time"
)

type (
	Driver string
	DSN    string
)

// Drivers that have [Adapter]'s registered.
func Drivers() []string { return adapters.Names() }

// GetAdapter returns the registered [Adapter] for the given [Driver],
// or an error, if one is not registered.
func GetAdapter(d Driver) (Adapter, error) { return adapters.Get(d) }

// RegisterAdapter sets the given [Adapter] instance as the registered
// one for the given [Driver].
//
// If an Adapter is already registered for the given driver, an error
// is returned.
func RegisterAdapter(d Driver, a Adapter) error {
	return adapters.Register(d, a)
}

// TODO Consider implementing adapter de-registration.

// RegisterPlainAdapter with the given constructor function and schema
// file system for the given [Driver].
//
// If an Adapter is already registered for the given driver, an error
// is returned.
//
// The adapter is "plain" in the sense that it passes the driver name as
// is when calling [sql.Open] and in the migration URL scheme prefix.
// This is useful when Driver matches the Go SQL driver name exactly.
func RegisterPlainAdapter(d Driver, c ctorFn, schema fs.FS) error {
	return adapters.Register(d, NewAdapter(
		func(n DSN) (TxableDB, error) {
			return sql.Open(string(d), string(n))
		},
		c, schema, func(n DSN) string {
			return string(d) + "://" + string(n)
		},
	))
}

// Adapter supports custom driver usage in the [user.Storage] sqlc
// implementation.
type Adapter interface {
	// Open a database specified by a driver-specific data source name.
	Open(DSN) (TxableDB, error)
	// Ctor is the constructor for instances of the [Querier] that this
	// adapter provides.
	Ctor() ctorFn
	// Schema is a file system that describes the database schema as a
	// chronological sequence of SQL migrations.
	Schema() fs.FS
	// MigrateURL is used as the DSN when creating a new golang-migrate
	// instance.
	// It includes the underlying driver name as the scheme, which in
	// some cases may be different from the format the driver itself
	// uses.
	//
	// E.g., `MigrateURL` for the pgx driver could be similar to:
	//
	//  "pgx://user:pass@host:port/path?query"
	//
	// when the underlying driver requires a DSN like the following:
	//
	//  "postgres://user:pass@host:port/path?query"
	MigrateURL(DSN) string
}

type DB interface {
	ExecContext(context.Context, string, ...any) (sql.Result, error)
	PrepareContext(context.Context, string) (*sql.Stmt, error)
	QueryContext(context.Context, string, ...any) (*sql.Rows, error)
	QueryRowContext(context.Context, string, ...any) *sql.Row
}

// TxableDB is a [DB] than can also initiate transactions.
type TxableDB interface {
	DB
	BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)
}

type (
	Querier interface {
		FindByEmail(ctx context.Context, email string) (User, error)
		FindByEmailAndStatus(ctx context.Context, arg FindByEmailAndStatusParams) (User, error)
		FindByEmailToken(ctx context.Context, emailToken sql.NullString) (User, error)
		FindByID(ctx context.Context, id int32) (User, error)
		InsertUser(ctx context.Context, arg InsertUserParams) error
		SetStatus(ctx context.Context, arg SetStatusParams) (int64, error)
	}
	FindByEmailAndStatusParams struct {
		Email  string
		Status int32
	}
	InsertUserParams struct {
		Email        string
		PasswordHash string
		FirstName    string
		LastName     string
		EmailToken   sql.NullString
		Status       int32
	}
	SetStatusParams struct {
		Status int32
		ID     int32
	}
	User struct {
		ID           int32
		Email        string
		PasswordHash string
		FirstName    string
		LastName     string
		EmailToken   sql.NullString
		Status       int32
		CreatedAt    time.Time
		UpdatedAt    time.Time
		AuthKey      string
	}
)

var _ Adapter = (*adapter)(nil)

type adapter struct {
	ctor ctorFn
	open openFn
	sch  fs.FS
	mRUL func(DSN) string
}

func (a *adapter) Open(n DSN) (TxableDB, error) { return a.open(n) }
func (a *adapter) Ctor() ctorFn                 { return a.ctor }
func (a *adapter) Schema() fs.FS                { return a.sch }
func (a *adapter) MigrateURL(n DSN) string      { return a.mRUL(n) }

type (
	openFn = func(n DSN) (TxableDB, error)
	ctorFn = func(db DB) Querier
)

var adapters = &adapterRegistry{m: make(map[Driver]Adapter, 3)}

type adapterRegistry struct {
	mu sync.Mutex
	m  map[Driver]Adapter
}

func (r *adapterRegistry) Names() []string {
	r.mu.Lock()
	defer r.mu.Unlock()
	s := make([]string, 0, len(r.m))
	for n := range r.m {
		s = append(s, string(n))
	}
	sort.Slice(s, func(i, j int) bool {
		return s[i] < s[j]
	})
	return s
}

func (r *adapterRegistry) Get(d Driver) (a Adapter, err error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	a, ok := r.m[d]
	if !ok {
		err = fmt.Errorf("unrecognized DB driver: %q", d)
	}
	return
}

func (r *adapterRegistry) Register(d Driver, a Adapter) (err error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if _, ok := r.m[d]; ok {
		return fmt.Errorf("adapter already registered for %q", d)
	}
	r.m[d] = a
	return
}
