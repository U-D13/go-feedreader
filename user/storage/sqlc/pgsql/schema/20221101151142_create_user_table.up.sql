CREATE TABLE IF NOT EXISTS "user" (
	id serial NOT NULL PRIMARY KEY,
	email varchar(255) NOT NULL UNIQUE,
	password_hash varchar(255) NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	email_token varchar(255) DEFAULT NULL,
	status smallint NOT NULL DEFAULT 1,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	auth_key varchar(32) NOT NULL
);

CREATE INDEX IF NOT EXISTS "user:status,id" ON "user" (status, id);
CREATE INDEX IF NOT EXISTS "user:status,email" ON "user" (status, email);
CREATE INDEX IF NOT EXISTS "user:status,email_token" ON "user" (status, email_token);

CREATE OR REPLACE FUNCTION update_at_now()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated_at = now();
   RETURN NEW;
END;
$$ language 'plpgsql';

-- BUG sqlc refuses to accept the CREATE OR REPLACE TRIGGER syntax.
-- CREATE OR REPLACE TRIGGER update_user_at_now
CREATE TRIGGER update_user_at_now
BEFORE UPDATE ON "user" FOR EACH ROW
EXECUTE PROCEDURE update_at_now();
