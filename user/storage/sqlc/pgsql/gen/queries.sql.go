// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: queries.sql

package gen

import (
	"context"
	"database/sql"
)

const findByEmail = `-- name: FindByEmail :one
SELECT id, email, password_hash, first_name, last_name, email_token, status, created_at, updated_at, auth_key FROM "user" WHERE email=$1 LIMIT 1
`

func (q *Queries) FindByEmail(ctx context.Context, email string) (User, error) {
	row := q.db.QueryRowContext(ctx, findByEmail, email)
	var i User
	err := row.Scan(
		&i.ID,
		&i.Email,
		&i.PasswordHash,
		&i.FirstName,
		&i.LastName,
		&i.EmailToken,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.AuthKey,
	)
	return i, err
}

const findByEmailAndStatus = `-- name: FindByEmailAndStatus :one
SELECT id, email, password_hash, first_name, last_name, email_token, status, created_at, updated_at, auth_key FROM "user" WHERE email=$1 AND status=$2 LIMIT 1
`

type FindByEmailAndStatusParams struct {
	Email  string
	Status int16
}

func (q *Queries) FindByEmailAndStatus(ctx context.Context, arg FindByEmailAndStatusParams) (User, error) {
	row := q.db.QueryRowContext(ctx, findByEmailAndStatus, arg.Email, arg.Status)
	var i User
	err := row.Scan(
		&i.ID,
		&i.Email,
		&i.PasswordHash,
		&i.FirstName,
		&i.LastName,
		&i.EmailToken,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.AuthKey,
	)
	return i, err
}

const findByEmailToken = `-- name: FindByEmailToken :one
SELECT id, email, password_hash, first_name, last_name, email_token, status, created_at, updated_at, auth_key FROM "user" WHERE email_token=$1 LIMIT 1
`

func (q *Queries) FindByEmailToken(ctx context.Context, emailToken sql.NullString) (User, error) {
	row := q.db.QueryRowContext(ctx, findByEmailToken, emailToken)
	var i User
	err := row.Scan(
		&i.ID,
		&i.Email,
		&i.PasswordHash,
		&i.FirstName,
		&i.LastName,
		&i.EmailToken,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.AuthKey,
	)
	return i, err
}

const findByID = `-- name: FindByID :one
SELECT id, email, password_hash, first_name, last_name, email_token, status, created_at, updated_at, auth_key FROM "user" WHERE id=$1
`

func (q *Queries) FindByID(ctx context.Context, id int32) (User, error) {
	row := q.db.QueryRowContext(ctx, findByID, id)
	var i User
	err := row.Scan(
		&i.ID,
		&i.Email,
		&i.PasswordHash,
		&i.FirstName,
		&i.LastName,
		&i.EmailToken,
		&i.Status,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.AuthKey,
	)
	return i, err
}

const insertUser = `-- name: InsertUser :exec
INSERT INTO "user" (
  email, password_hash, first_name, last_name, email_token, status, auth_key
) VALUES (
  $1, $2, $3, $4, $5, $6, $7
)
`

type InsertUserParams struct {
	Email        string
	PasswordHash string
	FirstName    string
	LastName     string
	EmailToken   sql.NullString
	Status       int16
	AuthKey      string
}

func (q *Queries) InsertUser(ctx context.Context, arg InsertUserParams) error {
	_, err := q.db.ExecContext(ctx, insertUser,
		arg.Email,
		arg.PasswordHash,
		arg.FirstName,
		arg.LastName,
		arg.EmailToken,
		arg.Status,
		arg.AuthKey,
	)
	return err
}

const setStatus = `-- name: SetStatus :execrows
UPDATE "user" SET status=$2 WHERE id=$1
`

type SetStatusParams struct {
	ID     int32
	Status int16
}

func (q *Queries) SetStatus(ctx context.Context, arg SetStatusParams) (int64, error) {
	result, err := q.db.ExecContext(ctx, setStatus, arg.ID, arg.Status)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}
