-- name: InsertUser :exec
INSERT INTO "user" (
  email, password_hash, first_name, last_name, email_token, status, auth_key
) VALUES (
  $1, $2, $3, $4, $5, $6, $7
);

-- name: FindByID :one
SELECT * FROM "user" WHERE id=$1;

-- name: FindByEmail :one
SELECT * FROM "user" WHERE email=$1 LIMIT 1;

-- name: FindByEmailAndStatus :one
SELECT * FROM "user" WHERE email=$1 AND status=$2 LIMIT 1;

-- name: FindByEmailToken :one
SELECT * FROM "user" WHERE email_token=$1 LIMIT 1;

-- name: SetStatus :execrows
UPDATE "user" SET status=$2 WHERE id=$1;
