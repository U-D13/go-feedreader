// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sqlc_test

import (
	"sync"

	. "github.com/antichris/go-feedreader/user/storage/sqlc"
	mocks "github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/golang/mock/gomock"
)

type adapterRegistry = XadapterRegistry

var (
	injectNew           = XinjectNew
	createWithSQLTx     = XcreateWithSQLTx
	withAdapterRegistry = XwithAdapterRegistry
	newAdapterRegistry  = XnewAdapterRegistry
)

const driver = "mock"

func registerMockAdapterWithMockRegistry(c *gomock.Controller) {
	withMockAdapterRegistry(func() { registerMockAdapter(c) })
}

func withMockAdapterRegistry(f func()) {
	withAdapterRegistry(mockAdapterRegistry, f)
}

var mockAdapterRegistry = newAdapterRegistry(1)

func registerMockAdapter(c *gomock.Controller) {
	registerOnce.Do(func() {
		mocks.RegisterMockAdapter(c, driver)
	})
}

var registerOnce = sync.Once{}

const dsn DSN = "dsn"
