// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mocks

import (
	"testing/fstest"

	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/golang/mock/gomock"
)

func RegisterMockAdapter(ctrl *gomock.Controller, d sqlc.Driver) {
	err := sqlc.RegisterAdapter(d, sqlc.NewAdapter(
		func(d sqlc.DSN) (sqlc.TxableDB, error) {
			return NewMockTxableDB(ctrl), nil
		},
		func(db sqlc.DB) sqlc.Querier {
			return NewMockQuerier(ctrl)
		},
		fstest.MapFS{},
		func(d sqlc.DSN) string { return string(d) },
	))
	if err != nil {
		ctrl.T.Errorf("%v", err)
	}
}

func QuerierUnderlyingMock(q sqlc.Querier) (m *MockQuerier, ok bool) {
	m, ok = q.(*MockQuerier)
	return
}
