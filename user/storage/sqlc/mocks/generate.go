// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package mocks provides a GoMock implementation of [sqlc.Adapter],
// [sqlc.DB], [sqlc.TxableDB] and [sqlc.Querier].
package mocks

//go:generate -command mg go run github.com/golang/mock/mockgen -destination=mocks.go -package=$GOPACKAGE -copyright_file=../../../../internal/_resources/license-header
//go:generate mg github.com/antichris/go-feedreader/user/storage/sqlc Adapter,DB,TxableDB,Querier
//go:generate go run mvdan.cc/gofumpt -w mocks.go
