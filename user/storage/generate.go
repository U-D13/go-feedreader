// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package storage provides GoMock implementations of [user.Storage],
// [user.TxableStorage] and [user.StorageTx].
//
// Since this package is intended as a void logical parent (for project
// organization purposes only) for packages that implement the above
// storage interfaces, it seems just as well suited for housing mock
// implementations.
package storage

//go:generate -command mg go run github.com/golang/mock/mockgen -destination=mocks.go -package=$GOPACKAGE -copyright_file=../../internal/_resources/license-header
//go:generate mg github.com/antichris/go-feedreader/user Storage,TxableStorage,StorageTx
//go:generate go run mvdan.cc/gofumpt -w mocks.go
