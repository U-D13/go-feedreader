// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package user_test

import (
	"net/mail"
	"testing"
	"time"

	. "github.com/antichris/go-feedreader/user"
	"github.com/antichris/go-feedreader/user/testdata"
	"github.com/stretchr/testify/require"
)

func TestStatus_String(t *testing.T) {
	t.Parallel()
	for k, v := range map[Status]string{
		0:      "Deleted",
		Active: "Active",
		5:      "Status(5)",
	} {
		status, want := k, v
		t.Run(want, func(t *testing.T) {
			t.Parallel()
			got := status.String()
			require.Equal(t, want, got)
		})
	}
}

func TestUser_CreatedAtString(t *testing.T) {
	t.Parallel()
	u := testdata.User
	want := u.CreatedAt.Format(time.RFC3339)

	got := u.CreatedAtString()
	require.Equal(t, want, got)
}

func TestUser_UpdatedAtString(t *testing.T) {
	t.Parallel()
	u := testdata.User
	want := u.UpdatedAt.Format(time.RFC3339)

	got := u.UpdatedAtString()
	require.Equal(t, want, got)
}

func TestUser_EmailAddr(t *testing.T) {
	t.Parallel()
	u := testdata.User
	type Addr = mail.Address
	for n, tt := range map[string]struct {
		u    User
		want Addr
	}{"no first name": {
		u: User{
			LastName: u.LastName,
			Email:    u.Email,
		},
		want: Addr{
			Name:    u.LastName,
			Address: u.Email,
		},
	}, "no last name": {
		u: User{
			FirstName: u.FirstName,
			Email:     u.Email,
		},
		want: Addr{
			Name:    u.FirstName,
			Address: u.Email,
		},
	}, "no name": {
		u:    User{Email: u.Email},
		want: Addr{Name: "", Address: u.Email},
	}, "nominal": {
		u: User{
			FirstName: u.FirstName,
			LastName:  u.LastName,
			Email:     u.Email,
		},
		want: Addr{
			Name:    u.FirstName + " " + u.LastName,
			Address: u.Email,
		},
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := tt.u.EmailAddr()
			require.Equal(t, tt.want, *got)
		})
	}
}
