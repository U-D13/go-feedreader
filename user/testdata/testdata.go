// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package testdata

import (
	"database/sql"
	"time"

	"github.com/antichris/go-feedreader/user"
)

const (
	UserID int32 = 13

	Email = "user@example.com"
)

var (
	User = user.User{
		ID:           UserID,
		Email:        Email,
		PasswordHash: "<pass-hash>",
		FirstName:    "Jane",
		LastName:     "Doe",
		EmailToken:   &EmailToken,
		Status:       user.Default,
		CreatedAt:    time.Now().Add(-1 * time.Hour),
		UpdatedAt:    time.Now().Add(-1 * time.Minute),
		AuthKey:      "<auth-key>",
	}
	EmailToken = "<email-token>"

	// EmailTokenNS is a valid sql.NullString value of EmailToken.
	EmailTokenNS = sql.NullString{
		String: EmailToken,
		Valid:  true,
	}
)
