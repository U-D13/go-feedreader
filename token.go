// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/goa/gen/token"
	ts "github.com/antichris/go-feedreader/token"
	"github.com/antichris/go-feedreader/user"
	"goa.design/goa/v3/security"
)

var _ token.Service = (*tokensrvc)(nil)

// Token service example implementation.
// The example methods log the requests and return zero values.
type tokensrvc struct {
	logger *log.Logger
	s      ts.Service
	auth   auth.Service
	us     user.Storage
	delay  time.Duration
}

// APIKeyAuth implements the authorization logic for service "Token" for the
// "rJWT" security scheme.
func (s *tokensrvc) APIKeyAuth(
	ctx context.Context, key string, scheme *security.APIKeyScheme,
) (context.Context, error) {
	return s.auth.APIKeyAuth(ctx, key, tokenUnauthErr, tokenBadReqErr)
}

// Create an authorization token for the given credentials.
func (s *tokensrvc) Create(
	ctx context.Context, p *token.Credentials,
) (res token.OurToken, err error) {
	t := time.Now()
	s.logger.Print("token.Create")
	u, err := s.us.GetByEmailAndStatus(ctx, p.Email, user.Active)
	if err != nil {
		if errors.Is(err, user.ErrNotFound) {
			err = s.delayedErr(t, errInvalidCreds)
		}
		return
	}
	err = s.auth.ComparePasswordHash([]byte(u.PasswordHash), []byte(p.Password))
	if err != nil {
		if !errors.Is(err, auth.ErrMismatchedHashAndPassword) {
			s.logger.Println(err)
		}
		err = s.delayedErr(t, errInvalidCreds)
		return
	}
	return s.s.CreateFor(uint32(u.ID), 0)
}

// Issue a new authorization token for the holder of current.
func (s *tokensrvc) Reissue(
	ctx context.Context, p *token.ReissuePayload,
) (res token.OurToken, err error) {
	s.logger.Print("token.Reissue")
	userID, err := auth.ContextUserID(ctx)
	if err != nil {
		s.logger.Print(err)
		err = tokenInternalErr(err)
		return
	}
	return s.s.CreateFor(userID, 0)
}

// Describe an authorization token.
func (s *tokensrvc) Describe(
	ctx context.Context, p *token.DescribePayload,
) (res *token.TokenData, err error) {
	s.logger.Print("token.Describe")
	td, err := s.s.Decode(token.OurToken(p.Auth))
	if err != nil {
		if errors.Is(err, ts.ErrUnsupportedFormat) ||
			errors.Is(err, ts.ErrInvalidSignature) {
			err = tokenBadReqErr(err)
		}
		return
	}
	res = &td
	return
}

func (s *tokensrvc) delayedErr(start time.Time, err error) error {
	time.Sleep(time.Until(start.Add(s.delay)))
	return err
}

func tokenInternalErr(e error) error {
	return &token.InternalError{Message: e.Error()}
}

func tokenBadReqErr(e error) error {
	return &token.BadRequestError{Message: e.Error()}
}

func tokenUnauthErr(e error) error {
	return tokenUnauthErrMsg(e.Error())
}

func tokenUnauthErrMsg(msg string) error {
	return &token.UnauthorizedError{Message: msg}
}

var errInvalidCreds = tokenUnauthErrMsg("invalid email or password")
