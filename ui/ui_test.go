// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ui_test

import (
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"testing/fstest"

	httpu "github.com/antichris/go-feedreader/internal/util/http"
	"github.com/antichris/go-feedreader/internal/util/testu"
	. "github.com/antichris/go-feedreader/ui"
	"github.com/stretchr/testify/require"
)

func TestService_Register(t *testing.T) {
	t.Parallel()
	type pattern int
	const (
		pRoot pattern = iota
		pRootIndex
		pRootFavicon
		pStatic
		pStaticDir
		pStaticIndex
		pStaticFavicon
	)
	const (
		vFavicon = "/" + fFavicon
		vIndex   = "/index.html"
		vStatic  = "static"

		indexContent = "<title>Reader</title>"
	)
	indexContentBS := []byte(indexContent)

	pp := []string{
		pRoot:          "/",
		pRootIndex:     vIndex,
		pRootFavicon:   vFavicon,
		pStatic:        "",
		pStaticDir:     "/",
		pStaticIndex:   vIndex,
		pStaticFavicon: vFavicon,
	}
	for i := pStatic; int(i) < len(pp); i++ {
		pp[i] = "/" + vStatic + pp[i]
	}
	t.Logf("%#v", pp)

	newRequest := func(t int) *http.Request {
		return httptest.NewRequest(http.MethodGet, pp[t], nil)
	}
	withStatic := func(c Config) *Config {
		c.StaticPath = vStatic
		return &c
	}

	bFavicon := func() string {
		b, err := fs.ReadFile(FS, fFavicon)
		if err != nil {
			t.Fatalf("reading favicon file: %s", err)
		}
		return string(b)
	}()
	// bMoved := func(path string) string {
	// 	return "<a href=\"" + path + "\">Moved Permanently</a>.\n\n"
	// }

	type want struct {
		pattern   string
		status    int
		location  string
		body      string
		exactBody bool
	}

	wNotFound := func(pattern string) want {
		return want{
			pattern:   pattern,
			status:    sNotFound,
			body:      "404 page not found\n",
			exactBody: true,
		}
	}
	wUnhandled := wNotFound("")
	wIndexOK := func(pattern string) want {
		return want{
			pattern: pattern,
			status:  sOK,
			body:    "Reader</title>",
		}
	}
	wFaviconAt := func(pattern string) want {
		return want{
			pattern:   pattern,
			status:    sOK,
			body:      bFavicon,
			exactBody: true,
		}
	}
	wFavicon := wFaviconAt(pp[pStaticDir])

	wNominal := []want{
		pRoot:          wIndexOK(pp[pRoot]),
		pRootIndex:     wIndexOK(pp[pRoot]),
		pRootFavicon:   wNotFound(pp[pRoot]),
		pStatic:        wIndexOK(pp[pStatic]),
		pStaticDir:     wIndexOK(pp[pStaticDir]),
		pStaticIndex:   wIndexOK(pp[pStaticDir]),
		pStaticFavicon: wFavicon,
	}
	for n, tt := range map[string]struct {
		c    *Config
		wErr string
		want []want
	}{`files and index "only"`: {
		c:    withStatic(Config{FilesOnly: true, IndexOnly: true}),
		wErr: "neither index nor file handler registered",
		want: []want{
			pRoot:          wUnhandled,
			pRootIndex:     wUnhandled,
			pRootFavicon:   wUnhandled,
			pStatic:        wUnhandled,
			pStaticDir:     wUnhandled,
			pStaticIndex:   wUnhandled,
			pStaticFavicon: wUnhandled,
		},
	}, "files only": {
		c: withStatic(Config{FilesOnly: true}),
		want: []want{
			pRoot:          wUnhandled,
			pRootIndex:     wUnhandled,
			pRootFavicon:   wUnhandled,
			pStatic:        wNotFound(pp[pStatic]),
			pStaticDir:     wNotFound(pp[pStaticDir]),
			pStaticIndex:   wNotFound(pp[pStaticDir]),
			pStaticFavicon: wFavicon,
		},
	}, "files only at root": {
		c: &Config{FilesOnly: true},
		want: []want{
			pRoot:          wNotFound(pp[pRoot]),
			pRootIndex:     wNotFound(pp[pRoot]),
			pRootFavicon:   wFaviconAt(pp[pRoot]),
			pStatic:        wNotFound(pp[pRoot]),
			pStaticDir:     wNotFound(pp[pRoot]),
			pStaticIndex:   wNotFound(pp[pRoot]),
			pStaticFavicon: wNotFound(pp[pRoot]),
		},
	}, "index only": {
		c: withStatic(Config{IndexOnly: true}),
		want: []want{
			pRoot:          wIndexOK(pp[pRoot]),
			pRootIndex:     wIndexOK(pp[pRoot]),
			pRootFavicon:   wNotFound(pp[pRoot]),
			pStatic:        wIndexOK(pp[pRoot]),
			pStaticDir:     wIndexOK(pp[pRoot]),
			pStaticIndex:   wIndexOK(pp[pRoot]),
			pStaticFavicon: wNotFound(pp[pRoot]),
		},
	}, "files and index at root": {
		c: &Config{},
		want: []want{
			pRoot:          wIndexOK(pp[pRoot]),
			pRootIndex:     wIndexOK(pp[pRoot]),
			pRootFavicon:   wFaviconAt(pp[pRoot]),
			pStatic:        wIndexOK(pp[pRoot]),
			pStaticDir:     wIndexOK(pp[pRoot]),
			pStaticIndex:   wIndexOK(pp[pRoot]),
			pStaticFavicon: wNotFound(pp[pRoot]),
		},
	}, "absolute static": {
		c:    &Config{StaticPath: "/" + vStatic},
		want: wNominal,
	}, "nominal": {
		c:    withStatic(Config{}),
		want: wNominal,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			m := http.NewServeMux()

			svc := New(tt.c, FS, indexContentBS)
			err := svc.Register(m)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				// TODO Consider aborting here and not checking handlers.
			} else {
				r.NoError(err)
			}

			for i, p := range pp {
				i, p := i, p
				t.Run(fmt.Sprintf("target=%v", p), func(t *testing.T) {
					t.Parallel()
					w := tt.want[i]
					q := newRequest(i)
					h, p := m.Handler(q)

					r := require.New(t)
					r.Equal(w.pattern, p)

					c := httptest.NewRecorder()
					h.ServeHTTP(c, q)
					s := c.Result()

					b, err := io.ReadAll(s.Body)
					r.NoError(err)

					r.Equal(w.status, s.StatusCode)
					if len(w.location) == 0 {
						_, err := s.Location()
						r.EqualError(err, http.ErrNoLocation.Error())
					} else {
						l, err := s.Location()
						r.NoError(err)
						r.Equal(w.location, l.String(), err)
					}
					if len(w.body) == 0 {
						r.Empty(string(b))
					} else {
						if w.exactBody {
							r.Equal(string(b), w.body)
						} else {
							r.Contains(string(b), w.body)
						}
					}
				})
			}
		})
	}
}

func Test_prepareIndexContent(t *testing.T) {
	t.Parallel()

	tag := func(tag string, contents ...string) string {
		return fmt.Sprintf("<%s>%s</%s>",
			tag, strings.Join(contents, ""), tag)
	}
	headOnly := func(contents ...string) string {
		return tag("html", tag("head", contents...), tag("body"))
	}
	iconTag := func(href string) string {
		return `<link rel="icon" href="` + href + `"/>`
	}
	stylesheetTag := func(href string) string {
		return `<link rel="stylesheet" href="` + href + `"/>`
	}
	scriptTag := func(href string) string {
		return `<script src="` + href + `"></script>`
	}
	importMap := func(contents ...string) string {
		return `<script type="importmap">{"imports":{` +
			strings.Join(contents, ",") + `}}</script>`
	}
	frCfg := func(contents ...string) string {
		return `<script type="application/json" data-fr-cfg="">{` +
			strings.Join(contents, ",") + `}</script>`
	}

	const (
		pStatic = "static"
		pIndex  = "ui"

		fScript      = "script.js"
		remote       = "//example.com/"
		remoteScript = remote + fScript
	)
	script := scriptTag(fScript)

	for name, tt := range map[string]struct {
		indexPath  string
		staticPath string
		apiBaseURL string
		src        string
		want       string
		wErr       string
	}{"no-op:base without href": {
		indexPath:  pIndex,
		staticPath: pStatic,
		src:        "<base>",
		want:       headOnly("<base/>"),
	}, "no-op:comment": {
		indexPath:  pIndex,
		staticPath: pStatic,
		src:        "<!--comment-->",
		want:       "<!--comment-->" + headOnly(),
	}, "no-op:link without href": {
		staticPath: pStatic,
		src:        "<link foo=bar>",
		want:       headOnly(`<link foo="bar"/>`),
	}, "no-op:favicon without static path": {
		src:  iconTag(fFavicon),
		want: headOnly(iconTag(fFavicon)),
	}, "no-op:script cleans to root": {
		staticPath: "/../././../",
		src:        script,
		want:       headOnly(script),
	}, "no-op:remote script": {
		staticPath: pStatic,
		src:        scriptTag(remoteScript),
		want:       headOnly(scriptTag(remoteScript)),
	}, "no-op:remote script with protocol": {
		staticPath: pStatic,
		src:        scriptTag("https:" + remoteScript),
		want:       headOnly(scriptTag("https:" + remoteScript)),
	}, "no-op:inline script": {
		staticPath: pStatic,
		src:        tag("script"),
		want:       headOnly(tag("script")),
	}, "no-op:importmap remote URL": {
		staticPath: pStatic,
		src:        importMap(`"foo":"` + remoteScript + `"`),
		want:       headOnly(importMap(`"foo":"` + remoteScript + `"`)),
	}, "no-op:application/json": {
		staticPath: pStatic,
		src:        `<script type="application/json">{}</script>`,
		want:       headOnly(`<script type="application/json">{}</script>`),
	}, "borkt importmap": {
		staticPath: pStatic,
		src:        importMap(`"`),
		wErr: "prefixing file URLs: importmap: unmarshal:" +
			" unexpected end of JSON input",
	}, "borkt fr-cfg": {
		staticPath: pStatic,
		src:        frCfg(`"`),
		wErr:       "injecting fr-cfg: unmarshal: unexpected end of JSON input",
	}, "base href": {
		indexPath: pIndex,
		src:       `<base href="/">`,
		want:      headOnly(`<base href="/ui/"/>`),
	}, "favicon": {
		staticPath: pStatic,
		src:        iconTag(fFavicon),
		want:       headOnly(iconTag(pStatic + "/" + fFavicon)),
	}, "stylesheet": {
		staticPath: pStatic,
		src:        stylesheetTag("styles.css"),
		want:       headOnly(stylesheetTag("static/styles.css")),
	}, "script": {
		staticPath: pStatic,
		src:        script,
		want:       headOnly(scriptTag(pStatic + "/" + fScript)),
	}, "script with absolute static path": {
		staticPath: "/" + pStatic,
		src:        script,
		want:       headOnly(scriptTag("/" + pStatic + "/" + fScript)),
	}, "script with absolute static and index paths": {
		indexPath:  pIndex,
		staticPath: "/" + pStatic,
		src:        script,
		want:       headOnly(scriptTag("/" + pStatic + "/" + fScript)),
	}, "script with static relative to index path": {
		indexPath:  pIndex,
		staticPath: pStatic,
		src:        script,
		want:       headOnly(scriptTag(pStatic + "/" + fScript)),
	}, "no-op:script with explicit static == index path": {
		indexPath:  pIndex,
		staticPath: "/" + pIndex,
		src:        script,
		want:       headOnly(script),
	}, "importmap": {
		staticPath: pStatic,
		src:        importMap(`"foo":"bar"`),
		want:       headOnly(importMap(`"foo":"./` + pStatic + `/bar"`)),
	}, "API base URL": {
		apiBaseURL: "https:" + remote + "api",
		src:        frCfg(`"apiBaseURL":""`),
		want: headOnly(frCfg(`"apiBaseURL":"` +
			"https:" + remote + "api" + `"`)),
	}} {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			c := &Config{
				IndexPath:  tt.indexPath,
				StaticPath: tt.staticPath,
				APIURL:     tt.apiBaseURL,
			}
			// c.ServerConfig
			got, err := XprepareIndexContent([]byte(tt.src), c)
			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, string(got))
		})
	}
}

func Test_addCORS(t *testing.T) {
	t.Parallel()
	const canonical = "https://feedreader.tld"
	for _, tt := range []struct {
		want      bool
		wStatus   int
		origin    string
		canonical string
	}{
		{want: false, wStatus: sOK, origin: ""}, // XXX Allow non-CORS.
		{want: false, wStatus: sForbidden, origin: "http://example.com"},
		{want: false, wStatus: sForbidden, origin: "https://example.com"},
		{want: false, wStatus: sForbidden, origin: "http://localhost:65536"},
		{want: false, wStatus: sForbidden, origin: "http://[::1]:65536"},
		{want: false, wStatus: sForbidden, origin: canonical},
		{want: true, wStatus: sOK, origin: canonical, canonical: canonical},
		{want: true, wStatus: sOK, origin: "http://localhost"},
		{want: true, wStatus: sOK, origin: "https://foo-bar.localhost:65535"},
		{want: true, wStatus: sOK, origin: "http://127.0.0.1"},
		{want: true, wStatus: sOK, origin: "https://127.254.254.254:65535"},
		{want: true, wStatus: sOK, origin: "http://[::1]"},
		{want: true, wStatus: sOK, origin: "https://[::1]:65535"},
	} {
		tt := tt
		t.Run(fmt.Sprintf("%q", tt.origin), func(t *testing.T) {
			t.Parallel()
			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodGet, "/", &strings.Reader{})

			m := testu.NewFuncMock(t, "handler")
			gotH := XaddCORS(http.HandlerFunc(func(
				w http.ResponseWriter, r *http.Request,
			) {
				m.Called(w, r)
			}), tt.canonical)
			if tt.wStatus == sOK {
				m.On(w, r)
			}

			r.Header.Set(XhOrigin, tt.origin)
			gotH.ServeHTTP(w, r)
			h := w.Result().Header
			got := httpu.HasHeader(h, XhAllowOrigin) &&
				h.Get(XhAllowOrigin) == tt.origin

			require.Equal(t, tt.want, got)

			gotStatus := w.Result().StatusCode
			require.Equal(t, tt.wStatus, gotStatus)
			m.AssertExpectations(t)
		})
	}
}

func Test_isExternal(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		s    string
		want bool
	}{
		"local":         {"/path", false},
		"protocol-less": {"//host/path", true},
		"HTTP":          {"http://host/path", true},
		"HTTPS":         {"https://host/path", true},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := XisExternal(tt.s)
			require.Equal(t, tt.want, got)
		})
	}
}

const (
	sOK = http.StatusOK
	// sMoved    = http.StatusMovedPermanently
	sNotFound  = http.StatusNotFound
	sForbidden = http.StatusForbidden
)

const fFavicon = "favicon.ico"

var FS = fstest.MapFS{
	fFavicon: &fstest.MapFile{},
}
