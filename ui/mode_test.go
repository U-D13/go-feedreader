// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ui_test

import (
	"testing"

	. "github.com/antichris/go-feedreader/ui"
	"github.com/stretchr/testify/require"
)

func TestConfig_Mode(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		FilesOnly bool
		IndexOnly bool
		want      Mode
	}{"undefined": {
		FilesOnly: true,
		IndexOnly: true,
		want:      0,
	}, "files": {
		FilesOnly: true,
		want:      FileMode,
	}, "index": {
		IndexOnly: true,
		want:      IndexMode,
	}, "comprehensive": {
		want: ComprehensiveMode,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			c := Config{
				FilesOnly: tt.FilesOnly,
				IndexOnly: tt.IndexOnly,
			}
			got := CalcMode(c)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMode_IsValid(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		want bool
		Mode
	}{
		"zero":          {false, 0},
		"invalid":       {false, ComprehensiveMode + 1},
		"index":         {true, IndexMode},
		"file":          {true, FileMode},
		"comprehensive": {true, ComprehensiveMode},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := tt.IsValid()
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMode_IsFileServer(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		want bool
		Mode
	}{
		"zero":          {false, 0},
		"invalid":       {false, ComprehensiveMode + 1},
		"index":         {false, IndexMode},
		"file":          {true, FileMode},
		"comprehensive": {true, ComprehensiveMode},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := tt.IsFileServer()
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMode_IsIndexServer(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		want bool
		Mode
	}{
		"zero":          {false, 0},
		"invalid":       {false, ComprehensiveMode + 1},
		"index":         {true, IndexMode},
		"file":          {false, FileMode},
		"comprehensive": {true, ComprehensiveMode},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := tt.IsIndexServer()
			require.Equal(t, tt.want, got)
		})
	}
}
