// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package angular_test

import (
	"io/fs"
	"net/http"
	"testing"

	"github.com/antichris/go-feedreader/ui"
	. "github.com/antichris/go-feedreader/ui/angular"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	t.Parallel()
	got := New(&ui.Config{})

	mux := http.NewServeMux()
	err := got.Register(mux)
	r := require.New(t)
	r.NoError(err)
	r.HTTPSuccess(func(w http.ResponseWriter, r *http.Request) {
		mux.ServeHTTP(w, r)
	}, "GET", "/", nil)
	// TODO Assert on response to verify proper UI implementation.
}

func TestFSHasContents(t *testing.T) {
	t.Parallel()
	f, err := fs.Stat(FS, IndexFileName)
	r := require.New(t)
	r.NoError(err)
	r.NotEmpty(f)
}
