// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package angular is an Angular implementation of a GUI frontend
// for the Feed Reader.
package angular

import (
	"embed"
	"os"

	fsu "github.com/antichris/go-feedreader/internal/util/fs"
	"github.com/antichris/go-feedreader/ui"
	"github.com/laher/mergefs"
)

func New(c *ui.Config) *ui.Service {
	return ui.New(c, FS, IndexContent)
}

//go:embed _resources/dist/feedreader
var fs embed.FS

var FS = mergefs.Merge(fsu.MustSubAll("_resources/dist/feedreader",
	os.DirFS("ui/angular"), // TODO Test this.
	fs,
)...)

var IndexContent = fsu.MustReadFile(FS, IndexFileName)

const IndexFileName = "index.html"
