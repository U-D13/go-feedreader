// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ui

func CalcMode(c Config) (m Mode) {
	m = ComprehensiveMode
	if c.FilesOnly {
		m &^= IndexMode
	}
	if c.IndexOnly {
		m &^= FileMode
	}
	return
}

type Mode int

const (
	FileMode Mode = 1 << iota
	IndexMode
	// APIMode

	// A union of all supported modes.
	ComprehensiveMode = FileMode | IndexMode // | APIMode
)

func (m Mode) IsValid() bool {
	return m&ComprehensiveMode != 0
}

func (m Mode) IsFileServer() bool {
	return m&FileMode != 0
}

func (m Mode) IsIndexServer() bool {
	return m&IndexMode != 0
}
