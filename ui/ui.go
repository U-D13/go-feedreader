// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package ui implements server for the Feed Reader web UI.
package ui

// TODO Beat this into a more presentable and testable shape.

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"net/http"
	"path"
	"regexp"
	"strings"

	"golang.org/x/exp/slog"
	"golang.org/x/net/html"

	htmlu "github.com/antichris/go-feedreader/internal/util/html"
	httpu "github.com/antichris/go-feedreader/internal/util/http"
	stringsu "github.com/antichris/go-feedreader/internal/util/strings"
	"github.com/antichris/go-feedreader/terror"
)

func New(c *Config, fsys fs.FS, indexContent []byte) *Service {
	ip, sp := mountPaths(c)
	return &Service{
		c:   *c,
		f:   fsys,
		ic:  indexContent,
		im:  ip,
		sdm: sp,
		sfm: stringsu.TrimTrailingSlash(sp),
	}
}

type Config struct {
	FilesOnly  bool
	IndexOnly  bool
	StaticPath string
	IndexPath  string
	APIURL     string
	UIOrigin   string
}

type Service struct {
	c   Config
	f   fs.FS
	ic  []byte
	im  string // Index mount prefix.
	sdm string // Static directory mount prefix (with trailing slash).
	sfm string // Static "file" mount prefix (without trailing slash).
}

func (svc *Service) Register(mux Muxer) error {
	index, static, err := svc.GetHandlers()
	if err != nil {
		return err
	}
	// FIXME Redirect index file to directory (e.g. "/ui" to "/ui/")
	if index != nil {
		mux.Handle(svc.im, index)
	}
	if static != nil {
		if len(svc.sdm) > 1 {
			mux.Handle(svc.sfm, static)
		}
		mux.Handle(svc.sdm, static)
	}
	return nil
}

func (svc *Service) Mount(mux GoaMuxer) error {
	index, static, err := svc.GetHandlers()
	if err != nil {
		return err
	}
	if index != nil {
		mux.Handle("GET", svc.im, index)
		mux.Handle("GET", svc.im+"{*ignored}", index)
	}
	if static != nil {
		mux.Handle("GET", svc.sdm, static)
		mux.Handle("GET", svc.sdm+"{*ignored}", static)
	}
	return nil
}

func (svc *Service) GetHandlers() (index, static http.HandlerFunc, err error) {
	if svc.c.FilesOnly && svc.c.IndexOnly {
		// TODO Redesign so that this can never happen.
		err = errors.New("neither index nor file handler registered")
		return
	}
	indexHandler := http.HandlerFunc(http.NotFound)
	if !svc.c.FilesOnly {
		if indexHandler, err = svc.IndexHandler(); err != nil {
			return
		}
		if svc.sdm != svc.im || svc.c.IndexOnly {
			index = indexHandler
		}
	}
	if !svc.c.IndexOnly {
		static = getStaticHandler(svc.sdm, svc.f, indexHandler, svc.c.UIOrigin)
	}
	return
}

func (svc *Service) IndexHandler() (h http.HandlerFunc, err error) {
	var b []byte
	if b, err = prepareIndexContent(svc.ic, &svc.c); err != nil {
		return
	}
	h = getIndexHandler(b)
	return
}

func mountPaths(c *Config) (index, static string) {
	index = "/" + c.IndexPath
	if path.IsAbs(c.StaticPath) || isExternal(c.StaticPath) {
		static = c.StaticPath
	} else {
		static = path.Join(index, c.StaticPath)
	}
	index = stringsu.EnsureTrailingSlash(index)
	static = stringsu.EnsureTrailingSlash(static)
	return
}

func prepareIndexContent(src []byte, c *Config) (r []byte, err error) {
	n, err := html.Parse(bytes.NewReader(src))
	if err != nil {
		return nil, fmt.Errorf("parsing index HTML failed: %w", err)
	}

	ip, sp := prefixPaths(c)
	if len(ip) > 1 {
		replaceBaseHref(n, ip)
	}
	if len(sp) > 0 {
		if err = prefixFileUrls(n, sp); err != nil {
			return nil, fmt.Errorf("prefixing file URLs: %w", err)
		}
	}
	if err = injectFRCfg(n, c); err != nil {
		return nil, fmt.Errorf("injecting fr-cfg: %w", err)
	}

	// TODO Optimize to reduce allocations.
	b := &bytes.Buffer{}
	if err := html.Render(b, n); err != nil {
		return nil, fmt.Errorf("rendering index HTML failed: %w", err)
	}
	return b.Bytes(), nil
}

func prefixPaths(c *Config) (index, static string) {
	index = path.Clean("/" + c.IndexPath)
	index = stringsu.EnsureTrailingSlash(index)
	s := c.StaticPath
	if !isExternal(s) {
		s = path.Clean(s)
	}
	if s == "." {
		return
	}

	s = stringsu.EnsureTrailingSlash(s)
	if s == index {
		return
	}

	static = s
	return
}

func getIndexHandler(content []byte) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch path.Ext(r.URL.Path) {
		case ".js", ".css", ".ico", ".txt":
			http.NotFound(w, r)
		default:
			if _, err := w.Write(content); err != nil {
				httpu.Error(w, http.StatusInternalServerError)
				if l := slog.Ctx(r.Context()); l != nil {
					l.Error("serve index", err)
				}
			}
		}
	}
}

func getStaticHandler(
	p string,
	fsys fs.FS,
	indexHandler http.Handler,
	canonicalOrigin string,
) http.HandlerFunc {
	pp := p
	if len(p) > 1 {
		pp = stringsu.TrimTrailingSlash(pp)
	}

	fileHandler := addCORS(getFileHandler(p, fsys), canonicalOrigin)

	return func(w http.ResponseWriter, r *http.Request) {
		u := r.URL.Path
		switch {
		case u == p, u == pp, strings.HasSuffix(u, "/index.html"):
			indexHandler.ServeHTTP(w, r)
		default:
			_, err := fs.Stat(
				fsys,
				strings.TrimSuffix(strings.TrimPrefix(u, p), "/"),
			)
			if errors.Is(err, fs.ErrNotExist) {
				indexHandler.ServeHTTP(w, r)
				return
			}
			fileHandler.ServeHTTP(w, r)
		}
	}
}

func getFileHandler(prefix string, fsys fs.FS) http.Handler {
	return http.StripPrefix(prefix, http.FileServer(http.FS(fsys)))
}

func addCORS(h http.Handler, canonicalOrigin string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !allowOrigin(w, r, canonicalOrigin) {
			httpu.LeanError(w, http.StatusForbidden)
			return
		}
		h.ServeHTTP(w, r)
	}
}

func allowOrigin(
	w http.ResponseWriter, r *http.Request, canonicalOrigin string,
) bool {
	o := r.Header.Get(hOrigin)
	if len(o) == 0 {
		return true
	}
	if o == canonicalOrigin {
		addAllowOrigin(w, o)
		return true
	}
	for _, rx := range rxCORSOrigins {
		if rx.MatchString(o) {
			addAllowOrigin(w, o)
			return true
		}
	}
	return false
}

func addAllowOrigin(w http.ResponseWriter, origin string) {
	w.Header().Add(hAllowOrigin, origin)
}

const (
	hOrigin      = "Origin"
	hAllowOrigin = "Access-Control-Allow-Origin"
)

func prefixFileUrls(n *html.Node, prefix string) error {
	pfx := func(s string) string { return prefixLocal(s, prefix) }
	// HACK Importmap requires explicitly relative paths (./foo/x vs foo/x).
	// TODO Implement a more robust solution.
	pfxIM := func(s string) string {
		p := pfx(s)
		if path.IsAbs(p) {
			return p
		}
		return "./" + p
	}
	return htmlu.Walk(n, func(n *html.Node) (err error) {
		if n.Type != html.ElementNode {
			return
		}
		switch n.Data {
		case "script":
			if ok := htmlu.TransformAttrValue(n, "src", pfx); ok {
				break
			}
			if !hasTypeAttr(n, "importmap") {
				break
			}
			return prefixImportMap(n, pfxIM)
		case "link":
			rel, ok := htmlu.GetAttrValue(n, "rel")
			if !ok {
				break // There's nothing we can do for a link without `rel`.
			}
			if rel == "stylesheet" || rel == "icon" {
				// A `link` without `href` does not concern us.
				_ = htmlu.TransformAttrValue(n, "href", pfx)
			}
		}
		return
	})
}

func injectFRCfg(n *html.Node, c *Config) (err error) {
	const errDone = terror.Error("done")
	err = htmlu.Walk(n, func(n *html.Node) (err error) {
		if n.Type != html.ElementNode {
			return
		}
		if n.Data != "script" {
			return
		}
		if !hasTypeAttr(n, "application/json") {
			return
		}
		if _, ok := htmlu.GetAttrValue(n, "data-fr-cfg"); !ok {
			return
		}
		return htmlu.Walk(n, func(n *html.Node) (err error) {
			if n.Type != html.TextNode {
				return
			}
			m := struct {
				APIBaseURL string `json:"apiBaseURL"`
			}{}
			err = transformNodeJSONData(n, &m, func() error {
				m.APIBaseURL = c.APIURL
				return nil
			})
			if err != nil {
				return
			}
			return errDone
		})
	})
	if err == errDone {
		return nil
	}
	return
}

func hasTypeAttr(n *html.Node, v string) bool {
	t, ok := htmlu.GetAttrValue(n, "type")
	return ok && t == v
}

func prefixLocal(s string, prefix string) string {
	if isExternal(s) {
		return s
	}
	return prefix + path.Clean(s)
}

func prefixImportMap(n *html.Node, pfx func(s string) string) (err error) {
	err = htmlu.Walk(n, func(n *html.Node) (err error) {
		if n.Type != html.TextNode {
			return
		}
		m := struct {
			Imports map[string]string `json:"imports"`
		}{}
		return transformNodeJSONData(n, &m, func() error {
			for k, v := range m.Imports {
				m.Imports[k] = pfx(v)
			}
			return nil
		})
	})
	if err != nil {
		return fmt.Errorf("importmap: %w", err)
	}
	return
}

func transformNodeJSONData(
	n *html.Node,
	v any,
	fn func() error,
) (err error) {
	// TODO Consider using a decoder instance here.
	// err = json.NewDecoder(strings.NewReader(n.Data)).Decode(&m)
	if err = json.Unmarshal([]byte(n.Data), v); err != nil {
		return fmt.Errorf("unmarshal: %w", err)
	}
	if err = fn(); err != nil {
		return fmt.Errorf("transform: %w", err)
	}
	// TODO Consider using an encoder instance here.
	b, err := json.Marshal(v)
	if err != nil {
		// I can't imagine how this could happen, but...
		return fmt.Errorf("marshal: %w", err)
	}
	n.Data = string(b)
	return
}

func isExternal(s string) bool {
	// TODO Consider alternative external protocols, besides HTTP(S).
	for _, v := range []string{"//", "https://", "http://"} {
		if strings.HasPrefix(s, v) {
			return true
		}
	}
	return false
}

func replaceBaseHref(n *html.Node, href string) {
	yieldHRef := func(string) string { return href }
	errDone := errors.New("done")
	// The only possible error returned here signals successful completion,
	// there is no need to look at it.
	_ = htmlu.Walk(n, func(n *html.Node) (err error) {
		if n.Type != html.ElementNode || n.Data != "base" {
			return
		}
		if ok := htmlu.TransformAttrValue(n, "href", yieldHRef); !ok {
			return // Not it. Keep walking.
		}
		return errDone
	})
}

var rxCORSOrigins = func(s []string) []*regexp.Regexp {
	r := make([]*regexp.Regexp, len(s))
	var err error
	for i, v := range s {
		if r[i], err = regexp.Compile(v); err != nil {
			panic(fmt.Errorf("CORS Origin regexp #%d: %w", i, err))
		}
	}
	return r
}([]string{
	rxHTTPOptS + `([0-9a-z.-]+\.)?localhost` + rxAnyPortNum + `$`,
	rxHTTPOptS + `127\.(?:(?:1?[0-9]{1,2}|2[0-4][0-9]|25[0-4])\.){2}(?:[1-9][0-9]?|1[0-9]{2}|2[0-4][0-9]|25[0-4])` + rxAnyPortNum + `$`,
	rxHTTPOptS + `\[::1\]` + rxAnyPortNum + `$`,
})

const (
	rxHTTPOptS   = `^https?://`
	rxAnyPortNum = `(?::(?:[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|` +
		`65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]))?`
)

// Muxer is an HTTP request multiplexer, such as `http.ServeMux`.
type Muxer interface {
	// Handle registers the handler for the given pattern.
	// If a handler already exists for pattern, Handle panics.
	Handle(pattern string, handler http.Handler)
}

// GoaMuxer is the HTTP request multiplexer interface used by the generated
// code. ServerHTTP must match the HTTP method and URL of each incoming
// request against the list of registered patterns and call the handler
// for the corresponding method and the pattern that most closely
// matches the URL.
//
// The patterns may include wildcards that identify URL segments that
// must be captured.
//
// There are two forms of wildcards the implementation must support:
//
//   - "{name}" wildcards capture a single path segment, for example the
//     pattern "/images/{name}" captures "/images/favicon.ico" and adds
//     the key "name" with the value "favicon.ico" to the map returned
//     by Vars.
//
//   - "{*name}" wildcards must appear at the end of the pattern and
//     captures the entire path starting where the wildcard matches. For
//     example the pattern "/images/{*filename}" captures
//     "/images/public/thumbnail.jpg" and associates the key key
//     "filename" with "public/thumbnail.jpg" in the map returned by
//     Vars.
//
// The names of wildcards must match the regular expression
// "[a-zA-Z0-9_]+".
type GoaMuxer interface {
	// Handle registers the handler function for the given method
	// and pattern.
	Handle(method, pattern string, handler http.HandlerFunc)

	// Vars returns the path variables captured for the given
	// request.
	Vars(*http.Request) map[string]string
}
