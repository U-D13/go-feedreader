// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package auth_test

import (
	"context"
	"errors"
	"fmt"
	"testing"

	. "github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/token"
	tokenMocks "github.com/antichris/go-feedreader/token/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestContextUserID(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		ctx  context.Context
		wID  uint32
		wErr error
	}{
		"ID absent": {ctx: ctx, wErr: ErrNoContextUserID},
		"nominal": {
			ctx: context.WithValue(ctx, XuserIDContextKey, userID),
			wID: userID,
		},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			gotID, gotErr := ContextUserID(tt.ctx)

			r := require.New(t)
			if tt.wErr != nil {
				r.ErrorIs(gotErr, tt.wErr)
				return
			}
			r.NoError(gotErr)
			r.Equal(tt.wID, gotID)
		})
	}
}

func Test_service_APIKeyAuth(t *testing.T) {
	t.Parallel()
	var (
		unauth = prefixErrWrapper("unauth")
		badReq = prefixErrWrapper("badreq")
		snap   = errors.New("snap")
	)
	for n, tt := range map[string]struct {
		key    string
		userID uint32
		tsErr  error
		wCtx   context.Context
		wErrW  ErrorWrapper
	}{"expired": {
		tsErr: token.ErrExpired,
		wErrW: unauth,
	}, "invalid signature": {
		tsErr: token.ErrInvalidSignature,
		wErrW: unauth,
	}, "unsupported format": {
		tsErr: token.ErrUnsupportedFormat,
		wErrW: unauth,
	}, "other token error": {
		tsErr: snap,
		wErrW: badReq,
	}, "nominal": {
		userID: userID,
		wCtx:   context.WithValue(ctx, XuserIDContextKey, userID),
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var wErr error
			if tt.wErrW != nil {
				wErr = tt.wErrW(tt.tsErr)
			}

			ts := tokenMocks.NewMockService(gomock.NewController(t))
			ts.EXPECT().
				GetUserID(Token(tt.key)).
				Return(tt.userID, tt.tsErr)

			s := New(ts)
			gotCtx, gotErr := s.APIKeyAuth(ctx, tt.key, unauth, badReq)

			r := require.New(t)
			if wErr != nil {
				r.EqualError(gotErr, wErr.Error())
				return
			}
			r.NoError(gotErr)
			r.Equal(tt.wCtx, gotCtx)
		})
	}
}

func Test_service_HashAndComparePassword(t *testing.T) {
	t.Parallel()
	pass := []byte("password123")

	s := New(nil)

	got, err := s.HashPassword(pass)

	r := require.New(t)
	r.NoError(err)
	r.Contains(string(got), "$")

	err = s.ComparePasswordHash(got, pass)
	r.NoError(err)
}

const userID uint32 = 13

var ctx = context.Background()

// prefixErrWrapper returns an [ErrorWrapper] that adds prefix to the
// error it is given.
func prefixErrWrapper(prefix string) ErrorWrapper {
	return func(e error) error {
		return fmt.Errorf("%s: %w", prefix, e)
	}
}
