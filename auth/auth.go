// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package auth implements authentication and authorization tools.
//
// [Service] handles authorization via an rJWT (reduced JWT) token (as
// [APIKeyAuther]), and password hashing and validation of password
// hashes (as [PasswordHasher]).
//
// [ContextUserID] extracts user ID from context, if one has been set
// there by the [APIKeyAuther] implementation in this package.
package auth

import (
	"context"
	"errors"

	"github.com/antichris/go-feedreader/terror"
	"github.com/antichris/go-feedreader/token"
	"golang.org/x/crypto/bcrypt"
)

type (
	// Token is an rJWT (reduced JWT) token.
	//
	// This is a convenience alias for [token.Token].
	Token = token.Token
	// Error is a type of string that formats as its value and can be
	// used in constant definitions.
	//
	// This is a convenience alias for [terror.Error].
	Error = terror.Error
)

// ContextUserID returns the user ID, if one has been set in the
// context, otherwise it returns [ErrNoContextUserID].
func ContextUserID(ctx context.Context) (id uint32, err error) {
	// TODO Consider doing this in Service instead, so it can be mocked.
	id, ok := ctx.Value(userIDContextKey).(uint32)
	if !ok {
		err = ErrNoContextUserID
	}
	return
}

// SetContextUserID in a copy of ctx and return it.
func SetContextUserID(ctx context.Context, id uint32) context.Context {
	// TODO Consider removal in favor of mockable service.ContextUserID.
	return context.WithValue(ctx, userIDContextKey, id)
}

// New auth [Service] instance.
func New(ts token.Service) Service {
	return &service{ts: ts}
}

type (
	// Service fulfills authentication and authorization needs.
	Service interface {
		APIKeyAuther
		PasswordHasher
	}
	// APIKeyAuther provides a method that implements the authorization
	// logic for the rJWT (reduced JWT) security scheme.
	APIKeyAuther interface {
		// APIKeyAuth implements the authorization logic for the rJWT
		// (reduced JWT) security scheme.
		APIKeyAuth(
			ctx context.Context,
			key string,
			unauthorizedErr ErrorWrapper,
			badRequestErr ErrorWrapper,
		) (context.Context, error)
	}
	// PasswordHasher generates and validates password hashes.
	PasswordHasher interface {
		// HashPassword returns hash of the password.
		// Use ComparePasswordHash to compare the hashed password
		// returned by this hasher implementation with its cleartext
		// version.
		HashPassword(password []byte) ([]byte, error)
		// ComparePasswordHash compares a hashed password with its
		// possible plaintext equivalent.
		// Returns nil on success, or an error on failure.
		ComparePasswordHash(hashedPassword, password []byte) error
	}
	// An ErrorWrapper takes an error type that is internal to the
	// application components and turns it into an API error type.
	ErrorWrapper func(error) error // TODO Reevaluate this approach.
)

// ErrNoContextUserID is returned by [ContextUserID] when context does
// not hold a user ID that would be set by the implementation in this
// package.
const ErrNoContextUserID = Error("context does not hold user ID")

// ErrMismatchedHashAndPassword is returned by
// [PasswordHasher].ComparePasswordHash when a password and hash do not
// match.
var ErrMismatchedHashAndPassword = bcrypt.ErrMismatchedHashAndPassword

var _ Service = (*service)(nil)

type service struct {
	ts token.Service
}

func (s *service) APIKeyAuth(
	ctx context.Context, key string, unauthErr, badReqErr ErrorWrapper,
) (context.Context, error) {
	userID, err := s.ts.GetUserID(Token(key))
	if err != nil {
		if errors.Is(err, token.ErrExpired) ||
			errors.Is(err, token.ErrInvalidSignature) ||
			errors.Is(err, token.ErrUnsupportedFormat) {
			return ctx, unauthErr(err)
		}
		return ctx, badReqErr(err)
	}
	return SetContextUserID(ctx, userID), nil
}

func (*service) HashPassword(password []byte) ([]byte, error) {
	// TODO Make the cost configurable.
	const bcryptCost = 14
	return bcrypt.GenerateFromPassword(password, bcryptCost)
}

func (*service) ComparePasswordHash(hash, password []byte) error {
	return bcrypt.CompareHashAndPassword(hash, password)
}

var userIDContextKey = contextKey{}

type contextKey struct{}
