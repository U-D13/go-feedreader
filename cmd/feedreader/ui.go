// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/antichris/go-feedreader/internal/cmd/serve/ui"
	"github.com/antichris/go-feedreader/ui/angular"
	goahttp "goa.design/goa/v3/http"
	httpmdlwr "goa.design/goa/v3/http/middleware"
)

func serveUI(ctx context.Context, c *ui.Config) (err error) {
	l := newLogger(os.Stderr, "[ui]")

	u, err := c.URL()
	if err != nil {
		return
	}

	m := goahttp.NewMuxer()
	h, err := mountUI(l, m, c)
	if err != nil {
		return
	}
	h = addLogger(h, l)

	wf := serverWaitable(l, u, h, c.TLSConfig, shutdownTimeout, nil)
	return withIterrupts(ctx, l, wf)
}

func mountUI(
	l *log.Logger, m goahttp.Muxer, c *ui.Config,
) (h http.Handler, err error) {
	l.Printf(
		"mounting Angular UI on %q with static at %q",
		"/"+c.IndexPath, c.StaticPath,
	)
	if err = angular.New(&c.UIConfig).Mount(m); err != nil {
		return
	}

	h = m
	if c.Debug {
		h = httpmdlwr.Debug(m, os.Stdout)(h)
	}

	return
}
