// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Command feedreader manages Feed Reader server.
//
//   - [migrate] subcommand migrates Feed Reader storage.
//   - [serve] subcommand serves Feed Reader.
package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/cmd/frontend/cobra"
	"github.com/antichris/go-feedreader/internal/cmd/migrate"
	"github.com/antichris/go-feedreader/internal/cmd/serve"
	"github.com/antichris/go-feedreader/internal/cmd/serve/all"
	"github.com/antichris/go-feedreader/internal/cmd/serve/api"
	"github.com/antichris/go-feedreader/internal/cmd/serve/ui"
	goahttp "goa.design/goa/v3/http"
)

func main() {
	os.Exit(realMain(os.Args, os.Stdout, os.Stderr))
}

func realMain(args []string, wOut, wErr io.Writer) (exitCode int) {
	name := path.Base(args[0])
	rcm, rcf := cmd.NewRoot(name)

	serveCm, serveCf := serve.New(rcf)
	rcm.Subcommands = []*cmd.Command{
		migrate.New(rcf, wOut),
		serveCm,
	}
	serveCm.Subcommands = []*cmd.Command{
		all.New(rcf, serveAll),
		api.New(serveCf, serveAPI),
		ui.New(serveCf, serveUI),
	}

	cm := cobra.New(rcm, rcf)
	cm.SetOutput(os.Stdout)
	cm.SetErrOutput(os.Stderr)

	ctx := context.Background()
	// ctx, _ = context.WithTimeout(ctx, 3*time.Second)
	return cm.Exec(ctx, os.Args)
}

func serveAll(ctx context.Context, c *all.Config) (err error) {
	l := newLogger(os.Stderr, "")
	sl := newLogger(os.Stderr, "[ui]")
	al := newLogger(os.Stderr, "[api]")

	u, err := c.URL()
	if err != nil {
		return
	}

	m := goahttp.NewMuxer()
	e, cl, err := initAPI(al, c.APIConfig)
	defer cl()
	if err != nil {
		return
	}
	lmf := mountAPI(al, m, e, c.Debug)

	h, err := mountUI(sl, m, c.StaticConfig)
	if err != nil {
		return
	}
	h = addLogger(h, al)

	wf := serverWaitable(l, u, h, c.TLSConfig, shutdownTimeout, lmf)
	return withIterrupts(ctx, l, wf)
}

const shutdownTimeout = 30 * time.Second

func newLogger(w *os.File, prefix string) *log.Logger {
	prefix = strings.TrimSpace(prefix)
	if prefix != "" {
		prefix += " "
	}
	return log.New(w, prefix, log.Ltime|log.Lmicroseconds)
}

type waitableFunc = func(
	ctx context.Context, wg *sync.WaitGroup, e chan error,
)

func withIterrupts(ctx context.Context, l *log.Logger, fn waitableFunc) error {
	// Create channel used by both the signal handler and server goroutines
	// to notify the main goroutine when to stop the server.
	e := make(chan error)
	go handleInterrupts(e)

	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(ctx)

	// Start the servers and send errors (if any) to the error channel.
	fn(ctx, &wg, e)

	// Wait for signal.
	err := <-e
	l.Printf("exiting (%v)", err)

	// Send cancellation signal to the goroutines.
	cancel()

	wg.Wait()
	if err != nil && err != http.ErrServerClosed {
		return err
	}

	l.Println("exited")
	return nil
}

// handleInterrupts sets up an interrupt handler.
// This optional step configures the process so that SIGINT and SIGTERM
// signals cause the services to stop gracefully.
func handleInterrupts(e chan error) {
	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGINT, syscall.SIGTERM)
	e <- fmt.Errorf("%s", <-s)
}
