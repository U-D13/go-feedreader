// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/antichris/go-feedreader/internal/cmd/serve"
	httpmdlwr "goa.design/goa/v3/http/middleware"
	"goa.design/goa/v3/middleware"
)

func serverWaitable(
	l *log.Logger, u *url.URL, h http.Handler, tls serve.TLSConfig,
	shutdownTimeout time.Duration, logMounts func(),
) waitableFunc {
	return func(
		ctx context.Context, wg *sync.WaitGroup, e chan error,
	) {
		if logMounts != nil {
			logMounts()
		}
		// Start the servers and send errors (if any) to the error channel.
		srv := &http.Server{Addr: u.Host, Handler: h}

		// Start HTTP server in a separate goroutine.
		startAsyncServer(ctx, l, wg, srv, tls, shutdownTimeout, e)
	}
}

func startAsyncServer(
	ctx context.Context,
	l *log.Logger,
	wg *sync.WaitGroup,
	srv *http.Server,
	tls serve.TLSConfig,
	shutdownTimeout time.Duration,
	e chan error,
) {
	wg.Add(1)
	listenAndServe := func() {
		l.Printf("HTTP server listening on %q", srv.Addr)
		e <- srv.ListenAndServe()
	}
	if tls.Secure {
		listenAndServe = func() {
			l.Printf("HTTPS server listening on %q", srv.Addr)
			e <- srv.ListenAndServeTLS(tls.CertFile, tls.KeyFile)
		}
	}
	go func() {
		defer wg.Done()

		go listenAndServe()

		<-ctx.Done()

		// Shutdown gracefully with a timeout.
		l.Printf("shutting down HTTP server at %q", srv.Addr)
		ctx, cancel := context.WithTimeout(
			context.Background(), shutdownTimeout,
		)
		defer cancel()

		err := srv.Shutdown(ctx)
		if err != nil {
			l.Printf("shutting down: %v", err)
		}
	}()
}

// errorHandler returns a function that writes and logs the given error.
// The function also writes and logs the error unique ID so that it's possible
// to correlate.
func errorHandler(logger *log.Logger) func(context.Context, http.ResponseWriter, error) {
	return func(ctx context.Context, w http.ResponseWriter, err error) {
		id := ctx.Value(middleware.RequestIDKey).(string)
		_, _ = w.Write([]byte("[" + id + "] encoding: " + err.Error()))
		logger.Printf("[%s] ERROR: %s", id, err.Error())
	}
}

func addLogger(h http.Handler, l *log.Logger) http.Handler {
	// Setup goa log adapter.
	adapter := middleware.NewLogger(l)

	// Wrap the multiplexer with additional middlewares. Middlewares mounted
	// here apply to all the service endpoints.
	h = httpmdlwr.Log(adapter)(h)
	h = httpmdlwr.RequestID()(h)

	return h
}
