// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"

	feedreader "github.com/antichris/go-feedreader"
	"github.com/antichris/go-feedreader/goa"
	"github.com/antichris/go-feedreader/goa/gen/feed"
	"github.com/antichris/go-feedreader/goa/gen/health"
	feedsvr "github.com/antichris/go-feedreader/goa/gen/http/feed/server"
	healthsvr "github.com/antichris/go-feedreader/goa/gen/http/health/server"
	openAPIsvr "github.com/antichris/go-feedreader/goa/gen/http/open_api/server"
	tokensvr "github.com/antichris/go-feedreader/goa/gen/http/token/server"
	usersvr "github.com/antichris/go-feedreader/goa/gen/http/user/server"
	"github.com/antichris/go-feedreader/goa/gen/token"
	"github.com/antichris/go-feedreader/goa/gen/user"
	"github.com/antichris/go-feedreader/internal"
	"github.com/antichris/go-feedreader/internal/cmd/serve/api"
	"github.com/antichris/go-feedreader/mail"
	ts "github.com/antichris/go-feedreader/token"
	"github.com/antichris/go-feedreader/user/registration"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	goahttp "goa.design/goa/v3/http"
	httpmdlwr "goa.design/goa/v3/http/middleware"

	// TODO Put the following each behind a separate build tag.
	_ "github.com/antichris/go-feedreader/user/storage/sqlc/mysql"
	_ "github.com/antichris/go-feedreader/user/storage/sqlc/pgsql"
	_ "github.com/antichris/go-feedreader/user/storage/sqlc/sqlite"
)

func serveAPI(ctx context.Context, c *api.Config) (err error) {
	l := newLogger(os.Stderr, "[api]")

	u, err := c.Server.URL()
	if err != nil {
		return
	}

	// Build the service HTTP request multiplexer and configure it to serve
	// HTTP requests to the service endpoints.
	m := goahttp.NewMuxer()
	e, cl, err := initAPI(l, c)
	if err != nil {
		err = fmt.Errorf("init API: %w", err)
		return
	}
	defer cl()
	lmf := mountAPI(l, m, e, c.Server.Debug)
	h := addLogger(m, l)

	wf := serverWaitable(l, u, h, c.Server.TLSConfig, shutdownTimeout, lmf)
	return withIterrupts(ctx, l, wf)
}

// initAPI services
func initAPI(l *log.Logger, c *api.Config) (
	e apiEndpoints, cleanup func(), err error,
) {
	cleanup = func() {} // Ensure it's never nil.

	// TODO Move this to somewhere more appropriate.
	furl, err := url.Parse(c.FrontendURL)
	if err != nil {
		err = fmt.Errorf("frontend URL: %w", err)
		return
	}
	if len(furl.Scheme) == 0 {
		furl.Scheme = "http"
	}

	// TODO Move this to somewhere more appropriate.
	const EnvUIOrigin = "FEEDREADER_UI_ORIGIN"
	if _, ok := os.LookupEnv(EnvUIOrigin); !ok {
		if err = os.Setenv(EnvUIOrigin, (&url.URL{
			Scheme: furl.Scheme,
			Host:   furl.Host,
		}).String()); err != nil {
			err = fmt.Errorf("set env UI origin: %w", err)
			return
		}
	}

	dc := c.DBConfig
	dd := sqlc.Driver(dc.Driver)

	a, err := sqlc.GetAdapter(dd)
	if err != nil {
		err = fmt.Errorf("get adapter: %w", err)
		return
	}
	db, err := a.Open(sqlc.DSN(dc.DSN))
	if err != nil {
		err = fmt.Errorf("open sqlc database: %w", err)
		return
	}
	var (
		k = ts.Key(c.TokenKey)
		u = registration.FrontendURL(furl.String())
		m = mail.NewMailer(
			mail.EmailAddr(c.SMTP.Sender),
			mail.Host(c.SMTP.Host),
			mail.Port(c.SMTP.Port),
			mail.User(c.SMTP.User),
			mail.Pass(c.SMTP.Pass),
		)
	)
	svcs, cleanup, err := feedreader.BuildServices(l, dd, db, k, u, m)
	if err != nil {
		err = fmt.Errorf("build services: %w", err)
		return
	}

	var (
		healthSvc = feedreader.NewHealth(l)
		feedSvc   = svcs.Feed
		tokenSvc  = svcs.Token
		userSvc   = svcs.User
	)

	// Wrap the services in endpoints that can be invoked from other services
	// potentially running in different processes.

	e = apiEndpoints{
		h: health.NewEndpoints(healthSvc),
		f: feed.NewEndpoints(feedSvc),
		t: token.NewEndpoints(tokenSvc),
		u: user.NewEndpoints(userSvc),
	}
	return
}

type apiEndpoints struct {
	h *health.Endpoints
	f *feed.Endpoints
	t *token.Endpoints
	u *user.Endpoints
}

// handleHTTPServer starts configures and starts a HTTP server on the given
// URL. It shuts down the server if any error is received in the error channel.
func mountAPI(
	l *log.Logger, mux goahttp.Muxer, e apiEndpoints, debug bool,
) (logMounts func()) {
	// Provide the transport specific request decoder and response encoder.
	// The goa http package has built-in support for JSON, XML and gob.
	// Other encodings can be used by providing the corresponding functions,
	// see goa.design/implement/encoding.
	var (
		dec = goahttp.RequestDecoder
		enc = goahttp.ResponseEncoder
	)

	// Wrap the endpoints with the transport specific layers. The generated
	// server packages contains code generated from the design which maps
	// the service input and output data structures to HTTP requests and
	// responses.
	var (
		healthServer  *healthsvr.Server
		openAPIServer *openAPIsvr.Server
		feedServer    *feedsvr.Server
		tokenServer   *tokensvr.Server
		userServer    *usersvr.Server
		servers       goahttp.Servers
	)
	{
		openAPIFS := http.FS(goa.OpenAPIFS)

		eh := errorHandler(l)
		healthServer = healthsvr.New(e.h, mux, dec, enc, eh, nil)
		openAPIServer = openAPIsvr.New(nil, mux, dec, enc, eh, nil,
			http.FS(internal.RapiDocFS), openAPIFS, openAPIFS)
		feedServer = feedsvr.New(e.f, mux, dec, enc, eh, nil)
		tokenServer = tokensvr.New(e.t, mux, dec, enc, eh, nil)
		userServer = usersvr.New(e.u, mux, dec, enc, eh, nil)

		servers = goahttp.Servers{
			healthServer,
			openAPIServer,
			feedServer,
			tokenServer,
			userServer,
		}
	}
	if debug {
		servers.Use(httpmdlwr.Debug(mux, os.Stdout))
	}

	// Configure the mux.
	servers.Mount(mux)

	logMounts = func() {
		for _, m := range healthServer.Mounts {
			l.Printf("HTTP %q mounted on %s %s", m.Method, m.Verb, m.Pattern)
		}
		for _, m := range openAPIServer.Mounts {
			l.Printf("HTTP %q mounted on %s %s", m.Method, m.Verb, m.Pattern)
		}
		for _, m := range feedServer.Mounts {
			l.Printf("HTTP %q mounted on %s %s", m.Method, m.Verb, m.Pattern)
		}
		for _, m := range tokenServer.Mounts {
			l.Printf("HTTP %q mounted on %s %s", m.Method, m.Verb, m.Pattern)
		}
		for _, m := range userServer.Mounts {
			l.Printf("HTTP %q mounted on %s %s", m.Method, m.Verb, m.Pattern)
		}
	}
	return
}
