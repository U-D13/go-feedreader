// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feed

import (
	"context"
	"fmt"
	"mime"
	"strings"
	"time"

	"github.com/antichris/go-feedreader/goa/gen/feed"
	"github.com/mmcdole/gofeed"
)

type (
	// Service fetches the items of an RSS/Atom feed.
	Service interface {
		// Fetch items of an RSS/Atom feed at url.
		// The result is cached for [DefaultCacheDuration].
		Fetch(ctx context.Context, url string) (r items, err error)
	}

	// Parser parses an RSS/Atom feed.
	Parser interface {
		// Parse an RSS/Atom feed at url.
		Parse(ctx context.Context, url string) (r items, err error)
	}
	Cache interface {
		// Get value for the given key.
		// If the key is not present in the cache, ok is false.
		Get(key any) (value any, ok bool)
		// Set value for the given key with an expiration duration.
		// If duration is 0 or less, the value will never expire.
		Set(key any, value any, duration time.Duration)
	}

	// TODO Decouple this package from gofeed.
	GofeedParser interface {
		ParseURLWithContext(
			feedURL string, ctx context.Context,
		) (feed *gofeed.Feed, err error)
	}
	// TODO Decouple from .../gen/...
	items = []*feed.Item
)

const DefaultCacheDuration = 5 * time.Minute

var _ Service = (*service)(nil)

type service struct {
	c Cache
	d time.Duration
	p Parser
}

func (s *service) Fetch(ctx context.Context, url string) (r items, err error) {
	// TODO Implement periodic asynchronous fetch.
	f, ok := s.c.Get(url)
	if ok {
		r = f.(items)
		if len(r) > 0 {
			return
		}
		// If no items are cached, something must've gone wrong,
		// we'd better re-fetch.
	}
	r, err = s.p.Parse(ctx, url)
	if err != nil {
		return
	}
	s.c.Set(url, r, s.d)
	return
}

var _ Parser = (*parser)(nil)

type parser struct{ p GofeedParser }

func (s *parser) Parse(ctx context.Context, url string) (r items, err error) {
	f, err := s.p.ParseURLWithContext(url, ctx)
	if err != nil {
		return
	}
	r = make(items, len(f.Items))
	for i, v := range f.Items {
		r[i] = mapItem(v)
	}
	return
}

// mapItem from the gofeed structure to the one our API uses.
func mapItem(item *gofeed.Item) *feed.Item {
	r := &feed.Item{
		Title:       item.Title,
		Description: item.Description,
		Link:        feed.Link(item.Link),
	}
	pub := item.PublishedParsed
	if pub != nil {
		v := itemDate(pub)
		r.PubDate = &v
	}
	if upd := item.UpdatedParsed; upd != nil && upd != pub {
		v := itemDate(upd)
		r.Updated = &v
	}
	if i := imageLink(item); i != "" {
		r.Image = &i
	}
	return r
}

func itemDate(t *time.Time) feed.ItemDate {
	return feed.ItemDate(t.Format(time.RFC3339))
}

func imageLink(item *gofeed.Item) feed.Link {
	if item.Image != nil {
		return feed.Link(item.Image.URL)
	}
	if u := enclosureImageURL(item); u != "" {
		return feed.Link(u)
	}
	return feed.Link(mediaContentImageURL(item))
}

// enclosureImageURL returns the URL value of the first enclosure whose
// type is one of the supported imageMIMETypes.
func enclosureImageURL(item *gofeed.Item) string {
	for _, v := range item.Enclosures {
		for _, t := range imageMIMETypes {
			if v.Type == t {
				return v.URL
			}
		}
	}
	return ""
}

// imageMIMETypes in the order of preference.
var imageMIMETypes = []string{
	"image/svg+xml",
	"image/avif",
	"image/webp",
	"image/jpeg",
	"image/png",
	"image/gif",
}

// mediaContentImageURL returns the first URL value of a `media:content`
// node that ends with one of the supported imageMIMEExtensions.
func mediaContentImageURL(item *gofeed.Item) string {
	m, ok := item.Extensions["media"]
	if !ok {
		return ""
	}
	c, ok := m["content"]
	if !ok {
		return ""
	}
	for _, v := range c {
		u, ok := v.Attrs["url"]
		if !ok {
			continue
		}
		for _, x := range imageMIMEExtensions {
			if strings.HasSuffix(u, x) {
				return u
			}
		}
	}
	return ""
}

// imageMIMEExtensions in the order of preference.
var imageMIMEExtensions = mimeExtensions(imageMIMETypes)

// mimeExtensions returns file extensions associated with the
// given MIME types.
func mimeExtensions(types []string) (r []string) {
	// We can be sure the result will be at least as long as the input.
	s := make([]string, 0, len(types))
	for _, t := range types {
		xs, err := mime.ExtensionsByType(t)
		if err != nil {
			panic(fmt.Errorf("%w: %q", err, t))
		}
		s = append(s, xs...)
	}
	l := len(s)
	if l == 0 {
		return
	}
	r = make([]string, l)
	copy(r, s)
	return
}
