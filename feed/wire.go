// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//go:build wireinject

//go:generate go run github.com/google/wire/cmd/wire gen -header_file=../internal/_resources/license-header.go
//go:generate go fix -fix buildtag .
//go:generate go run mvdan.cc/gofumpt -w wire_gen.go

package feed

import (
	"time"

	"github.com/akyoto/cache"
	"github.com/google/wire"
	"github.com/mmcdole/gofeed"
)

// New feed [Service] instance.
func New() (Service, func()) {
	panic(wire.Build(
		serviceSet,
		provideCache,
		parserSet,
		provideGofeedParser,
	))
}

func injectParser(
	GofeedParser,
) *parser {
	panic(wire.Build(
		parserSet,
	))
}

var (
	serviceSet = wire.NewSet(
		wire.Bind(new(Service), new(*service)),
		wire.Struct(new(service), "*"),
		wire.Value(DefaultCacheDuration), // TODO Consider making this configurable.
	)
	parserSet = wire.NewSet(
		wire.Bind(new(Parser), new(*parser)),
		wire.Struct(new(parser), "*"),
	)
)

func provideGofeedParser() GofeedParser {
	return gofeed.NewParser()
}

func provideCache() (Cache, func()) {
	c := cache.New(time.Minute)
	return c, func() {
		c.Close()
	}
}
