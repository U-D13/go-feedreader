// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feed_test

import (
	"testing"
	"time"

	. "github.com/antichris/go-feedreader/feed"
	"github.com/antichris/go-feedreader/feed/mocks"
	"github.com/antichris/go-feedreader/goa/gen/feed"
	"github.com/golang/mock/gomock"
	"github.com/mmcdole/gofeed"
	ext "github.com/mmcdole/gofeed/extensions"
	"github.com/stretchr/testify/require"
)

func Test_service_Fetch(t *testing.T) {
	t.Parallel()
	const d = time.Second
	type feedItems = []*feed.Item
	items := feedItems{{Title: "foo"}}
	for n, tt := range map[string]struct {
		cItems feedItems
		cOk    bool
		pItems feedItems
		pErr   error
		want   feedItems
		wErr   error
	}{"error": {
		pErr: errSnap,
		wErr: errSnap,
	}, "empty hit": {
		cItems: items,
		cOk:    true,
		want:   items,
	}, "empty cache hit": {
		cOk:    true,
		pItems: items,
		want:   items,
	}, "cache miss": {
		cOk:    false,
		pItems: items,
		want:   items,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			ctl := gomock.NewController(t)
			c := mocks.NewMockCache(ctl)
			p := mocks.NewMockParser(ctl)
			svc, cl := newService(c, d, p)
			defer cl()

			cx := c.EXPECT()
			cx.Get(url).
				Return(tt.cItems, tt.cOk)
			cx.Set(url, tt.pItems, d).
				AnyTimes()
			p.EXPECT().
				Parse(ctx, url).
				Return(tt.pItems, tt.pErr).
				AnyTimes()

			got, err := svc.Fetch(ctx, url)

			r := require.New(t)
			if tt.wErr != nil {
				r.ErrorIs(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_parser_Parse(t *testing.T) {
	t.Parallel()
	const (
		title = "<title>"
		descr = "<description>"
		link  = "<link>"
	)
	type items = []*feed.Item
	var (
		utc7       = time.FixedZone("", 7*3600)
		pubDate    = time.Date(2001, 2, 3, 4, 5, 6, 0, utc7)
		updated    = time.Date(2008, 9, 10, 11, 12, 13, 0, time.UTC)
		pubdateStr = feed.ItemDate("2001-02-03T04:05:06+07:00")
		updatedStr = feed.ItemDate("2008-09-10T11:12:13Z")

		imageLink = feed.Link("<image>.png")
		imageFeed = items{{
			Image: &imageLink,
		}}
	)
	for n, tt := range map[string]struct {
		pFeed gofeed.Feed
		pErr  error
		wFeed items
		wErr  error
	}{"error": {
		pErr: errSnap,
		wErr: errSnap,
	}, "title": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Title: title,
		}}},
		wFeed: items{{
			Title: title,
		}},
	}, "description": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Description: descr,
		}}},
		wFeed: items{{
			Description: descr,
		}},
	}, "link": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Link: link,
		}}},
		wFeed: items{{
			Link: feed.Link(link),
		}},
	}, "pubDate": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			PublishedParsed: &pubDate,
		}}},
		wFeed: items{{
			PubDate: &pubdateStr,
		}},
	}, "updated": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			UpdatedParsed: &updated,
		}}},
		wFeed: items{{
			Updated: &updatedStr,
		}},
	}, "image": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Image: &gofeed.Image{URL: string(imageLink)},
		}}},
		wFeed: imageFeed,
	}, "enclosure": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Enclosures: []*gofeed.Enclosure{{
				URL:  string(imageLink),
				Type: imageMIMETypes[0],
			}},
		}}},
		wFeed: imageFeed,
	}, "media without content": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Extensions: ext.Extensions{"media": {}},
		}}},
		wFeed: items{{}},
	}, "media:content no URL": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Extensions: ext.Extensions{"media": {
				"content": []ext.Extension{{
					Attrs: map[string]string{
						"foo": "bar",
					},
				}},
			}},
		}}},
		wFeed: items{{}},
	}, "media:content not image": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Extensions: ext.Extensions{"media": {
				"content": []ext.Extension{{
					Attrs: map[string]string{
						"url": "foo",
					},
				}},
			}},
		}}},
		wFeed: items{{}},
	}, "media:content": {
		pFeed: gofeed.Feed{Items: []*gofeed.Item{{
			Extensions: ext.Extensions{"media": {
				"content": []ext.Extension{{
					Attrs: map[string]string{
						"url": string(imageLink),
					},
				}},
			}},
		}}},
		wFeed: imageFeed,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			p := mocks.NewMockGofeedParser(gomock.NewController(t))
			svc := injectParser(p)

			p.EXPECT().
				ParseURLWithContext(url, ctx).
				Return(&tt.pFeed, tt.pErr)

			gotFeed, gotErr := svc.Parse(ctx, url)

			r := require.New(t)
			r.Equal(gotErr, tt.wErr)
			r.Equal(tt.wFeed, gotFeed)
		})
	}
}

func Test_mimeExtensionsForTypes(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		types  []string
		want   []string
		wPanic string
	}{"bad type": {
		types:  []string{"/"},
		wPanic: `mime: no media type: "/"`,
	}, "nominal": {
		types: []string{"image/png"},
		want:  []string{".png"},
	}, "empty": {}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got []string
			run := func() { got = mimeExtensions(tt.types) }

			r := require.New(t)
			if tt.wPanic != "" {
				r.PanicsWithError(tt.wPanic, run)
				return
			}
			r.NotPanics(run)
			r.Equal(tt.want, got)
		})
	}
}

const url = "<url>"

func newService(c Cache, d time.Duration, p Parser) (*service, func()) {
	s, cleanup := New()
	r := s.(*service).
		XsetCache(c).
		XsetDuration(d).
		XsetParser(p)
	return r, cleanup
}
