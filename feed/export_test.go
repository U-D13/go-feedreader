// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feed

import "time"

var (
	XinjectParser = injectParser

	XimageMIMETypes = imageMIMETypes
	XmimeExtensions = mimeExtensions
)

type (
	Xservice = service
)

func (x *service) XsetCache(v Cache) *service            { x.c = v; return x }
func (x *service) XsetDuration(v time.Duration) *service { x.d = v; return x }
func (x *service) XsetParser(v Parser) *service          { x.p = v; return x }
