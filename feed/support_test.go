// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feed_test

import (
	"context"

	. "github.com/antichris/go-feedreader/feed"
	. "github.com/antichris/go-feedreader/terror"
)

var (
	injectParser = XinjectParser

	imageMIMETypes = XimageMIMETypes
	mimeExtensions = XmimeExtensions
)

type (
	service = Xservice
)

const errSnap = Error("snap")

var ctx = context.Background()
