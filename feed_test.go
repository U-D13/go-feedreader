// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader_test

import (
	"context"
	"io"
	"log"
	"strings"
	"testing"
	"testing/fstest"

	. "github.com/antichris/go-feedreader"
	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/feed/mocks"
	gfeed "github.com/antichris/go-feedreader/goa/gen/feed"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_feedsrvc_APIKeyAuth(t *testing.T) {
	t.Parallel()
	requireAuthServiceAPIKeyAuthCall(t, func(a auth.Service) Auther {
		s, c := newFeed(nil, a)
		c()
		return s
	})
}

func Test_feedsrvc_Index(t *testing.T) {
	t.Parallel()
	type (
		it = gfeed.Item
		is = []*it
	)
	var (
		pubdateStr = gfeed.ItemDate("2001-02-03T04:05:06+07:00")
		updatedStr = gfeed.ItemDate("2008-09-10T11:12:13Z")
		imageLink  = gfeed.Link("<image>.png")

		items = is{&it{
			Title:       "<title>",
			Description: "<description>",
			Link:        "<link>",
			PubDate:     &pubdateStr,
			Updated:     &updatedStr,
			Image:       &imageLink,
		}}
	)
	for n, tt := range map[string]struct {
		svcFeed  is
		svcErr   error
		wantFeed is
		wantErr  error
	}{"error": {
		svcErr:  errSnap,
		wantErr: feedInternalErr(errSnap),
	}, "nominal": {
		svcFeed:  items,
		wantFeed: items,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			w := io.Discard
			// TODO Assert on logger output.
			// w := &strings.Builder{}
			fs := mocks.NewMockService(gomock.NewController(t))
			svc, c := newFeed(newLogger(w), nil)
			defer c()
			svc.XsetFeedService(fs).
				XsetGetURL(func(ctx context.Context) string { return url })

			fs.EXPECT().
				Fetch(ctx, url).
				Return(tt.svcFeed, tt.svcErr)

			gotFeed, gotErr := svc.Index(ctx, nil)

			r := require.New(t)
			r.Equal(gotErr, tt.wantErr)
			r.Equal(tt.wantFeed, gotFeed)
		})
	}
}

func Test_getFeedURL(t *testing.T) {
	t.Parallel()
	const file = "file"
	t.Run("no file", func(t *testing.T) {
		t.Parallel()
		want := feedURL
		got := getFeedURL(fstest.MapFS{}, file)
		require.Equal(t, want, got)
	})

	const (
		ws   = "\t "
		lCmt = ws + "#comment"
		lURL = ws + url
	)
	type lines = []string
	for n, tt := range map[string]struct {
		want string
		lines
	}{
		"only whitespace":  {feedURL, lines{ws, ws}},
		"only comment":     {feedURL, lines{lCmt}},
		"after whitespace": {url, lines{ws, lURL}},
		"after comment":    {url, lines{lCmt, lURL}},
		"immediate":        {url, lines{lURL}},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			fsys := fstest.MapFS{file: &fstest.MapFile{
				Data: []byte(strings.Join(tt.lines, "\n")),
			}}
			got := getFeedURL(fsys, file)
			require.Equal(t, tt.want, got)
		})
	}
}

func Test_feedUnauthErr(t *testing.T) {
	t.Parallel()
	want := &gfeed.UnauthorizedError{Message: errSnap.Error()}
	got := feedUnauthErr(errSnap)
	require.Equal(t, want, got)
}

func Test_feedInternalErr(t *testing.T) {
	t.Parallel()
	want := &gfeed.InternalError{Message: errSnap.Error()}
	got := feedInternalErr(errSnap)
	require.Equal(t, want, got)
}

const url = "<url>"

func newFeed(l *log.Logger, a auth.Service) (*feedsrvc, func()) {
	s, c := NewFeed(l, a)
	return s.(*feedsrvc), c
}
