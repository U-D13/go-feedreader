// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader_test

import (
	"context"
	"io"
	"log"
	"testing"
	"time"

	. "github.com/antichris/go-feedreader"
	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/auth/mocks"
	"github.com/antichris/go-feedreader/goa/gen/token"
	tokenSvc "github.com/antichris/go-feedreader/token"
	tokenMocks "github.com/antichris/go-feedreader/token/mocks"
	"github.com/antichris/go-feedreader/user"
	uStoreMocks "github.com/antichris/go-feedreader/user/storage"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_tokensrvc_APIKeyAuth(t *testing.T) {
	t.Parallel()
	requireAuthServiceAPIKeyAuthCall(t, func(a auth.Service) Auther {
		return newTokensrvc(nil, a, nil, nil)
	})
}

func Test_tokensrvc_Create(t *testing.T) {
	t.Parallel()
	const delay = 30 * time.Millisecond
	var (
		u = user.User{ID: userID}
		p = &token.Credentials{
			Password: "<password>",
			Email:    email,
		}
		res = tk

		want = res
	)
	for n, tt := range map[string]struct {
		lookup error
		passH  error
		delay  time.Duration
		token  error
		wErr   error
	}{"lookup error": {
		lookup: errSnap,
		wErr:   errSnap,
	}, "not found": {
		lookup: user.ErrNotFound,
		delay:  delay,
		wErr:   errInvalidCreds,
	}, "bad pass": {
		passH: auth.ErrMismatchedHashAndPassword,
		delay: delay,
		wErr:  errInvalidCreds,
	}, "other hash err": {
		passH: errSnap,
		delay: delay,
		wErr:  errInvalidCreds,
	}, "service error": {
		token: errSnap,
		wErr:  errSnap,
	}, "nominal": {
		wErr: nil,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			svc, ts, us := newMockedTokensrvc(t)
			as := mocks.NewMockService(gomock.NewController(t))
			svc.XsetAuth(as).
				XsetDelay(tt.delay)
			us.EXPECT().
				GetByEmailAndStatus(ctx, p.Email, user.Active).
				Return(u, tt.lookup)
			as.EXPECT().
				ComparePasswordHash([]byte(u.PasswordHash), []byte(p.Password)).
				Return(tt.passH).
				AnyTimes()
			ts.EXPECT().
				CreateFor(uint32(userID), time.Duration(0)).
				Return(res, tt.token).
				AnyTimes()

			start := time.Now()
			got, gotErr := svc.Create(ctx, p)
			end := time.Now()

			r := require.New(t)
			r.WithinDuration(start.Add(tt.delay), end, 10*time.Millisecond)
			if tt.wErr != nil {
				r.ErrorIs(gotErr, tt.wErr)
				return
			}
			r.NoError(gotErr)
			r.Equal(want, got)
		})
	}
}

func Test_tokensrvc_Reissue(t *testing.T) {
	t.Parallel()
	var (
		uidCtx = ctxWithUserID(ctx)
		res    = token.OurToken("<token>")

		want = res
	)
	for n, tt := range map[string]struct {
		ctx     context.Context
		tsErr   error
		wantErr error
	}{"no user ID": {
		ctx:     ctx,
		wantErr: tokenInternalErr(auth.ErrNoContextUserID),
	}, "service error": {
		ctx:     uidCtx,
		tsErr:   errSnap,
		wantErr: errSnap,
	}, "nominal": {
		ctx:     uidCtx,
		wantErr: nil,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			svc, ts, _ := newMockedTokensrvc(t)
			ts.EXPECT().
				CreateFor(uint32(userID), time.Duration(0)).
				Return(res, tt.tsErr).
				AnyTimes()

			got, gotErr := svc.Reissue(tt.ctx, nil)

			r := require.New(t)
			if tt.wantErr != nil {
				r.Equal(tt.wantErr, gotErr)
				return
			}
			r.NoError(gotErr)
			r.Equal(want, got)
		})
	}
}

func Test_tokensrvc_Describe(t *testing.T) {
	t.Parallel()
	var (
		p   = &token.DescribePayload{Auth: string(tk)}
		res = token.TokenData{UID: userID}

		want = &res

		formatErr    = tokenSvc.ErrUnsupportedFormat
		signatureErr = tokenSvc.ErrInvalidSignature
	)
	for n, tt := range map[string]struct {
		p       *token.DescribePayload
		tsErr   error
		wantErr error
	}{"bad format": {
		tsErr:   formatErr,
		wantErr: tokenBadReqErr(formatErr),
	}, "bad signature": {
		tsErr:   signatureErr,
		wantErr: tokenBadReqErr(signatureErr),
	}, "other error": {
		tsErr:   errSnap,
		wantErr: errSnap,
	}, "nominal": {
		wantErr: nil,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			svc, ts, _ := newMockedTokensrvc(t)
			ts.EXPECT().
				Decode(tk).
				Return(res, tt.tsErr).
				AnyTimes()

			got, gotErr := svc.Describe(ctx, p)

			r := require.New(t)
			if tt.wantErr != nil {
				r.Equal(tt.wantErr, gotErr)
				return
			}
			r.NoError(gotErr)
			r.Equal(want, got)
		})
	}
}

func Test_tokenInternalErr(t *testing.T) {
	t.Parallel()
	want := &token.InternalError{Message: errSnap.Error()}
	got := tokenInternalErr(errSnap)
	require.Equal(t, want, got)
}

func Test_tokenBadReqErr(t *testing.T) {
	t.Parallel()
	want := &token.BadRequestError{Message: errSnap.Error()}
	got := tokenBadReqErr(errSnap)
	require.Equal(t, want, got)
}

func Test_tokenUnauthErr(t *testing.T) {
	t.Parallel()
	want := &token.UnauthorizedError{Message: errSnap.Error()}
	got := tokenUnauthErr(errSnap)
	require.Equal(t, want, got)
}

func newMockedTokensrvc(t *testing.T) (
	*tokensrvc, *tokenMocks.MockService, *uStoreMocks.MockStorage,
) {
	w := io.Discard
	// TODO Assert on logger output.
	// w := &strings.Builder{}
	ts := mockTokenService(t)
	us := mockUserStorage(t)
	svc := newTokensrvc(newLogger(w), nil, ts, us)
	return svc, ts, us
}

func mockUserStorage(t *testing.T) *uStoreMocks.MockStorage {
	return uStoreMocks.NewMockStorage(gomock.NewController(t))
}

func mockTokenService(t *testing.T) *tokenMocks.MockService {
	return tokenMocks.NewMockService(gomock.NewController(t))
}

func newTokensrvc(
	l *log.Logger, a auth.Service, ts tokenSvc.Service, us user.Storage,
) *tokensrvc {
	return NewToken(l, a, ts, us).(*tokensrvc)
}
