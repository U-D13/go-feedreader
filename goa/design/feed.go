// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package design

//lint:ignore ST1001 Using the DSL without this would be unwieldy.
import . "goa.design/goa/v3/dsl"

var _ = Service("Feed", func() {
	Description("Serves feed contents.")

	Security(rJWTAuth)

	errorInternal()
	errorUnauthorized()

	HTTP(func() {
		Path("/feed")
	})
	Method("Index", func() {
		Description("Serves feed contents.")

		Payload(ourTokenField)
		Result(ArrayOf(item))

		HTTP(func() {
			GET("/")

			responseOK(func() {
				Description("Feed contents")
			})
			responseInternalErr()
			responseUnauthorized()
		})
	})
})

var item = Type("Item", func() {
	Description("An item in an RSS or Atom feed.")

	requiredField(fTitle, nTitle, String)
	requiredField(fDescription, nDescription, String)
	requiredField(fLink, nLink, link)

	Field(fPubDate, nPubDate, itemDate, func() {
		Description("Publication date.")
	})
	Field(fUpdated, nUpdated, itemDate, func() {
		Description("The last time the entry was modified in a significant way.")
	})
	Field(fImage, nImage, link, func() {
		Description("Image URL.")
	})
})

var itemDate = Type("ItemDate", String, func() {
	Description("ItemDate is an RFC3339 date time string.")
	Format(FormatDateTime)
})

var link = Type("Link", String, func() {
	Format(FormatURI)
})

const (
	nTitle       = "title"
	nDescription = "description"
	nLink        = "link"
	nPubDate     = "pubDate"
	nUpdated     = "updated"
	nImage       = "image"
)

const (
	_ uint8 = iota
	fTitle
	fDescription
	fLink
	fPubDate
	fUpdated
	fImage
)
