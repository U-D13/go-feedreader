// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// This file defines variables and helper functions for implementing
// Goa security.

package design

//lint:ignore ST1001 Using the DSL without this would be unwieldy.
import . "goa.design/goa/v3/dsl"

// ourTokenField adds a required APIKeyField for our token.
func ourTokenField() {
	APIKeyField(fAPIKey, sAPIKey, nToken, func() {
		Description("Bearer token.")
		Pattern("^Bearer " + rxToken + "$")
		Example("Bearer " + exampleToken)
	})
	Required(nToken)
}

var rJWTAuth = APIKeySecurity(sAPIKey, func() {
	Description("Bearer token security using reduced JWT.")
})

const (
	nToken  = "auth"
	sAPIKey = "rJWT" // API key security schema name.
	fAPIKey = 1
)
