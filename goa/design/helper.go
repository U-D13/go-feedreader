// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// This file defines helper functions useful across the entire design.

package design

//lint:ignore ST1001 Using the DSL without this would be unwieldy.
import . "goa.design/goa/v3/dsl"

func responseOK(args ...any)        { Response(StatusOK, args...) }
func responseCreated(args ...any)   { Response(StatusCreated, args...) }
func responseNoContent(args ...any) { Response(StatusNoContent, args...) }

// requiredField does precisely the same what Field does, but, in
// addition, also immediately defines the field as Required.
func requiredField(tag any, name string, args ...any) {
	Field(tag, name, args...)
	Required(name)
}

// with prepends the given v to the rest of args.
//
// Example:
//
//	func requiredStringField(tag any, name string, args ...any) {
//		Field(tag, name, with(String, args)...)
//		Required(name)
//	}
func with(v any, args []any) []any {
	return append([]any{v}, args...)
}
