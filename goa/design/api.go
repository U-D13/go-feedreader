// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// TODO Add gRPC support.

// Package design uses Goa DSL to specify the API design of Feed Reader.
package design

//go:generate go run goa.design/goa/v3/cmd/goa gen github.com/antichris/go-feedreader/goa/design --output=..
//go:generate go run mvdan.cc/gofumpt -w ../gen

import (
	//lint:ignore ST1001 Using the DSL without this would be unwieldy.
	. "goa.design/goa/v3/dsl"
	cors "goa.design/plugins/v3/cors/dsl"
)

/*
	API                 Service          Type            ResultType
	├── Title           ├── Description  ├── Extend      ├── TypeName
	├── Description     ├── Docs         ├── Reference   ├── ContentType
	├── Version         ├── Security     ├── ConvertTo   ├── Extend
	├── Docs            ├── Error        ├── CreateFrom  ├── Reference
	├── License         ├── GRPC         ├── Attribute   ├── ConvertTo
	├── TermsOfService  ├── HTTP         ├── Field       ├── CreateFrom
	├── Contact         ├── Method       └── Required    ├── Attributes
	├── Server          │   ├── Payload                  └── View
	└── HTTP            │   ├── Result
						│   ├── Error
						│   ├── GRPC
						│   └── HTTP
						└── Files
*/

var _ = API("feed-reader", func() {
	Title("Feed Reader API")
	Description(`# RSS/Atom feed reader API.

To use this API, you must first [create an authorization token] for a registered
user. You can register a new user account by posting a [user creation request],
following that up by [account activation].

[create an authorization token]: #post-/api/token
[user creation request]: #post-/api/user
[account activation]: #put-/api/user/activation
`)
	Version("pre-0.1")
	License(func() {
		Name("Mozilla Public License, v2.0")
		URL("https://www.mozilla.org/en-US/MPL/2.0/")
	})

	HTTP(func() {
		Path("/api")

		supportedMIMEs := []string{"application/json", "application/xml"}
		Consumes(supportedMIMEs...)
		Produces(supportedMIMEs...)
	})

	Server("feedreader", func() {
		Host("dev", func() {
			Description("Development host.")
			URI("http://feedreader.localhost:8080")
		})
		Host("devs", func() {
			Description("Secure development host.")
			URI("https://feedreader.localhost:4430")
		})
		Host("prod", func() {
			Description(`"Production" host.`)
			URI("https://feedreader.localhost")
		})
		// Host("production", func() {
		// 	Description("Production hosts.")
		// 	// URIs can be parameterized using {param} notation.
		// 	URI("https://{version}.goa.design/calc")
		// 	URI("grpcs://{version}.goa.design")

		// 	// Variable describes a URI variable.
		// 	Variable("version", String, "API version", func() {
		// 		// URL parameters must have a default value and/or an
		// 		// enum validation.
		// 		Default("v1")
		// 	})
		// })
	})
	Server("feedreader-alt", func() {
		Host("dev-alt", func() {
			Description("Development host.")
			URI("http://feedreader.localhost:81")
		})
	})

	dslCORS := func() {
		cors.Methods("DELETE", "GET", "HEAD", "PUT", "PATCH")
		cors.Headers("Content-Type", "Authorization")
		cors.MaxAge(600)
	}

	// Frontend origin of API resource requests.
	cors.Origin("$FEEDREADER_UI_ORIGIN", dslCORS)

	// The Swagger Editor.
	cors.Origin("*editor.swagger.io", dslCORS)
	cors.Origin("*editor-next.swagger.io", dslCORS)

	// The `localhost` (with sub-hosts) and loopback IPs.
	cors.Origin(rxOriginLocalSubHost, dslCORS)
	cors.Origin(rxOriginLoopbackIPv4, dslCORS)
	cors.Origin(rxOriginLoopbackIPv6, dslCORS)
})

var _ = Service("OpenAPI", func() {
	openapiSummaryMeta("OpenAPI description")
	Description("Serves the OpenAPI description of the Feed Reader API.")

	Files("/", "rapidoc.html", func() {
		openapiSummaryMeta("OpenAPI documentation")
		Description("Serves human-friendly documentation of the OpenAPI description.")
	})
	Files("/openapi.json", "openapi3.json", func() {
		openapiSummaryMeta("OpenAPI JSON")
		Description("Serves the OpenAPI description as a JSON document.")
		// ContentType("application/vnd.oai.openapi+json;version=3.0")
	})
	Files("/openapi.yaml", "openapi3.yaml", func() {
		openapiSummaryMeta("OpenAPI YAML")
		Description("Serves the OpenAPI description as a YAML document.")
		// ContentType("application/vnd.oai.openapi;version=3.0")
	})
})

var _ = Service("Health", func() {
	Description("The responsivity and availability of this service reflect" +
		" those of the entire API.")

	HTTP(func() {
		Path("/health")
	})
	Method("Show", func() {
		Description("Responds with a success code (HTTP 204 No Content, GRPC" +
			" 0 OK) as long as the server is up and running.")

		HTTP(func() {
			GET("/")
			responseNoContent(func() {
				Description("Server is up and running.")
			})
		})
	})
})

const (
	rxOriginLocalSubHost = rxStart + rxHTTPOptS + rxLocalSubHost + rxEnd
	rxOriginLoopbackIPv4 = rxStart + rxHTTPOptS + rxLoopbackIPv4 + rxEnd
	rxOriginLoopbackIPv6 = rxStart + rxHTTPOptS + rxLoopbackIPv6 + rxEnd

	rxHTTPOptS     = `https?:\/\/`
	rxLocalSubHost = `([0-9a-z.-]+\.)?localhost` + rxAnyPortNum
	rxLoopbackIPv4 = `127\.(?:(?:1?[0-9]{1,2}|` + rx200to254 + `)\.){2}` +
		`(?:[1-9][0-9]?|1[0-9]{2}|` + rx200to254 + `)` + rxAnyPortNum
	rxLoopbackIPv6 = `\[::1\]` + rxAnyPortNum

	rx200to254   = `2[0-4][0-9]|25[0-4]`
	rxAnyPortNum = `(?::(?:[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|` +
		`65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5]))?`

	rxStart, rxEnd = `/^`, `$/`
)

func openapiSummaryMeta(summary string) { Meta("swagger:summary", summary) }
