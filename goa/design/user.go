// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package design

import (
	//lint:ignore ST1001 Using the DSL without this would be unwieldy.
	"github.com/antichris/go-feedreader/user"
	. "goa.design/goa/v3/dsl"
)

var _ = Service("User", func() {
	Description("Manages user records.")

	Security(rJWTAuth)

	errorNotFound()
	errorBadRequest()
	errorUnauthorized()
	errorInternal()

	HTTP(func() {
		Path("/user")
	})
	Method("IsEmailAvailableForNew", func() {
		Description("Check whether an email address can be used to create" +
			" a new user.")

		NoSecurity()

		Payload(String, "Email address to check.", dslEmail)
		Result(Empty)

		errorConflict()

		HTTP(func() {
			GET("/email-available/{email}")

			responseNoContent(func() {
				Description("Email address is available for registration.")
			})
			responseConflict(func() {
				Description("Email address cannot be used for registration.")
			})
			responseInternalErr()
		})
	})
	Method("Create", func() {
		Description("Create a new user.")

		NoSecurity()

		Payload(createUserPayload)
		Result(Empty)

		HTTP(func() {
			POST("/")

			responseCreated(func() {
				Description("User created.")
			})
			responseBadRequest()
		})
	})
	Method("Activate", func() {
		Description("Activate user account using an email confirmation token.")

		NoSecurity()

		Payload(func() {
			Field(fEmailToken, "token", String, func() {
				Description("Email confirmation/account activation token.")
				Example("JW_zoXorHb9uWCotqWbcD1wmj2r3jqud")
			})
		})

		HTTP(func() {
			PUT("/activation")

			responseNoContent(func() {
				Description("User account activated.")
			})
			responseBadRequest()
			responseInternalErr()
		})
	})
	Method("Introspect", func() {
		Description("Show the record of the requesting user.")

		Payload(ourTokenField)
		Result(showUserResponse)

		HTTP(func() {
			GET("/")

			responseOK()
			responseUnauthorized()
			responseInternalErr()
		})
	})
	Method("Show", func() {
		Description("Show the record of a given user.")

		Payload(func() {
			ourTokenField()
			requiredUserIDField()
		})
		Result(showUserResponse)

		errorForbidden()
		errorNotFound()

		HTTP(func() {
			GET("/{" + nUserID + "}")

			responseOK()
			responseBadRequest()
			responseUnauthorized()
			responseForbidden()
			responseNotFound()
			responseInternalErr()
		})
	})
})

var createUserPayload = Type("CreateUser", func() {
	Extend(baseUser)

	requiredUserPasswordField()
})

var showUserResponse = Type("ShowUser", func() {
	Extend(baseUser)
	CreateFrom(user.User{})

	requiredUserIDField()
	requiredField(fUserStatus, nUserStatus, UInt32, func() {
		Description("User status.")
		Enum(0, 1, 2)
		unconvertableFieldMeta()
	})
	requiredField(fCreatedAt, nCreatedAt, itemDate, func() {
		Description("Creation date and time.")
		unconvertableFieldMeta()
	})
	requiredField(fUpdatedAt, nUpdatedAt, itemDate, func() {
		Description("Last update date and time.")
		unconvertableFieldMeta()
	})
})

var baseUser = Type("BaseUser", func() {
	Description("User of the system.")

	requiredField(fFirstName, nFirstName, String, func() {
		Description("First name.")
		MaxLength(255)
		Example("Jane")
	})
	requiredField(fLastName, nLastName, String, func() {
		Description("Last name.")
		MaxLength(255)
		Example("Doe")
	})
	requiredUserEmailField()
})

func requiredUserIDField() {
	requiredField(fUserID, nUserID, Int32, "ID number.")
}

func requiredUserEmailField() {
	requiredField(fEmail, nEmail, String, dslEmail)
}

func requiredUserPasswordField() {
	requiredField(fPassword, nPassword, String, func() {
		Description("Password.")

		MinLength(12)
		Example(func() {
			Description("A weak example (36297+ times at haveibeenpwned.com)")
			Value("password1234")
		})
	})
}

func unconvertableFieldMeta() {
	Meta("struct.field.external", "-")
}

var dslEmail = func() {
	Description("Email address.")

	Format(FormatEmail)
	MaxLength(255)
	Example("user@example.com")
}

const (
	nUserID     = "id"
	nFirstName  = "firstName"
	nLastName   = "lastName"
	nEmail      = "email"
	nPassword   = "password"
	nUserStatus = "status"
	nCreatedAt  = "createdAt"
	nUpdatedAt  = "updatedAt"
)

const (
	_ uint8 = iota + fAPIKey
	fUserID
	fFirstName
	fLastName
	fEmail
	fPassword
	fUserStatus
	fEmailToken
	fCreatedAt
	fUpdatedAt
)
