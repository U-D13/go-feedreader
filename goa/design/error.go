// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// This file defines errors and error responses.

package design

import (
	//lint:ignore ST1001 Using the DSL without this would be unwieldy.
	. "goa.design/goa/v3/dsl"
	"goa.design/goa/v3/expr"
)

func errorBadRequest()   { Error(nBadRequest, badRequestError) }
func errorUnauthorized() { Error(nUnauthorized, unauthorizedError) }
func errorForbidden()    { Error(nForbidden, forbiddenError) }
func errorNotFound()     { Error(nNotFound, notFoundError) }
func errorConflict()     { Error(nConflictError, conflictErrorType, "Conflict") }
func errorInternal() {
	Error(nInternalError, internalError, func() {
		Fault()
	})
}

func responseBadRequest(args ...any) {
	Response(nBadRequest, with(StatusBadRequest, args)...)
}

func responseUnauthorized(args ...any) {
	Response(nUnauthorized, with(StatusUnauthorized, args)...)
}

func responseForbidden(args ...any) {
	Response(nForbidden, with(StatusForbidden, args)...)
}

func responseNotFound(args ...any) {
	Response(nNotFound, with(StatusNotFound, args)...)
}

func responseConflict(args ...any) {
	Response(nConflictError, with(StatusConflict, args)...)
}

func responseInternalErr(args ...any) {
	Response(nInternalError, with(StatusInternalServerError, args)...)
}

var (
	badRequestError = errorType(nBadRequest+"Error",
		"Bad request.",
		errorFields{
			Name:    "Bad Request",
			Message: "Invalid request body",
			Status:  StatusBadRequest,
			Type:    `yii\web\BadRequestHttpException`,
		},
	)
	unauthorizedError = errorType(nUnauthorized+"Error",
		"User authentication required.",
		errorFields{
			Name:    "Unauthorized",
			Message: "Your request was made with invalid credentials",
			Status:  StatusUnauthorized,
			Type:    `yii\web\UnauthorizedHttpException`,
		},
	)
	forbiddenError = errorType(nForbidden+"Error",
		"You do not have access rights to the resource.",
		errorFields{
			Name:    "Forbidden",
			Message: "You can only view your own user data",
			Status:  StatusForbidden,
			Type:    `yii\web\ForbiddenHttpException`,
		},
	)
	notFoundError = errorType(nNotFound+"Error",
		"Resource not found.",
		errorFields{
			Name:    "Not Found",
			Message: "Resource not found",
			Status:  StatusNotFound,
			Type:    `yii\web\NotFoundHttpException`,
		},
	)
	internalError = errorType(nInternalError,
		"Internal server error.",
		errorFields{
			Name:    "Internal Server Error",
			Message: "Could not reticulate splines",
			Status:  StatusInternalServerError,
			Type:    `RuntimeException`,
		},
	)
)

var conflictErrorType = Type(nConflictError, func() {
	Description("The request could not be completed due to a conflict with" +
		" the current state of the resource.")
})

func errorType(name string, descr string, def errorFields) expr.UserType {
	return Type(name, func() {
		Description(descr)

		Field(fErrorName, "name", String, func() {
			Default(def.Name)
			Example(def.Name)
		})
		requiredField(fErrorMessage, "message", String, func() {
			Example(def.Message)
		})
		Field(fErrorCode, "code", Int32, func() {
			Default(def.Code)
			Example(def.Code)
		})
		Field(fErrorStatus, "status", Int32, func() {
			Description("HTTP status code.")
			Default(def.Status)
			Example(def.Status)
		})
		Field(fErrorType, "type", String, func() {
			Description("Fully qualified PHP class name.")
			Default(def.Type)
			Example(def.Type)
		})
	})
}

type errorFields struct {
	Name, Message, Type string
	Code, Status        int
}

const (
	nNotFound      = "NotFound"
	nBadRequest    = "BadRequest"
	nUnauthorized  = "Unauthorized"
	nForbidden     = "Forbidden"
	Conflict       = "Conflict"
	nInternalError = "InternalError"
)
const nConflictError = Conflict + "Error"

const (
	_ uint8 = iota
	fErrorName
	fErrorMessage
	fErrorCode
	fErrorStatus
	fErrorType
)
