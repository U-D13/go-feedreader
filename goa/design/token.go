// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package design

//lint:ignore ST1001 Using the DSL without this would be unwieldy.
import . "goa.design/goa/v3/dsl"

var _ = Service("Token", func() {
	Description("Manages authorization tokens.")

	Security(rJWTAuth)

	errorBadRequest()
	errorUnauthorized()
	errorInternal()

	HTTP(func() {
		Path("/token")
	})
	Method("Create", func() {
		Description(`Create an authorization token for the given credentials.
If invalid credentials are provided, the response is delayed for 5 seconds.`)

		NoSecurity()

		Payload(credentials)
		Result(ourToken)

		HTTP(func() {
			POST("/")

			responseOK()
			responseBadRequest()
			responseUnauthorized()
		})
	})
	Method("Reissue", func() {
		Description("Issue a new authorization token for the holder of current.")

		Payload(ourTokenField)
		Result(ourToken)

		HTTP(func() {
			GET("/")

			responseOK()
			responseUnauthorized()
		})
	})
	Method("Describe", func() {
		Description("Describe an authorization token.")

		Payload(ourTokenField)
		Result(tokenData)

		HTTP(func() {
			GET("/deets")

			responseOK()
			responseUnauthorized()
		})
	})
})

var credentials = Type("Credentials", func() {
	Description("Authentication credentials.")

	requiredUserEmailField()
	requiredUserPasswordField()
})

var ourToken = Type("OurToken", String, func() {
	Description("Our simplified JWT-derivative authorization token.")
	Pattern(`^` + rxToken + `$`)
	Example(exampleToken)
})

var tokenData = Type("TokenData", func() {
	Description("Authorization token details.")

	requiredField(fUID, nUID, UInt32, "The user ID of the token holder.")
	// TODO Expand into 64 bit space, 2038-01-19T03:14:07Z is approaching.
	requiredField(fIAT, nIAT, Int32, "Token issuance timestamp.")
	requiredField(fEXP, nEXP, Int32, "Token expiry timestamp.")
})

const (
	rxToken      = `[\w-]+\.[\w-]+`
	exampleToken = "eyJ1aWQiOjM4LCJpYXQiOjE2Mzg1NTEwNjksImV4cCI6MTYzODYzNzQ2O" +
		"X0.BNx8m83utetrVqz6SdSR0H0nXUMuM6KMkT7ijpsam60"
)

const (
	nUID = "uid"
	nIAT = "iat"
	nEXP = "exp"
)

const (
	_ uint8 = iota
	fUID
	fIAT
	fEXP
)
