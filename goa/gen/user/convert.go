// Code generated by goa v3.10.2, DO NOT EDIT.
//
// User service type conversion functions
//
// Command:
// $ goa gen github.com/antichris/go-feedreader/goa/design --output=..

package user

import (
	user "github.com/antichris/go-feedreader/user"
)

// CreateFromUser initializes t from the fields of v
func (t *ShowUser) CreateFromUser(v *user.User) {
	temp := &ShowUser{
		ID:        v.ID,
		Email:     v.Email,
		FirstName: v.FirstName,
		LastName:  v.LastName,
	}
	*t = *temp
}
