// Code generated by goa v3.10.2, DO NOT EDIT.
//
// Feed client
//
// Command:
// $ goa gen github.com/antichris/go-feedreader/goa/design --output=..

package feed

import (
	"context"

	goa "goa.design/goa/v3/pkg"
)

// Client is the "Feed" service client.
type Client struct {
	IndexEndpoint goa.Endpoint
}

// NewClient initializes a "Feed" service client given the endpoints.
func NewClient(index goa.Endpoint) *Client {
	return &Client{
		IndexEndpoint: index,
	}
}

// Index calls the "Index" endpoint of the "Feed" service.
// Index may return the following errors:
//   - "InternalError" (type *InternalError)
//   - "Unauthorized" (type *UnauthorizedError)
//   - error: internal error
func (c *Client) Index(ctx context.Context, p *IndexPayload) (res []*Item, err error) {
	var ires interface{}
	ires, err = c.IndexEndpoint(ctx, p)
	if err != nil {
		return
	}
	return ires.([]*Item), nil
}
