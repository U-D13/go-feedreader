// Code generated by goa v3.10.2, DO NOT EDIT.
//
// Token HTTP client encoders and decoders
//
// Command:
// $ goa gen github.com/antichris/go-feedreader/goa/design --output=..

package client

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/url"

	token "github.com/antichris/go-feedreader/goa/gen/token"
	goahttp "goa.design/goa/v3/http"
	goa "goa.design/goa/v3/pkg"
)

// BuildCreateRequest instantiates a HTTP request object with method and path
// set to call the "Token" service "Create" endpoint
func (c *Client) BuildCreateRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: CreateTokenPath()}
	req, err := http.NewRequest("POST", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("Token", "Create", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeCreateRequest returns an encoder for requests sent to the Token Create
// server.
func EncodeCreateRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*token.Credentials)
		if !ok {
			return goahttp.ErrInvalidType("Token", "Create", "*token.Credentials", v)
		}
		body := NewCreateRequestBody(p)
		if err := encoder(req).Encode(&body); err != nil {
			return goahttp.ErrEncodingError("Token", "Create", err)
		}
		return nil
	}
}

// DecodeCreateResponse returns a decoder for responses returned by the Token
// Create endpoint. restoreBody controls whether the response body should be
// restored after having been read.
// DecodeCreateResponse may return the following errors:
//   - "BadRequest" (type *token.BadRequestError): http.StatusBadRequest
//   - "Unauthorized" (type *token.UnauthorizedError): http.StatusUnauthorized
//   - error: internal error
func DecodeCreateResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = io.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = io.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusOK:
			var (
				body string
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Create", err)
			}
			err = goa.MergeErrors(err, goa.ValidatePattern("body", body, "^[\\w-]+\\.[\\w-]+$"))
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Create", err)
			}
			res := NewCreateOurTokenOK(body)
			return res, nil
		case http.StatusBadRequest:
			var (
				body CreateBadRequestResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Create", err)
			}
			err = ValidateCreateBadRequestResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Create", err)
			}
			return nil, NewCreateBadRequest(&body)
		case http.StatusUnauthorized:
			var (
				body CreateUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Create", err)
			}
			err = ValidateCreateUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Create", err)
			}
			return nil, NewCreateUnauthorized(&body)
		default:
			body, _ := io.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("Token", "Create", resp.StatusCode, string(body))
		}
	}
}

// BuildReissueRequest instantiates a HTTP request object with method and path
// set to call the "Token" service "Reissue" endpoint
func (c *Client) BuildReissueRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: ReissueTokenPath()}
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("Token", "Reissue", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeReissueRequest returns an encoder for requests sent to the Token
// Reissue server.
func EncodeReissueRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*token.ReissuePayload)
		if !ok {
			return goahttp.ErrInvalidType("Token", "Reissue", "*token.ReissuePayload", v)
		}
		{
			head := p.Auth
			req.Header.Set("Authorization", head)
		}
		return nil
	}
}

// DecodeReissueResponse returns a decoder for responses returned by the Token
// Reissue endpoint. restoreBody controls whether the response body should be
// restored after having been read.
// DecodeReissueResponse may return the following errors:
//   - "Unauthorized" (type *token.UnauthorizedError): http.StatusUnauthorized
//   - error: internal error
func DecodeReissueResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = io.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = io.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusOK:
			var (
				body string
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Reissue", err)
			}
			err = goa.MergeErrors(err, goa.ValidatePattern("body", body, "^[\\w-]+\\.[\\w-]+$"))
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Reissue", err)
			}
			res := NewReissueOurTokenOK(body)
			return res, nil
		case http.StatusUnauthorized:
			var (
				body ReissueUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Reissue", err)
			}
			err = ValidateReissueUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Reissue", err)
			}
			return nil, NewReissueUnauthorized(&body)
		default:
			body, _ := io.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("Token", "Reissue", resp.StatusCode, string(body))
		}
	}
}

// BuildDescribeRequest instantiates a HTTP request object with method and path
// set to call the "Token" service "Describe" endpoint
func (c *Client) BuildDescribeRequest(ctx context.Context, v interface{}) (*http.Request, error) {
	u := &url.URL{Scheme: c.scheme, Host: c.host, Path: DescribeTokenPath()}
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, goahttp.ErrInvalidURL("Token", "Describe", u.String(), err)
	}
	if ctx != nil {
		req = req.WithContext(ctx)
	}

	return req, nil
}

// EncodeDescribeRequest returns an encoder for requests sent to the Token
// Describe server.
func EncodeDescribeRequest(encoder func(*http.Request) goahttp.Encoder) func(*http.Request, interface{}) error {
	return func(req *http.Request, v interface{}) error {
		p, ok := v.(*token.DescribePayload)
		if !ok {
			return goahttp.ErrInvalidType("Token", "Describe", "*token.DescribePayload", v)
		}
		{
			head := p.Auth
			req.Header.Set("Authorization", head)
		}
		return nil
	}
}

// DecodeDescribeResponse returns a decoder for responses returned by the Token
// Describe endpoint. restoreBody controls whether the response body should be
// restored after having been read.
// DecodeDescribeResponse may return the following errors:
//   - "Unauthorized" (type *token.UnauthorizedError): http.StatusUnauthorized
//   - error: internal error
func DecodeDescribeResponse(decoder func(*http.Response) goahttp.Decoder, restoreBody bool) func(*http.Response) (interface{}, error) {
	return func(resp *http.Response) (interface{}, error) {
		if restoreBody {
			b, err := io.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			resp.Body = io.NopCloser(bytes.NewBuffer(b))
			defer func() {
				resp.Body = io.NopCloser(bytes.NewBuffer(b))
			}()
		} else {
			defer resp.Body.Close()
		}
		switch resp.StatusCode {
		case http.StatusOK:
			var (
				body DescribeResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Describe", err)
			}
			err = ValidateDescribeResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Describe", err)
			}
			res := NewDescribeTokenDataOK(&body)
			return res, nil
		case http.StatusUnauthorized:
			var (
				body DescribeUnauthorizedResponseBody
				err  error
			)
			err = decoder(resp).Decode(&body)
			if err != nil {
				return nil, goahttp.ErrDecodingError("Token", "Describe", err)
			}
			err = ValidateDescribeUnauthorizedResponseBody(&body)
			if err != nil {
				return nil, goahttp.ErrValidationError("Token", "Describe", err)
			}
			return nil, NewDescribeUnauthorized(&body)
		default:
			body, _ := io.ReadAll(resp.Body)
			return nil, goahttp.ErrInvalidResponse("Token", "Describe", resp.StatusCode, string(body))
		}
	}
}
