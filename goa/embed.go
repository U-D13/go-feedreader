// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package goa

import (
	"embed"

	"github.com/antichris/go-feedreader/internal/util/fs"
)

//go:embed gen/http/openapi3.*
var _fs embed.FS
var OpenAPIFS = fs.MustSub(_fs, "gen/http")
