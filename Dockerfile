##	This Source Code Form is subject to the terms of the Mozilla Public
##	License, v. 2.0. If a copy of the MPL was not distributed with this
##	file, You can obtain one at https://mozilla.org/MPL/2.0/.

##	Set this to `arm64` to cross-compile with musl on Linux.
##	Leave empty when building on other than `linux/amd64` for `linux/arm64`.
##	Note that this is not the same as `TARGETARCH` used by the BuildKit backend:
##	https://docs.docker.com/engine/reference/builder/#automatic-platform-args-in-the-global-scope
ARG TARGET_ARCH=

FROM golang:1.19.5-alpine3.17 AS go

FROM go AS build
ARG TARGET_ARCH
##	Alpine's `wget` shows progress bar with no customization.
##	Even if `printf` failed, so would `sha256sum` right away.
#	hadolint ignore=DL3047,DL4006
RUN set -ux \
&&	if [ "$TARGET_ARCH" = arm64 ]; then \
		TGZ='aarch64-linux-musl-cross.tgz' \
		SHA='c909817856d6ceda86aa510894fa3527eac7989f0ef6e87b5721c58737a06c38' \
&&		wget -O "$TGZ" "https://musl.cc/${TGZ}" \
&&		sha256sum "$TGZ"  \
&&		printf '%s  %s' "$SHA" "$TGZ" | sha256sum -c \
&&		tar -zxf "$TGZ" -C / \
&&		rm "$TGZ" \
;	else \
		apk add --no-cache \
			gcc=12.2.1_git20220924-r4 \
			musl-dev=1.2.3-r4 \
;	fi

WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download -x

COPY . .
ARG TARGET_ARCH
RUN set -ux \
&&	if [ "$TARGET_ARCH" = arm64 ]; then \
	export \
		GOARCH="$TARGET_ARCH" \
##		Use the cross-compiler toolchain un-tar-gzipped earlier.
		CC='/aarch64-linux-musl-cross/bin/aarch64-linux-musl-gcc' \
;	fi \
&&	CGO_ENABLED=1 \
	go build -v \
		-trimpath \
		-a -ldflags '-s -w -linkmode=external -extldflags=-static' \
		-tags 'cgo netgo osusergo static_build' \
		./cmd/feedreader

FROM go AS certs
RUN apk add --no-cache \
	ca-certificates-bundle=20220614-r4

FROM scratch AS aggregation
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /build/feedreader /build/LICENSE /build/README.md /

FROM scratch AS final
COPY --from=aggregation / /
ENTRYPOINT [ "/feedreader" ]
