// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd

import (
	"github.com/antichris/go-feedreader/internal/util/strings"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
)

// NewRoot constructs a new root [Command] with [Config].
func NewRoot(name string) (*Command, *Config) {
	cf := &Config{}
	cm := &Command{
		Name:      name,
		ShortDesc: "Feed Reader server CLI",
		LongDesc:  "Manage Feed Reader server using command line interface.",
		EnvPrefix: "FR",
		Flags: []*Flag{{
			// 	Name:        FlagCFGFile,
			// 	Description: "configuration file",
			// 	Pointer:     &cf.CFGFile,
			// 	Shorthand:   "c",
			// }, {
			Name:        "verbose",
			Description: "log verbose output",
			Pointer:     &cf.Verbose,
			Shorthand:   "v",
			PreferShort: true,
		}},
	}
	return cm, cf
}

// Config for the root command.
// Includes flags and types that should be available to all subcommands.
type Config struct {
	Verbose bool
}

// FlagCFGFile is the name of the configuration file flag.
const FlagCFGFile = "config"

// DBConfigFlags returns the default [DBConfig] bound to flags.
func DBConfigFlags() (*DBConfig, []*Flag) {
	c := DefaultDBConfig()
	d := &Flag{
		Name:        "driver",
		Description: "Database driver identifier",
		Pointer:     &c.Driver,
	}
	f := []*Flag{d, {
		Name:        "dsn",
		Description: "Scheme-less data source name URI",
		Pointer:     &c.DSN,
	}}
	ds := sqlc.Drivers()
	if len(ds) == 0 {
		return &c, f
	}
	d.Description += " (" + strings.SentenceQuoted(ds, "or") + ")"
	return &c, f
}

func DefaultDBConfig() DBConfig {
	return DBConfig{
		Driver: "sqlite",
		DSN:    "feedreader.db",
	}
}

// DBConfig defines the database connection parameter config.
type DBConfig struct {
	// Driver to use for storage.
	Driver string

	// Data source name.
	// Only the URL form is supported.
	//
	// The scheme prefix must be omitted, since that is determined by
	// the Driver field.
	//
	// E.g.
	//
	// 	"user:pass@host:port/path?query"
	//
	// Instead of
	//
	// 	"scheme://user:pass@host:port/path?query"
	DSN string
}
