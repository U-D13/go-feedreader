// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd_test

import (
	"testing"

	. "github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestNewRoot(t *testing.T) {
	t.Parallel()
	const name = "name"

	cm, cf := NewRoot(name)

	r := require.New(t)
	r.Equal(name, cm.Name)

	f, ok := LookupFlag(cm.Flags, "verbose")
	r.True(ok)
	p, ok := f.Pointer.(*bool)
	r.True(ok)
	r.Same(&cf.Verbose, p)
}

func TestDBConfigFlags(t *testing.T) {
	t.Parallel()
	t.Run("pointer", func(t *testing.T) {
		t.Parallel()
		c, fs := DBConfigFlags()
		// TODO Use a similar test for other config flags.
		for n, want := range map[string]*string{
			"driver": &c.Driver,
			"dsn":    &c.DSN,
		} {
			n, want := n, want
			t.Run(n, func(t *testing.T) {
				t.Parallel()
				f, ok := LookupFlag(fs, n)
				r := require.New(t)
				r.True(ok)
				got, ok := f.Pointer.(*string)
				r.True(ok)
				r.Same(want, got)
			})
		}
	})
	t.Run("driver description", func(t *testing.T) {
		t.Parallel()
		want := ` ("a", "b", "c", "d" or "e")`

		c := gomock.NewController(t)
		for _, d := range []sqlc.Driver{
			"a", "b", "c", "d", "e",
		} {
			mocks.RegisterMockAdapter(c, d)
		}

		_, fs := DBConfigFlags()
		f, ok := LookupFlag(fs, "driver")

		r := require.New(t)
		r.True(ok)

		got := f.Description
		r.Contains(got, want)
	})
}
