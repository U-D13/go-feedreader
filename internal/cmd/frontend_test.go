// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd_test

import (
	"context"
	"io"
	"testing"

	. "github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/stretchr/testify/require"
)

func TestFrontendFactoryFunc(t *testing.T) {
	t.Parallel()
	var (
		cm = &Command{Name: "foo"}
		cf = &Config{
			// CFGFile: "bar"
		}
		f = &mockFrontend{}
	)
	m := testu.NewFuncMock(t, "frontendFactoryFunc")

	fn := func(cm *Command, cf *Config) Frontend {
		return m.Called(cm, cf).Get(0).(Frontend)
	}
	m.On(cm, cf).Return(f)

	ff := FrontendFactoryFunc(fn)
	got := ff.Create(cm, cf)

	require.Same(t, f, got)
	m.AssertExpectations(t)
}

type mockFrontend struct{}

func (mockFrontend) Exec(context.Context, []string) (_ int) {
	return
}
func (mockFrontend) SetErrOutput(io.Writer) {}
func (mockFrontend) SetInput(io.Reader)     {}
func (mockFrontend) SetOutput(io.Writer)    {}
