// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package testdata

import "github.com/antichris/go-feedreader/internal/cmd"

// WithSubcommands returns a copy of c with two subcommands.
func WithSubcommands(c cmd.Command) *cmd.Command {
	c.Subcommands = []*cmd.Command{
		Command("1", "sub-"),
		Command("2", "sub-"),
	}
	return &c
}

// Command has the name and short and long descriptions prefilled with
// given prefix and suffix pre- and appended, respectively
func Command(suffix, prefix string) *cmd.Command {
	return &cmd.Command{
		Name:      prefix + "command" + suffix,
		ShortDesc: prefix + "shortDescription" + suffix,
		LongDesc:  prefix + "longDescription" + suffix,
	}
}

// BoolFlag has the name and description prefilled with given prefix and
// suffix pre- and appended, respectively, and given bool pointer set.
func BoolFlag(suffix, prefix string, pointer *bool) *cmd.Flag {
	f := Flag(suffix, prefix)
	f.Pointer = pointer
	return f
}

// BoolFlag has the name and description prefilled with given prefix and
// suffix pre- and appended, respectively, and given bool pointer set.
func StringFlag(suffix, prefix string, pointer *string) *cmd.Flag {
	f := Flag(suffix, prefix)
	f.Pointer = pointer
	return f
}

// Flag has the name and description prefilled with given prefix and
// suffix pre- and appended, respectively
func Flag(suffix, prefix string) *cmd.Flag {
	return &cmd.Flag{
		Name:        prefix + "flag" + suffix,
		Description: prefix + "description" + suffix,
	}
}
