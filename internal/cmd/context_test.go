// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestContextCommand(t *testing.T) {
	t.Parallel()
	c := &Command{}
	ctx := context.Background()
	for n, tt := range map[string]struct {
		ctx context.Context
		wC  *Command
		wOk bool
	}{"without": {
		ctx: ctx,
		wOk: false,
	}, "with": {
		ctx: WithCommand(ctx, c),
		wC:  c,
		wOk: true,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			gotC, gotOk := GetCommand(tt.ctx)
			r := require.New(t)
			r.Equal(tt.wOk, gotOk)
			r.Equal(tt.wC, gotC)
		})
	}
}
