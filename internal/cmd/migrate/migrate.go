// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package migrate implements a command to migrate Feed Reader storage
// to the latest version by applying the necessary migrations.
package migrate

// TODO Implement finer grained subcommands (listing, stepping up/down etc.)

import (
	"context"
	"errors"
	"io"
	"io/fs"
	"log"
	"net/url"
	"os"
	"os/signal"
	"syscall"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/source"
	"github.com/golang-migrate/migrate/v4/source/iofs"
)

// New instance of the [migrate] subcommand.
func New(rcf *cmd.Config, out io.Writer) *cmd.Command {
	dc, df := cmd.DBConfigFlags()
	c := newCFG(rcf, dc, out)
	return &cmd.Command{
		Name:      "migrate",
		ShortDesc: "Migrate Feed Reader storage",
		LongDesc: `Migrate Feed Reader storage to the latest version by applying the
necessary migrations.`,
		Exec: func(ctx context.Context, args []string) (err error) {
			return exec(ctx, c)
		},
		Flags: df,
	}
}

var exec = Exec

// Exec function for this command.
func Exec(ctx context.Context, c Configger) (err error) {
	fsys, u, err := c.AdapterData()
	if err != nil {
		return
	}
	d, err := iofs.New(fsys, ".")
	if err != nil {
		return
	}
	defer d.Close()

	// XXX URL scheme must match the migrate driver name exactly.
	m, err := newMigrate(d, u)
	if err != nil {
		return
	}
	l := log.New(c.Output(), "", 0)

	go abortOnSignal(m, l)
	m.SetLog(newLogger(c, l))

	if c.Verbose() {
		// Had u not been valid, we'd not have passed err check earlier.
		u_, _ := url.Parse(u)
		l.Printf("migrating %q up...", u_.Redacted())
	}

	if err = m.Up(); err != nil {
		l.Println(err)
		if errors.Is(err, migrate.ErrNoChange) {
			err = nil
		}
	}
	return
}

// Configger provides the config for executing migrations.
type Configger interface {
	// Driver is the database driver name.
	Driver() sqlc.Driver
	// AdapterData returns the schema file system and migration DSN URL
	// for the [sqlc.Adapter] registered for the driver.
	// An error is returned if it was impossible to locate an adapter
	// for the driver.
	AdapterData() (fsys fs.FS, url string, err error)
	// Verbose returns whether operations should log verbose output.
	Verbose() bool
	// Output is the configured output stream.
	Output() io.Writer
}

func newCFG(rcf *cmd.Config, dc *cmd.DBConfig, out io.Writer) Configger {
	return &cfg{
		rcf: rcf,
		dbc: dc,
		out: out,
	}
}

type cfg struct {
	rcf *cmd.Config
	dbc *cmd.DBConfig
	out io.Writer
}

func (c cfg) AdapterData() (fsys fs.FS, url string, err error) {
	a, err := sqlc.GetAdapter(c.Driver())
	if err != nil {
		return
	}
	fsys = a.Schema()
	url = a.MigrateURL(sqlc.DSN(c.dbc.DSN))
	return
}

func (c cfg) Driver() sqlc.Driver { return sqlc.Driver(c.dbc.Driver) }
func (c cfg) Verbose() bool       { return c.rcf.Verbose }
func (c cfg) Output() io.Writer   { return c.out }

// newMigrate returns a new [migrator] instance from an existing source
// instance and a database URL.
//
// A thin wrapper for [migrate.NewWithSourceInstance].
var newMigrate = func(d source.Driver, url string) (migrator, error) {
	m, err := migrate.NewWithSourceInstance("iofs", d, url)
	return migr{m}, err
}

// migrator abstracts [migrate.Migrate] away to enable mocking.
type migrator interface {
	// Up looks at the currently active migration version and will
	// migrate all the way up (applying all up migrations).
	Up() error
	// GracefulStop signals Migrate to stop executing migrations as soon
	// as possible at a safe break point, so that the database is not
	// corrupted.
	GracefulStop()
	SetLog(migrate.Logger)
}

var _ migrator = migr{}

// migr is an implementation of [migrator].
type migr struct{ *migrate.Migrate }

// GracefulStop signals [migrate.Migrate] to stop executing migrations
// as soon as possible at a safe break point, so that the database is
// not corrupted.
//
// It never blocks.
func (m migr) GracefulStop() {
	go func() { m.Migrate.GracefulStop <- true }()
}
func (m migr) SetLog(l migrate.Logger) { m.Log = l }

func abortOnSignal(m migrator, l printfer) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	l.Printf("received signal: %v", <-c)
	m.GracefulStop()
	signal.Stop(c)
}

// newLogger instance.
func newLogger(v verboser, p printfer) logger {
	return logger{verboser: v, printfer: p}
}

var _ migrate.Logger = logger{}

// logger implements [migrate.Logger].
type logger struct {
	verboser
	printfer
}

// printfer implements a Printf method.
type printfer interface {
	Printf(format string, v ...interface{})
}

// verboser implements a Verbose method.
type verboser interface{ Verbose() bool }
