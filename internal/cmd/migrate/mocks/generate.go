// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package mocks provides a GoMock implementation of [migrate.Configger].
package mocks

//go:generate -command mg go run github.com/golang/mock/mockgen -package=$GOPACKAGE -copyright_file=../../../_resources/license-header
//go:generate mg -destination=mocks.go github.com/antichris/go-feedreader/internal/cmd/migrate Configger
//go:generate mg -destination=database_mocks.go github.com/golang-migrate/migrate/v4/database Driver
//go:generate go run mvdan.cc/gofumpt -w mocks.go database_mocks.go
