// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package migrate_test

import (
	"bytes"
	"context"
	"io/fs"
	"log"
	"testing"
	"testing/fstest"
	"time"

	"github.com/antichris/go-feedreader/internal/cmd"
	. "github.com/antichris/go-feedreader/internal/cmd/migrate"
	"github.com/antichris/go-feedreader/internal/cmd/migrate/mocks"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/antichris/go-feedreader/user/storage/sqlc"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	t.Parallel()
	registerMockAdapter(gomock.NewController(t))
	wErr := errSnap
	var (
		cf     = &cmd.Config{Verbose: true}
		dc, df = cmd.DBConfigFlags()
		b      = &bytes.Buffer{}
	)
	mx := testu.NewMock(t)
	exec := func(ctx context.Context, c Configger) (err error) {
		return mx.MethodCalled("exec", ctx, c).Error(0)
	}
	mx.On("exec", ctx, mock.MatchedBy(func(c Configger) (ok bool) {
		a := assert.New(t)
		return a.Equal(c.Verbose(), cf.Verbose) &&
			a.Equal(c.Driver(), sqlc.Driver(dc.Driver)) &&
			a.Equal(c.Output(), b)
	})).Return(wErr)

	got := New(cf, b)

	var err error
	withExec(exec, func() {
		err = got.Exec(ctx, []string{})
	})

	r := require.New(t)
	r.Equal(wErr, err)
	r.Subset(got.Flags, df)
	mx.AssertExpectations(t)
}

func TestExec(t *testing.T) {
	t.Parallel()
	fsys := fstest.MapFS{}
	for n, tt := range map[string]struct {
		fsys    fs.FS
		url     string
		adapter error
		newMigr error
		verbose bool
		up      error
		wOut    string
		wErr    string
	}{"adapter error": {
		adapter: errSnap,
		wErr:    errSnap.Error(),
	}, "iofs error": {
		fsys: errFS{errSnap},
		wErr: "failed to init driver with path .: snap",
	}, "newMigrate error": {
		fsys:    fsys,
		newMigr: errSnap,
		wErr:    errSnap.Error(),
	}, "up error": {
		fsys: fsys,
		up:   errSnap,
		wErr: errSnap.Error(),
	}, "no change": {
		fsys: fsys,
		up:   migrate.ErrNoChange,
		wOut: "no change\n",
	}, "verbose": {
		fsys:    fsys,
		verbose: true,
		wOut:    "migrating \"\" up...\n",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			b := &bytes.Buffer{}

			mc := mocks.NewMockConfigger(gomock.NewController(t))
			mm := newMockMigrator(t)
			newMigrate, nm := mockNewMigrate(t)

			cx := mc.EXPECT()
			cx.AdapterData().
				Return(tt.fsys, tt.url, tt.adapter)
			cx.Output().
				Return(b).
				AnyTimes()
			cx.Verbose().
				Return(tt.verbose).
				AnyTimes()
			nm.On("newMigrate", mock.Anything, tt.url).
				Return(mm, tt.newMigr)
			mm.On("SetLog", mock.Anything)
			mm.On("Up").
				Return(tt.up)

			var err error
			withNewMigrate(newMigrate, func() {
				err = Exec(ctx, mc)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(b.String(), tt.wOut)
			nm.AssertExpectations(t)
		})
	}
	// TODO Test the effect of abortOnSignal.
}

func Test_newMigrate(t *testing.T) {
	t.Parallel()
	var (
		d   = mocks.NewMockDriver(gomock.NewController(t))
		s   = "mock-driver"
		url = s + ":"
	)
	d.EXPECT().
		Open(url).
		AnyTimes()

	func() {
		defer func() {
			v := recover()
			if v == nil || v == "Register called twice for driver "+s {
				return
			}
			panic(v)
		}()
		database.Register(s, d)
	}()

	for n, tt := range map[string]struct {
		url  string
		wErr string
	}{"error": {
		wErr: "URL cannot be empty",
	}, "nominal": {
		url: url,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var err error
			withDefaultNewMigrate(func() {
				// TODO Consider checking the [migrator] instance.
				_, err = newMigrate(nil, tt.url)
			})

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
		})
	}
}

func Test_cfg_AdapterData(t *testing.T) {
	t.Parallel()
	registerMockAdapter(gomock.NewController(t))

	a, err := sqlc.GetAdapter(driver)
	require.NoError(t, err)
	wFsys := a.Schema()

	for n, tt := range map[string]struct {
		driver string
		wErr   string
	}{"bad driver": {
		driver: "foo",
		wErr:   `unrecognized DB driver: "foo"`,
	}, "nominal": {
		driver: driver,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()

			cf := &cmd.Config{}
			dc, _ := cmd.DBConfigFlags()
			b := &bytes.Buffer{}

			dc.Driver = tt.driver
			wURL := a.MigrateURL(sqlc.DSN(dc.DSN))

			c := newCFG(cf, dc, b)

			fsys, url, err := c.AdapterData()

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(wFsys, fsys)
			r.Equal(wURL, url)
		})
	}
}

func Test_migr_GracefulStop(t *testing.T) {
	t.Parallel()
	mm := &migrate.Migrate{
		GracefulStop: make(chan bool),
	}
	m := newMigr(mm)

	go m.GracefulStop()

	require.Eventually(t, func() bool {
		select {
		case <-mm.GracefulStop:
			return true
		default:
			return false
		}
	}, 1*time.Millisecond, 10*time.Microsecond)
}

func Test_setLog(t *testing.T) {
	t.Parallel()
	var (
		mm = &migrate.Migrate{}
		mc = mocks.NewMockConfigger(gomock.NewController(t))
		l  = log.New(&bytes.Buffer{}, "", 0)
		m  = newMigr(mm)
		ml = newMiglog(mc, l)
	)
	r := require.New(t)
	r.Nil(mm.Log)

	m.SetLog(ml)

	r.NotNil(mm.Log)
	r.Equal(ml, mm.Log)
}
