// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package migrate

import (
	"context"
	"sync"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/source"
)

type (
	Xmigrator = migrator

	XexecFunc       = execFunc
	XnewMigrateFunc = newMigrateFunc
)

var (
	XnewCFG     = newCFG
	XnewMigrate = newMigrate
	XnewMiglog  = newLogger

	XwithExec       = withExec
	XwithNewMigrate = withNewMigrate

	XwithDefaultNewMigrate = withDefaultNewMigrate
)

func withExec(execFn execFunc, fn func()) {
	execMu.Lock()
	defer execMu.Unlock()
	defer func(v execFunc) { exec = v }(exec)
	exec = execFn
	fn()
}

type execFunc func(ctx context.Context, c Configger) (err error)

var execMu = sync.Mutex{}

func withNewMigrate(newMigrateFn newMigrateFunc, fn func()) {
	newMigrateMu.Lock()
	defer newMigrateMu.Unlock()
	defer func(v newMigrateFunc) { newMigrate = v }(newMigrate)
	newMigrate = newMigrateFn
	fn()
}

type newMigrateFunc func(d source.Driver, url string) (migrator, error)

var newMigrateMu = sync.Mutex{}

func withDefaultNewMigrate(fn func()) {
	newMigrateMu.Lock()
	defer newMigrateMu.Unlock()
	fn()
}

func XnewMigr(m *migrate.Migrate) migrator { return migr{m} }
