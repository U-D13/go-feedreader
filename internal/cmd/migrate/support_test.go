// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package migrate_test

import (
	"context"
	"io/fs"
	"sync"
	"testing"

	. "github.com/antichris/go-feedreader/internal/cmd/migrate"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/antichris/go-feedreader/terror"
	"github.com/antichris/go-feedreader/user/storage/sqlc/mocks"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/source"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/mock"
)

type (
	migrator = Xmigrator

	execFunc       = XexecFunc
	newMigrateFunc = XnewMigrateFunc
)

var (
	newCFG     = XnewCFG
	newMigrate = XnewMigrate
	newMiglog  = XnewMiglog

	withExec       = XwithExec
	withNewMigrate = XwithNewMigrate
	newMigr        = XnewMigr

	withDefaultNewMigrate = XwithDefaultNewMigrate
)

const errSnap = terror.Error("snap")

var ctx = context.Background()

const driver = "mock"

func registerMockAdapter(c *gomock.Controller) {
	registerOnce.Do(func() {
		mocks.RegisterMockAdapter(c, driver)
	})
}

var registerOnce = sync.Once{}

func mockNewMigrate(t *testing.T) (newMigrateFunc, *mock.Mock) {
	m := testu.NewMock(t)
	newMigrate := func(d source.Driver, url string) (migrator, error) {
		r := m.MethodCalled("newMigrate", d, url)
		return r.Get(0).(migrator), r.Error(1)
	}
	return newMigrate, m
}

func newMockMigrator(t mock.TestingT) *mockMigrator {
	return &mockMigrator{testu.NewMock(t)}
}

var _ migrator = (*mockMigrator)(nil)

type mockMigrator struct{ *mock.Mock }

func (m *mockMigrator) GracefulStop()           { m.Called() }
func (m *mockMigrator) SetLog(l migrate.Logger) { m.Called(l) }
func (m *mockMigrator) Up() error               { return m.Called().Error(0) }

var _ fs.FS = errFS{}

type errFS struct{ error }

func (e errFS) Open(name string) (fs.File, error) { return nil, e }
