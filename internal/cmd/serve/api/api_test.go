// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package api

import (
	"context"
	"testing"

	"github.com/antichris/go-feedreader/internal/cmd/serve"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/antichris/go-feedreader/terror"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	t.Parallel()
	var (
		ctx  = context.Background()
		wErr = terror.Error("snap")
	)
	m := testu.NewFuncMock(t, "Exec")
	exec := func(ctx context.Context, c *Config) error {
		return m.Called(ctx, c).Error(0)
	}
	m.On(ctx, mock.MatchedBy(func(*Config) bool { return true })).
		Return(wErr)

	// TODO Properly test the various details of the returned value.
	got := New(&serve.Config{}, exec)
	err := got.Exec(ctx, nil)

	r := require.New(t)
	r.Equal(wErr, err)
	m.AssertExpectations(t)
}
