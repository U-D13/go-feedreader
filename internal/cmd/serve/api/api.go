// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package api implements a command that starts the Feed Reader API
// server.
package api

import (
	"context"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/cmd/serve"
	"github.com/antichris/go-feedreader/token"
)

func New(
	scf *ServerConfig, fn func(context.Context, *Config) error,
) *cmd.Command {
	c, f := ConfigFlagsMerged(scf)
	return &cmd.Command{
		Name:      "api",
		ShortDesc: "Serve Feed Reader API",
		LongDesc:  "Start serving Feed Reader API service endpoints.",
		Exec: func(ctx context.Context, args []string) error {
			return fn(ctx, c)
		},
		Flags: f,
	}
}

func ConfigFlagsMerged(cf *ServerConfig) (*Config, []*cmd.Flag) {
	c, f := ConfigFlags()
	c.Server = cf
	{
		dc, df := cmd.DBConfigFlags()
		sc, sf := SMTPConfigFlags()
		c.DBConfig = dc
		c.SMTP = sc
		f = append(f, df...)
		f = append(f, sf...)
	}
	return c, f
}

func ConfigFlags() (*Config, []*cmd.Flag) {
	c := DefaultConfig()
	f := []*cmd.Flag{{
		Name:        "ui-url",
		Description: "URL of the web UI root page",
		Pointer:     &c.FrontendURL,
	}, {
		Name:        "token-key",
		Description: "HMAC key for API token signing",
		Pointer:     &c.TokenKey,
		Shorthand:   "k",
	}}
	return &c, f
}

func DefaultConfig() Config {
	return Config{
		// FIXME FrontendURL must depend on UI server.
		FrontendURL: "http://feedreader.localhost:8080",
		// 64 base64-encoded bytes yield 48 bytes (384 bits) of randomness.
		TokenKey: genTokenKey(64),
	}
}

type Config struct {
	FrontendURL string
	TokenKey    string

	SMTP   *SMTPConfig
	Server *ServerConfig
	*DBConfig
}

type (
	DBConfig     = cmd.DBConfig
	ServerConfig = serve.Config
)

func SMTPConfigFlags() (*SMTPConfig, []*cmd.Flag) {
	c := &SMTPConfig{
		Sender: "no-reply@feedreader.localhost",
		Host:   "localhost",
		Port:   "2500",
	}
	f := []*cmd.Flag{{
		Name:        "smtp-host",
		Description: "Host name of the SMTP server",
		Pointer:     &c.Host,
	}, {
		Name:        "smtp-port",
		Description: "Port number or name of the SMTP server",
		Pointer:     &c.Port,
	}, {
		Name:        "smtp-user",
		Description: "User name for authentication on the SMTP server",
		Pointer:     &c.User,
	}, {
		Name:        "smtp-pass",
		Description: "Password for authentication on the SMTP server",
		Pointer:     &c.Pass,
	}}
	return c, f
}

type SMTPConfig struct {
	Sender string
	Host   string
	Port   string
	User   string
	Pass   string
}

// genTokenKey returns l base64-encoded base64-encoded cryptographically
// secure random bytes for use as the API token HMAC signing key.
func genTokenKey(l int) string {
	k := make([]byte, l)
	_, err := token.B64RandReader.Read(k)
	if err != nil {
		// This should never occur.
		panic(err)
	}
	return string(k)
}
