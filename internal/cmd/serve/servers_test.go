// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package serve

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestServerURL(t *testing.T) {
	t.Parallel()
	type test = struct {
		serverID string
		host     string
		port     string
		secure   bool
		want     *url.URL
		wErr     string
	}
	for n, tt := range map[string]test{"bad server ID": {
		serverID: "foo",
		wErr:     `invalid server ID: "foo"`,
	}, "nominal": func(v test) test {
		u, _ := url.Parse(servers[v.serverID])
		u.Host = v.host + ":" + v.port
		v.want = u
		return v
	}(test{
		serverID: "dev",
		host:     "foo",
		port:     "http",
	})} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got, err := ServerURL(tt.serverID, tt.host, tt.port, tt.secure)

			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func Test_invalidServerIDErr_ErrorHint(t *testing.T) {
	t.Parallel()
	want := `invalid server ID: "foo" (valid servers: dev|devs|prod)`
	err := invalidServerIDErr("foo")

	got := err.ErrorHint()
	require.Equal(t, want, got)
}

func Test_prepURL(t *testing.T) {
	t.Parallel()
	const (
		b  = "//1.localhost"
		bp = "//2.localhost:443"
		h  = "3.localhost"
		dp = ":80"
		p  = "8080"
	)
	tests := map[string]struct {
		base   string
		host   string
		port   string
		secure bool
		want   string
		wErr   string
	}{"bad base": {
		base: ":",
		wErr: "missing protocol scheme",
	}, "nominal": {
		base: b,
		want: b + dp,
	}, "secure": {
		base:   b,
		secure: true,
		want:   "https:" + b + dp,
	}, "host": {
		base: b,
		host: h,
		want: "//" + h + dp,
	}, "port": {
		base: b,
		port: p,
		want: b + ":" + p,
	}, "nominal with port": {
		base: bp,
		want: bp,
	}, "secure with port": {
		base:   bp,
		secure: true,
		want:   "https:" + bp,
	}, "host with port": {
		base: bp,
		host: h,
		want: "//" + h + dp,
	}, "port with port": {
		base: bp,
		port: p,
		want: "//2.localhost:" + p,
	}}
	for n, tt := range tests {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got, err := prepURL(tt.base, tt.host, tt.port, tt.secure)
			r := require.New(t)
			if tt.wErr != "" {
				r.ErrorContains(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got.String())
		})
	}
}
