// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package serve implements the root command for serving Feed Reader.
package serve

import (
	"net/url"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/util/strings"
)

// New returns config for the subcommand.
func New(rc *cmd.Config) (*cmd.Command, *Config) {
	c, f := ConfigFlags(rc)
	return &cmd.Command{
		Name:      "serve",
		ShortDesc: "Serve Feed Reader",
		LongDesc: "Start serving Feed Reader static resources and/or API" +
			" service endpoints.",
		Flags: f,
	}, c
}

func ConfigFlags(rc *cmd.Config) (*Config, []*cmd.Flag) {
	c := &Config{
		RootConfig: rc,
		ServerID:   "dev",
	}
	tf := TLSFlags(&c.TLSConfig)
	serverIDs := strings.SentenceQuoted(keys(servers), "or")
	f := []*cmd.Flag{{
		Name:        "server",
		Description: "Server host ID (" + serverIDs + ")",
		Pointer:     &c.ServerID,
	}, {
		Name: "host",
		Description: "Host domain name (overrides host domain specified" +
			" in service design)",
		Pointer: &c.Host,
		// Shorthand: "h",
	}, {
		Name: "port",
		Description: "HTTP port (overrides host HTTP port specified in" +
			" service design)",
		Pointer:   &c.Port,
		Shorthand: "p",
	}, {
		Name:        "debug",
		Description: "Log request and response bodies",
		Pointer:     &c.Debug,
	}}
	f = append(f, tf...)
	return c, f
}

func TLSFlags(cf *TLSConfig) []*cmd.Flag {
	f := []*cmd.Flag{{
		Name:        "secure",
		Description: "Use secure scheme (https or grpcs)",
		Pointer:     &cf.Secure,
		Shorthand:   "s",
	}, {
		Name:        "cert-file",
		Description: "The path of a TLS certificate file for HTTPS mode",
		Pointer:     &cf.CertFile,
	}, {
		Name:        "key-file",
		Description: "The path of a TLS certificate private key file for HTTPS mode",
		Pointer:     &cf.KeyFile,
	}}
	return f
}

// Config for a server subcommand.
type Config struct {
	ServerID string
	Host     string
	Port     string
	Debug    bool

	TLSConfig
	*RootConfig
}

func (c Config) URL() (*url.URL, error) {
	return ServerURL(c.ServerID, c.Host, c.Port, c.Secure)
}

type TLSConfig struct {
	Secure   bool
	CertFile string
	KeyFile  string
}

type RootConfig = cmd.Config
