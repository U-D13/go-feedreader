// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package serve_test

import (
	"testing"

	"github.com/antichris/go-feedreader/internal/cmd"
	. "github.com/antichris/go-feedreader/internal/cmd/serve"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	t.Parallel()
	const name = "serve"
	cm, _ := New(&cmd.Config{})

	r := require.New(t)
	r.Equal(name, cm.Name)
}

func TestConfigFlags(t *testing.T) {
	t.Parallel()
	c, fs := ConfigFlags(nil)
	t.Run("pointer", func(t *testing.T) {
		t.Parallel()
		for n, want := range map[string]*string{
			"server": &c.ServerID,
			"host":   &c.Host,
			"port":   &c.Port,
		} {
			n, want := n, want
			t.Run(n, func(t *testing.T) {
				t.Parallel()
				f, ok := cmd.LookupFlag(fs, n)
				r := require.New(t)
				r.True(ok)
				got, ok := f.Pointer.(*string)
				r.True(ok)
				r.Same(want, got)
			})
		}
		n, want := "debug", &c.Debug
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			f, ok := cmd.LookupFlag(fs, n)
			r := require.New(t)
			r.True(ok)
			got, ok := f.Pointer.(*bool)
			r.True(ok)
			r.Same(want, got)
		})
	})
	t.Run("server description", func(t *testing.T) {
		t.Parallel()
		want := ` ("dev", "devs" or "prod")`

		f, ok := cmd.LookupFlag(fs, "server")

		r := require.New(t)
		r.True(ok)

		got := f.Description
		r.Contains(got, want)
	})
}

func TestConfig_URL(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		c    Config
		want string
		wErr string
	}{"bad server": {
		c: Config{
			ServerID: "bad",
		},
		wErr: `invalid server ID: "bad"`,
	}, "nominal": {
		c: Config{
			ServerID:  "prod",
			Port:      "443",
			TLSConfig: TLSConfig{Secure: true},
		},
		want: "https://feedreader.localhost:443",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got, err := tt.c.URL()
			r := require.New(t)
			if tt.wErr != "" {
				r.EqualError(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got.String())
		})
	}
}
