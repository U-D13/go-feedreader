// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package all implements a command that starts a combined server
// serving both the Feed Reader API and static resources.
package all

import (
	"context"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/cmd/serve"
	"github.com/antichris/go-feedreader/internal/cmd/serve/api"
	"github.com/antichris/go-feedreader/internal/cmd/serve/ui"
)

func New(
	rcf *cmd.Config, fn func(context.Context, *Config) error,
) *cmd.Command {
	c, f := ConfigFlagsMerged(rcf)
	return &cmd.Command{
		Name:      "all",
		ShortDesc: "Serve all Feed Reader",
		LongDesc: "Start serving Feed Reader static resources and API" +
			" service endpoints.",
		Exec: func(ctx context.Context, args []string) error {
			return fn(ctx, c)
		},
		Flags: f,
	}
}

func ConfigFlagsMerged(rcf *cmd.Config) (*Config, []*cmd.Flag) {
	bc, bf := serve.ConfigFlags(rcf)
	ac, af := api.ConfigFlagsMerged(bc)
	sc, sf := ui.ConfigFlagsMerged(bc)
	c := &Config{
		BaseConfig:   bc,
		APIConfig:    ac,
		StaticConfig: sc,
	}
	f := bf
	f = append(f, af...)
	f = append(f, sf...)
	return c, f
}

type Config struct {
	*BaseConfig
	*APIConfig
	*StaticConfig
}

type (
	BaseConfig   = serve.Config
	APIConfig    = api.Config
	StaticConfig = ui.Config
)
