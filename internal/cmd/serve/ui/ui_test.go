// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ui_test

import (
	"context"
	"errors"
	"flag"
	"testing"

	"github.com/antichris/go-feedreader/internal/cmd/serve"
	. "github.com/antichris/go-feedreader/internal/cmd/serve/ui"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestComand_Exec(t *testing.T) {
	t.Parallel()
	t.Run("paths", func(t *testing.T) {
		t.Parallel()
		errSnap := errors.New("snap")
		const (
			root          = "/"
			path          = "foo/bar/qux"
			dots          = ".././/.././/"
			rootDots      = root + dots
			rootPath      = root + path
			dotsPath      = dots + path
			rootDotsPath  = root + dotsPath
			external      = "//host"
			externalHTTP  = "http://host"
			externalHTTPS = "https://host"
		)
		for n, tt := range map[string]struct {
			index   string
			static  string
			wIndex  string
			wStatic string
		}{
			"empty": {},
			"root index": {
				index:  root,
				wIndex: "",
			}, "relative index": {
				index:  path,
				wIndex: path,
			}, "dots index": {
				index:  dots,
				wIndex: "",
			}, "root dots index": {
				index:  rootDots,
				wIndex: "",
			}, "absolute index": {
				index:  rootPath,
				wIndex: path,
			}, "dots path index": {
				index:  dotsPath,
				wIndex: path,
			}, "root dots path index": {
				index:  rootDotsPath,
				wIndex: path,
			},
			"root static": {
				static:  root,
				wStatic: root,
			}, "relative static": {
				static:  path,
				wStatic: path,
			}, "dots static": {
				static:  dots,
				wStatic: "",
			}, "root dots static": {
				static:  rootDots,
				wStatic: root,
			}, "absolute static": {
				static:  rootPath,
				wStatic: rootPath,
			}, "dots path static": {
				static:  dotsPath,
				wStatic: path,
			}, "root dots path static": {
				static:  rootDotsPath,
				wStatic: rootPath,
			}, "external static": {
				static:  external,
				wStatic: external,
			}, "external HTTP static": {
				static:  externalHTTP,
				wStatic: externalHTTP,
			}, "external HTTPS static": {
				static:  externalHTTPS,
				wStatic: externalHTTPS,
			},
			// TODO: Add test cases.
		} {
			tt := tt
			t.Run(n, func(t *testing.T) {
				t.Parallel()
				r := require.New(t)
				f := func(ctx context.Context, c *Config) error {
					r.Equal(tt.wStatic, c.StaticPath)
					r.Equal(tt.wIndex, c.IndexPath)
					return errSnap
				}

				c := New(&serve.Config{ServerID: "dev"}, f)

				fs := &flag.FlagSet{}
				c.FillFlagSet(fs)
				r.NoError(fs.Set("static-path", tt.static))
				r.NoError(fs.Set("index-path", tt.index))

				err := c.Exec(ctx, []string{})

				r.ErrorIs(err, errSnap)
			})
		}
	})
	t.Run("origin", func(t *testing.T) {
		t.Parallel()
		serverURL := func(
			t testing.TB, serverID, host, port string, secure bool,
		) string {
			u, err := serve.ServerURL(serverID, host, port, false)
			if err != nil {
				t.Fatalf(`getting %q server URL: %s`, serverID, err)
			}
			return u.String()
		}
		const (
			serverID = "dev"
			host     = "custom.host:12345"
			origin   = "origin"
		)
		for n, tt := range map[string]struct {
			uiOrigin string
			serverID string
			host     string
			want     string
			wErr     string
		}{"bad server ID": {
			wErr: `invalid server ID: ""`,
		}, "server URL": {
			serverID: serverID,
			want:     serverURL(t, serverID, "", "", false),
		}, "server URL custom host": {
			serverID: serverID,
			host:     host,
			want:     serverURL(t, serverID, host, "", false),
		}, "given": {
			uiOrigin: origin,
			want:     origin,
		}} {
			tt := tt
			t.Run(n, func(t *testing.T) {
				t.Parallel()
				r := require.New(t)
				m := testu.NewFuncMock(t, "f")
				f := func(ctx context.Context, c *Config) error {
					m.Called(ctx, c)
					r.Equal(tt.want, c.UIOrigin)
					return nil
				}
				m.On(ctx, mock.Anything)

				c := New(&serve.Config{
					ServerID: tt.serverID,
					Host:     tt.host,
				}, f)

				fs := &flag.FlagSet{}
				c.FillFlagSet(fs)
				r.NoError(fs.Set("ui-origin", tt.uiOrigin))

				err := c.Exec(ctx, []string{})

				if tt.wErr != "" {
					r.EqualError(err, tt.wErr)
					return
				}
				r.NoError(err)
				m.AssertExpectations(t)
			})
		}
	})
}

var ctx = context.Background()
