// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package ui implements a command that starts the Feed Reader web UI
// resource server.
package ui

import (
	"context"
	"path"
	"strings"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/cmd/serve"
	"github.com/antichris/go-feedreader/ui"
)

func New(
	scf *serve.Config,
	fn func(context.Context, *Config) error,
) *cmd.Command {
	c, f := ConfigFlagsMerged(scf)
	return &cmd.Command{
		Name:      "ui",
		ShortDesc: "Serve Feed Reader web UI resources",
		LongDesc: "Start serving the web UI resources of the Feed Reader" +
			" application.",
		Exec: func(ctx context.Context, args []string) (err error) {
			if len(c.UIOrigin) == 0 {
				u, err := c.URL()
				if err != nil {
					return err
				}
				c.UIOrigin = u.String()
			}
			PrepPaths(c)
			return fn(ctx, c)
		},
		Flags: f,
	}
}

func ConfigFlagsMerged(scf *ServerConfig) (*Config, []*cmd.Flag) {
	c, f := ConfigFlags()
	c.ServerConfig = scf
	return c, f
}

func ConfigFlags() (*Config, []*cmd.Flag) {
	c := &Config{
		UIConfig: UIConfig{
			StaticPath: "static",
			// FIXME APIURL must depend on API server.
			APIURL: "http://feedreader.localhost:8080/api",
		},
	}
	f := []*cmd.Flag{{
		Name:        "files-only",
		Description: "Only serve the static resource files",
		Pointer:     &c.FilesOnly,
		Shorthand:   "f",
	}, {
		Name:        "index-only",
		Description: "Only serve the index (entrypoint) page",
		Pointer:     &c.IndexOnly,
		Shorthand:   "i",
	}, {
		Name:        "static-path",
		Description: "The URL path prefix for the static resources",
		Pointer:     &c.StaticPath,
		// TODO It would be nice to be able to use this shorthand.
		// Shorthand:   "s",
	}, {
		Name:        "index-path",
		Description: "The URL path prefix of the index (entrypoint) page",
		Pointer:     &c.IndexPath,
	}, {
		Name:        "api-url",
		Description: "The URL prefix for all REST API calls",
		Pointer:     &c.APIURL,
	}, {
		Name:        "ui-origin",
		Description: "Index URL, the origin of static resource requests",
		Pointer:     &c.UIOrigin,
	}}
	return c, f
}

func PrepPaths(c *Config) {
	c.IndexPath = path.Clean("/" + c.IndexPath)[1:]
	if isExternal(c.StaticPath) {
		return
	}
	if path.IsAbs(c.StaticPath) {
		c.StaticPath = path.Clean(c.StaticPath)
	} else {
		c.StaticPath = path.Clean("/" + c.StaticPath)[1:]
	}
}

func isExternal(s string) bool {
	for _, v := range []string{"//", "https://", "http://"} {
		if strings.HasPrefix(s, v) {
			return true
		}
	}
	return false
}

// Config for the subcommand.
type Config struct {
	UIConfig
	*ServerConfig
}

type (
	UIConfig     = ui.Config
	ServerConfig = serve.Config
)
