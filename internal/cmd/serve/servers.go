// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package serve

import (
	"fmt"
	"net"
	"net/url"
	"sort"
	"strings"
)

var servers = map[string]string{
	"dev":  "http://feedreader.localhost:8080",
	"devs": "https://feedreader.localhost:4430",
	"prod": "https://feedreader.localhost",
}

func ServerURL(serverID, host, port string, secure bool) (u *url.URL, err error) {
	addr, ok := servers[serverID]
	if !ok {
		err = invalidServerIDErr(serverID)
		return
	}
	u, err = prepURL(addr, host, port, secure)
	if err != nil {
		// TODO Redesign this untestable spot.
		// As it stands, the only way an error could happen here is if
		// [servers] had an unparsable entry.
		err = fmt.Errorf("invalid URL %#v: %w", addr, err)
	}
	return
}

type invalidServerIDErr string

func (e invalidServerIDErr) Error() string {
	return fmt.Sprintf("invalid server ID: %q", string(e))
}

func (e invalidServerIDErr) ErrorHint() string {
	return fmt.Sprintf("%s (valid servers: %s)", e, strings.Join(keys(servers), "|"))
}

func prepURL(base, host, port string, secure bool) (u *url.URL, err error) {
	u, err = url.Parse(base)
	if err != nil {
		return
	}
	if secure {
		u.Scheme = "https"
	}
	if host != "" {
		u.Host = host
	}
	if port != "" {
		u.Host = net.JoinHostPort(u.Hostname(), port)
	} else if u.Port() == "" {
		u.Host = net.JoinHostPort(u.Host, "80")
	}
	return
}

func keys(m map[string]string) []string {
	ks := make([]string, len(m))
	i := 0
	for k := range m {
		ks[i] = k
		i++
	}
	sort.Slice(ks, func(i, j int) bool {
		return ks[i] < ks[j]
	})
	return ks
}
