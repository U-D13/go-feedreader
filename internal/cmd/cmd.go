// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd

import (
	"context"
	"fmt"
)

// Command describes a command.
type Command struct {
	Name        string
	ShortDesc   string // appears in Command listings
	LongDesc    string // appears in help for this Command
	Exec        func(ctx context.Context, args []string) (err error)
	EnvPrefix   string
	Flags       []*Flag
	Subcommands []*Command
}

// FillFlagSet registers the flags of c into fs.
func (c Command) FillFlagSet(fs FlagSet) {
	for _, f := range c.Flags {
		f.AddToFlagSet(fs)
	}
}

func LookupFlag(s []*Flag, name string) (f *Flag, ok bool) {
	for _, v := range s {
		if ok = v.Name == name; ok {
			f = v
			return
		}
	}
	return
}

// Flag describes a CLI flag.
type Flag struct {
	Name        string
	Description string
	// Pointer to receive the flag value, determines the data type.
	// Its target may be preloaded with the default value.
	Pointer     any
	Shorthand   string
	PreferShort bool
}

// TODO Ability to set default flag value dynamically.
// The UI needs the URL of the API server, and the API server needs the
// URL of the frontend for links in email messages.

// AddToFlagSet adds f to fs as *Var flag of the type pointed to by
// f.Pointer.
func (f Flag) AddToFlagSet(fs FlagSet) {
	if f.Pointer == nil {
		panic("flag value pointer must be set")
	}
	var (
		n = f.Name
		d = f.Description
	)
	if f.PreferShort {
		n = f.Shorthand
	}
	switch p := f.Pointer.(type) {
	case *string:
		if p == nil {
			panic(errNilPtr(p))
		}
		fs.StringVar(p, n, *p, d)
	case *bool:
		if p == nil {
			panic(errNilPtr(p))
		}
		fs.BoolVar(p, n, *p, d)
	case *int:
		if p == nil {
			panic(errNilPtr(p))
		}
		fs.IntVar(p, n, *p, d)
	default:
		panic(fmt.Sprintf("unsupported flag value type: %T", p))
	}
}

// FlagSet is used to inject flags and is compatible with (by covering a
// fraction of) the standard library `flag.FlagSet`.
type FlagSet interface {
	StringVar(p *string, name string, value string, usage string)
	BoolVar(p *bool, name string, value bool, usage string)
	IntVar(p *int, name string, value int, usage string)
}

func errNilPtr(p any) string {
	return fmt.Sprintf("flag value pointer must not be nil: %#v", p)
}
