// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd

import (
	"context"
	"io"
)

// FrontendFactory creates CLI application Frontend instances.
type FrontendFactory interface {
	// Create an instance of a CLI application Frontend.
	Create(cm *Command, cf *Config) Frontend
}

// Frontend for a CLI application.
type Frontend interface {
	// Exec the CLI application.
	Exec(ctx context.Context, args []string) (exitCode int)
	// SetInput sets the input stream (e.g. os.Stdin).
	SetInput(r io.Reader)
	// SetOutput sets the output stream (e.g. os.Stdin).
	SetOutput(w io.Writer)
	// SetErrOutput sets the error output stream (e.g. os.Stderr).
	SetErrOutput(w io.Writer)
}

// FrontendFactoryFunc is a function that implements FrontendFactory.
type FrontendFactoryFunc func(cm *Command, cf *Config) Frontend

// Create an instance of a CLI application Frontend.
func (f FrontendFactoryFunc) Create(cm *Command, cf *Config) Frontend {
	return f(cm, cf)
}
