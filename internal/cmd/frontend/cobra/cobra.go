// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cobra

import (
	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// NewRoot [*cobra.Command].
func NewRoot(cm *cmd.Command, cf *cmd.Config, v *viper.Viper) *cobra.Command {
	c := MapCommand(cm)

	// cobra.OnInitialize(func() {
	// 	initConfig(cf, v)
	// })

	v.SetEnvPrefix(cm.EnvPrefix)

	fs := c.PersistentFlags()
	// fs.Lookup(cmd.FlagCFGFile).Usage += " (default is $HOME/.feedreader.yaml)"
	err := v.BindPFlags(fs)
	if err != nil {
		panic(err)
	}

	return c
}

// TODO Config file support.

// func initConfig(c *cmd.Config, v *viper.Viper) {
// 	// if c.CFGFile != "" {
// 	// 	// Use config file from the flag.
// 	// 	v.SetConfigFile(c.CFGFile)
// 	// } else {
// 	// 	// Find home directory.
// 	// 	home, err := os.UserHomeDir()
// 	// 	cobra.CheckErr(err)

// 	// 	v.AddConfigPath(home)
// 	// 	v.SetConfigName(".feedreader")
// 	// 	v.SetConfigType("yaml")
// 	// }

// 	v.AutomaticEnv()

// 	if err := v.ReadInConfig(); err == nil {
// 		// TODO Use a designated writer/logger for output.
// 		fmt.Println("Using config file:", v.ConfigFileUsed())
// 	}
// }

// MapCommand maps a [*cmd.Command] to a [*cobra.Command].
func MapCommand(src *cmd.Command) *cobra.Command {
	dst := &cobra.Command{
		Use:   src.Name,
		Short: src.ShortDesc,
		Long:  src.LongDesc,
	}
	if src.Exec != nil {
		dst.RunE = func(cc *cobra.Command, args []string) error {
			cc.SilenceErrors = true
			cc.SilenceUsage = true
			return cmdError{src.Exec(cc.Context(), args)}
		}
	}
	AddFlags(dst, src)
	AddSubcommands(dst, src)

	return dst
}

// AddFlags to dst.
func AddFlags(dst *cobra.Command, src *cmd.Command) {
	if len(src.Flags) == 0 {
		return
	}
	fs := dst.PersistentFlags()
	src.FillFlagSet(fs)
	AdjustFlags(fs, src)
}

// AddSubcommands to dst.
func AddSubcommands(dst *cobra.Command, src *cmd.Command) {
	for _, c := range src.Subcommands {
		dst.AddCommand(MapCommand(c))
	}
}

// AdjustFlags to have both shorthand and full name.
//
// This works around [cmd.Command.FillFlagSet] being geared towards the
// Go standard library flag package, that has no support for flag
// shorthands.
// As a consequence, [cmd.Flag]'s that prefer shorthand can only be
// identified in a [cmd.FlagSet] by that, thus losing the full name.
// Since [pflag.Flag]'s support both shorthand and full names, this
// restores the latter by iterating through [cmd.Command.Flags].
func AdjustFlags(s *pflag.FlagSet, c *cmd.Command) {
	for _, f := range c.Flags {
		n := f.Name
		if f.PreferShort {
			n = f.Shorthand
		}
		ff := s.Lookup(n)
		if f.PreferShort {
			ff.Name = f.Name
		}
		ff.Shorthand = f.Shorthand
	}
}

// isFrontendError returns whether an error returned by `RunE` of a
// mapped command has originated in the CLI frontend.
// Returns false, if the error comes from the mapped `Exec`.
func isFrontendError(err error) bool {
	_, ok := err.(cmdError)
	return !ok
}

// cmdError wraps a non-Cobra error.
type cmdError struct{ error }

// Unwrap returns the underlying error.
func (e cmdError) Unwrap() error { return e.error }
