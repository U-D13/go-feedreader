// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cobra

import (
	"context"
	"io"
	"testing"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/cmd/testdata"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/antichris/go-feedreader/terror"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/stretchr/testify/require"
)

func TestMapCommand(t *testing.T) {
	t.Parallel()
	t.Run("subcommands", func(t *testing.T) {
		t.Parallel()
		v := "value"
		c := testdata.WithSubcommands(*testdata.Command("", ""))
		c.Subcommands[0].Flags = []*cmd.Flag{testdata.StringFlag("", "", &v)}

		want := &cobra.Command{
			Use:   c.Name,
			Short: c.ShortDesc,
			Long:  c.LongDesc,
		}
		for _, sc := range c.Subcommands {
			cc := &cobra.Command{
				Use:   sc.Name,
				Short: sc.ShortDesc,
				Long:  sc.LongDesc,
			}
			if len(sc.Flags) != 0 {
				fs := cc.PersistentFlags()
				for _, f := range sc.Flags {
					fs.StringVarP(&v, f.Name, f.Shorthand, v, f.Description)
				}
			}
			want.AddCommand(cc)
		}

		got := MapCommand(c)

		require.Equal(t, want, got)
	})
	t.Run("exec", func(t *testing.T) {
		t.Parallel()
		var (
			ctx  = context.Background()
			args = []string{"foo", "bar"}
			want = terror.Error("snap")

			c = testdata.Command("", "")
		)
		for n, tt := range map[string]struct {
			args   []string
			wFEErr bool
		}{"frontend err": {
			args:   append(args, "--qux"),
			wFEErr: true,
		}, "nominal": {
			args: args,
		}} {
			t.Run(n, func(t *testing.T) {
				m := testu.NewFuncMock(t, "Exec")
				if !tt.wFEErr {
					c.Exec = func(ctx context.Context, args []string) (err error) {
						return m.Called(ctx, args).Error(0)
					}
					m.On(ctx, args).
						Return(want)
				}

				cc := MapCommand(c)
				cc.SetOut(io.Discard)
				cc.SetErr(io.Discard)
				cc.SetArgs(tt.args)

				got := cc.ExecuteContext(ctx)

				r := require.New(t)
				r.Equal(tt.wFEErr, isFrontendError(got))
				if tt.wFEErr {
					r.Error(got)
				} else {
					r.ErrorIs(got, want)
				}
				m.AssertExpectations(t)
			})
		}
	})
}

func TestAdjustFlags(t *testing.T) {
	t.Parallel()
	var (
		v     = true
		name  = "flag-1"
		short = "a"
	)
	pflag.BoolVarP(&v, name, short, v, "")
	want := pflag.Lookup(name)

	c := testdata.Command("", "")
	c.Flags = []*cmd.Flag{{
		Name:        name,
		Pointer:     &v,
		Shorthand:   short,
		PreferShort: true,
	}}
	fs := &pflag.FlagSet{}
	c.FillFlagSet(fs)

	got := gatherPflags(fs, len(c.Flags))[0]

	r := require.New(t)
	r.NotEqual(want, got)

	AdjustFlags(fs, c)

	r.Equal(want, got)
}

func gatherPflags(fs *pflag.FlagSet, cap int) []*pflag.Flag {
	s := make([]*pflag.Flag, 0, cap)
	fs.VisitAll(func(f *pflag.Flag) {
		s = append(s, f)
	})
	return s
}
