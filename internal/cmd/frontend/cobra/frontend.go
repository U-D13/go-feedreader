// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cobra

import (
	"context"
	"io"
	"log"
	"os"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// New Frontend.
func New(
	command *cmd.Command,
	config *cmd.Config,
) cmd.Frontend {
	v := viper.New()
	return FactoryFuncWithViper(v).Create(command, config)
}

func FactoryFuncWithViper(v *viper.Viper) cmd.FrontendFactoryFunc {
	return func(command *cmd.Command, config *cmd.Config) cmd.Frontend {
		c := NewRoot(command, config, v)

		// FIXME Don't use os.Stderr directly.
		l := log.New(os.Stderr, command.Name+": ", 0)

		return &Frontend{c, l}
	}
}

var _ cmd.Frontend = (*Frontend)(nil)

// Frontend implements cmd.Frontend.
type Frontend struct {
	*cobra.Command
	*log.Logger
}

// Exec the CLI application.
func (f *Frontend) Exec(ctx context.Context, args []string) (exitCode int) {
	if err := f.ExecuteContext(ctx); err != nil {
		// TODO Map errors to status codes.
		if isFrontendError(err) {
			return 2
		}
		f.Logger.Printf("%v", err)
		exitCode = 1
	}
	return
}

// SetInput sets the input stream (e.g. os.Stdin).
func (f *Frontend) SetInput(r io.Reader) {
	f.SetIn(r)
}

// SetOutput sets the output stream (e.g. os.Stdin).
func (f *Frontend) SetOutput(w io.Writer) {
	f.SetOut(w)
}

// SetErrOutput sets the error output stream (e.g. os.Stderr).
func (f *Frontend) SetErrOutput(w io.Writer) {
	f.SetErr(w)
	f.Logger.SetOutput(w)
}
