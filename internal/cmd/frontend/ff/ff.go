// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ff

import (
	"context"
	"flag"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/peterbourgon/ff/v3"
	"github.com/peterbourgon/ff/v3/ffcli"
)

// NewRoot ffcli.Command.
func NewRoot(command *cmd.Command) *ffcli.Command {
	cm := MapCommand(command, "")
	cm.ShortUsage += " <subcommand> [flags] [<arg>...]"

	cm.Exec = func(context.Context, []string) error {
		return flag.ErrHelp
	}
	cm.Options = []ff.Option{
		ff.WithEnvVarPrefix(command.EnvPrefix),
		ff.WithConfigFileFlag(cmd.FlagCFGFile),
		ff.WithConfigFileParser(ff.PlainParser),
		ff.WithAllowMissingConfigFile(true),
		ff.WithIgnoreUndefined(true),
	}

	return cm
}

// SubFlagSetName returns the name for FlagSet of subcommand.
func SubFlagSetName(parentName, commandName string) string {
	if parentName == "" {
		return commandName
	}
	if commandName == "" {
		return parentName
	}
	return parentName + " " + commandName
}

// MapCommand maps a cmd.Command to an ffcli.Command.
func MapCommand(command *cmd.Command, parentName string) *ffcli.Command {
	cm := &ffcli.Command{
		Name:      command.Name,
		ShortHelp: command.ShortDesc,
		LongHelp:  command.LongDesc,
	}
	if command.Exec != nil {
		cm.Exec = func(ctx context.Context, args []string) error {
			return command.Exec(ctx, args)
		}
	}
	AddFlags(cm, command, parentName)
	AddSubcommands(cm, command.Subcommands)

	return cm
}

// AddFlags to dst.
func AddFlags(dst *ffcli.Command, src *cmd.Command, parentName string) {
	fsName := SubFlagSetName(parentName, dst.Name)
	f := flag.NewFlagSet(fsName, flag.ExitOnError)
	src.FillFlagSet(f)
	dst.FlagSet = f
	dst.ShortUsage = fsName + " [flags]"
}

// AddSubcommands to dst.
func AddSubcommands(dst *ffcli.Command, src []*cmd.Command) {
	s := dst.Subcommands
	if cap(s) == 0 {
		s = make([]*ffcli.Command, 0, len(src))
	}
	for _, c := range src {
		s = append(s, MapCommand(c, dst.Name))
	}
	dst.Subcommands = s
}
