// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ff

import (
	"context"
	"io"
	"log"
	"os"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/peterbourgon/ff/v3/ffcli"
)

// New Frontend.
func New(command *cmd.Command, _ *cmd.Config) cmd.Frontend {
	c := NewRoot(command)

	// FIXME Don't use os.Stderr directly.
	l := log.New(os.Stderr, command.Name+": ", 0)

	return &Frontend{c, l}
}

var _ cmd.Frontend = (*Frontend)(nil)

// Frontend implements cmd.Frontend.
type Frontend struct {
	*ffcli.Command
	*log.Logger
}

// Exec the CLI application.
func (f *Frontend) Exec(ctx context.Context, args []string) (exitCode int) {
	if err := f.Parse(args[1:]); err != nil {
		f.Printf("error during Parse: %v", err)
		return 1
	}
	if err := f.Run(ctx); err != nil {
		f.Printf("%v", err)
		return 1
	}
	return 0
}

// SetInput sets the input stream (e.g. os.Stdin).
func (f *Frontend) SetInput(r io.Reader) {
	panic("not implemented") // TODO: Implement
}

// SetOutput sets the output stream (e.g. os.Stdin).
func (f *Frontend) SetOutput(w io.Writer) {
	// panic("not implemented") // TODO: Implement
}

// SetErrOutput sets the error output stream (e.g. os.Stderr).
func (f *Frontend) SetErrOutput(w io.Writer) {
	// panic("not implemented") // TODO: Implement
}
