// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package ff_test

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"testing"

	"github.com/antichris/go-feedreader/internal/cmd"
	. "github.com/antichris/go-feedreader/internal/cmd/frontend/ff"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/peterbourgon/ff/v3/ffcli"
	"github.com/stretchr/testify/require"
)

func TestNewRoot(t *testing.T) {
	t.Parallel()
	cm := &cmd.Command{
		Subcommands: []*cmd.Command{{
			Name: "foo",
		}},
	}
	gotCm := NewRoot(cm)

	t.Run("subcommands", func(t *testing.T) {
		t.Parallel()
		NewRequire(t).Subcommands(gotCm, cm)
	})
	t.Run("ShortUsage", func(t *testing.T) {
		t.Parallel()
		require.Contains(t, gotCm.ShortUsage, "<subcommand> [flags] [<arg>...]")
	})
	t.Run("exec ErrHelp", func(t *testing.T) {
		t.Parallel()
		gotErr := gotCm.Exec(context.Background(), []string{})
		require.ErrorIs(t, gotErr, flag.ErrHelp)
	})

	// TODO Test whether Options have been set correctly.
}

func TestSubFlagSetName(t *testing.T) {
	t.Parallel()
	for _, tt := range []struct {
		parent  string
		command string
		want    string
	}{
		{},
		{"", "bar", "bar"},
		{"foo", "", "foo"},
		{"foo", "bar", "foo bar"},
	} {
		tt := tt
		name := fmt.Sprintf("parent=%q/command=%q", tt.parent, tt.command)
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			got := SubFlagSetName(tt.parent, tt.command)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMapCommand(t *testing.T) {
	t.Parallel()
	var s string
	cm := &cmd.Command{
		Name:      "foo",
		ShortDesc: "short",
		LongDesc:  "long",
		Flags: []*cmd.Flag{{
			Name:    "bar",
			Pointer: &s,
		}},
		Subcommands: []*cmd.Command{{
			Name: "qux",
		}},
	}
	got := MapCommand(cm, "")

	t.Run("raw data", func(t *testing.T) {
		t.Parallel()
		NewRequire(t).CommandDataMapped(got, cm)
	})
	t.Run("exec", func(t *testing.T) {
		t.Parallel()
		wCtx := context.Background()
		wArgs := []string{"foo", "bar"}
		wErr := errors.New("qux")

		m := testu.NewFuncMock(t, "Exec")
		cm.Exec = func(ctx context.Context, args []string) (err error) {
			return m.Called(ctx, args).Error(0)
		}
		m.On(wCtx, wArgs).
			Return(wErr)

		got := MapCommand(cm, "")
		err := got.Exec(wCtx, wArgs)

		m.AssertExpectations(t)
		require.ErrorIs(t, err, wErr)
	})
	t.Run("flags", func(t *testing.T) {
		t.Parallel()
		fs := got.FlagSet
		f := fs.Lookup(cm.Flags[0].Name)
		r := require.New(t)
		r.Equal(cm.Name, fs.Name())
		r.NotNil(f)
	})
	t.Run("subcommands", func(t *testing.T) {
		t.Parallel()
		NewRequire(t).Subcommands(got, cm)
	})
}

func TestAddFlags(t *testing.T) {
	t.Parallel()
	parentName := "foo"
	src := &cmd.Command{}
	dst := &ffcli.Command{
		Name: "bar",
	}
	wFSName := "foo bar"
	AddFlags(dst, src, parentName)
	fs := dst.FlagSet

	require.Equal(t, wFSName, fs.Name())
}

// Utilities.

func NewRequire(t *testing.T) *reqAsserts {
	return &reqAsserts{t, require.New(t)}
}

type reqAsserts struct {
	t *testing.T
	*require.Assertions
}

func (req *reqAsserts) Subcommands(
	dst *ffcli.Command,
	src *cmd.Command,
) {
	req.t.Helper()
	ss := src.Subcommands
	ds := dst.Subcommands
	req.Len(ds, len(ss))
	for i := range ds {
		req.CommandDataMapped(ds[i], ss[i])
	}
}

func (req *reqAsserts) CommandDataMapped(
	dst *ffcli.Command,
	src *cmd.Command,
) {
	req.t.Helper()
	req.Equal(dst.Name, src.Name)
	req.Equal(dst.ShortHelp, src.ShortDesc)
	req.Equal(dst.LongHelp, src.LongDesc)
}
