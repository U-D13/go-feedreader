// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cli

import (
	"context"
	"io"
	"log"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/urfave/cli/v2"
)

// New Frontend.
func New(command *cmd.Command, _ *cmd.Config) cmd.Frontend {
	a := NewRoot(command)

	l := log.New(a.ErrWriter, command.Name+": ", 0)

	return &Frontend{a, l}
}

var _ cmd.Frontend = (*Frontend)(nil)

// Frontend implements cmd.Frontend.
type Frontend struct {
	*cli.App
	*log.Logger
}

// Exec the CLI application.
func (f *Frontend) Exec(ctx context.Context, args []string) (exitCode int) {
	if err := f.RunContext(ctx, args); err != nil {
		f.Printf("%v", err)
		return 1
	}
	return 0
}

// SetInput sets the input stream (e.g. os.Stdin).
func (f *Frontend) SetInput(r io.Reader) {
	f.Reader = r
}

// SetOutput sets the output stream (e.g. os.Stdin).
func (f *Frontend) SetOutput(w io.Writer) {
	f.App.Writer = w
}

// SetErrOutput sets the error output stream (e.g. os.Stderr).
func (f *Frontend) SetErrOutput(w io.Writer) {
	f.ErrWriter = w
	f.Logger.SetOutput(w)
}
