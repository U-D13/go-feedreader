// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cli

import (
	"fmt"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/urfave/cli/v2"
)

// NewRoot app instance.
func NewRoot(c *cmd.Command) *cli.App {
	app := &cli.App{
		Name:        c.Name,
		Usage:       c.ShortDesc,
		Description: c.LongDesc,
	}
	addAppFlags(app, c.Flags)
	AddAppCommands(app, c.Subcommands)
	// TODO Environment variable stuff.

	return app
}

func addAppFlags(dst *cli.App, src []*cmd.Flag) {
	l := len(src)
	if l == 0 {
		return
	}
	f := dst.Flags
	if cap(f) == 0 {
		f = make([]cli.Flag, 0, l)
	}
	dst.Flags = AppendFlags(f, src)
}

// MapCommand maps a cmd.Command to a cli.Command.
func MapCommand(command *cmd.Command) *cli.Command {
	cm := &cli.Command{
		Name:        command.Name,
		Usage:       command.ShortDesc,
		Description: command.LongDesc,
	}
	if command.Exec != nil {
		cm.Action = func(ctx *cli.Context) error {
			return command.Exec(ctx.Context, ctx.Args().Slice())
		}
	}
	AddFlags(cm, command.Flags)
	AddSubcommands(cm, command.Subcommands)

	return cm
}

// AddFlags to dst.
func AddFlags(dst *cli.Command, src []*cmd.Flag) {
	l := len(src)
	if l == 0 {
		return
	}
	f := dst.Flags
	if cap(f) == 0 {
		f = make([]cli.Flag, 0, l)
	}
	dst.Flags = AppendFlags(f, src)
}

// AppendFlags from c to s and return the new slice.
func AppendFlags(s []cli.Flag, src []*cmd.Flag) []cli.Flag {
	for _, f := range src {
		switch p := f.Pointer.(type) {
		case *string:
			ff := cli.StringFlag{
				Name:        f.Name,
				Usage:       f.Description,
				Value:       *p,
				Destination: p,
			}
			if f.Shorthand != "" {
				ff.Aliases = []string{f.Shorthand}
			}
			s = append(s, &ff)
		case *bool:
			ff := cli.BoolFlag{
				Name:        f.Name,
				Usage:       f.Description,
				Value:       *p,
				Destination: p,
			}
			if f.Shorthand != "" {
				ff.Aliases = []string{f.Shorthand}
			}
			s = append(s, &ff)
		default:
			panic(fmt.Sprintf("unsupported flag value type: %T", p))
		}
	}
	return s
}

// AddAppCommands to dst.
func AddAppCommands(dst *cli.App, src []*cmd.Command) {
	dst.Commands = addSubcommands(dst.Commands, src)
}

// AddSubcommands to dst.
func AddSubcommands(dst *cli.Command, src []*cmd.Command) {
	dst.Subcommands = addSubcommands(dst.Subcommands, src)
}

func addSubcommands(s []*cli.Command, src []*cmd.Command) []*cli.Command {
	l := len(src)
	if l == 0 {
		return s
	}
	if cap(s) == 0 {
		s = make([]*cli.Command, 0, l)
	}
	for _, c := range src {
		s = append(s, MapCommand(c))
	}
	return s
}
