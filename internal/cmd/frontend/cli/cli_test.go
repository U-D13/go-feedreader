// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cli

import (
	"context"
	"flag"
	"testing"

	"github.com/antichris/go-feedreader/internal/cmd"
	"github.com/antichris/go-feedreader/internal/cmd/testdata"
	"github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/antichris/go-feedreader/terror"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"
)

func TestNewRoot(t *testing.T) {
	t.Parallel()
	var (
		sv = "value"
		bv = true
		c  = testdata.Command("", "")
		sc = testdata.Command("1", "sub-")
		sf = testdata.StringFlag("1", "", &sv)
		bf = testdata.BoolFlag("2", "", &bv)
	)
	sf.Shorthand = "a"
	bf.Shorthand = "b"
	var (
		want = &cli.App{
			Name:        c.Name,
			Usage:       c.ShortDesc,
			Description: c.LongDesc,
		}
		wantSub = &cli.Command{
			Name:        sc.Name,
			Usage:       sc.ShortDesc,
			Description: sc.LongDesc,
		}
		wSF = &cli.StringFlag{
			Name:        sf.Name,
			Usage:       sf.Description,
			Value:       sv,
			Destination: &sv,
			Aliases:     []string{sf.Shorthand},
		}
		wBF = &cli.BoolFlag{
			Name:        bf.Name,
			Usage:       bf.Description,
			Value:       bv,
			Destination: &bv,
			Aliases:     []string{bf.Shorthand},
		}
	)
	for n, tt := range map[string]struct {
		c    *cmd.Command
		want *cli.App
	}{"simple": {
		c:    c,
		want: want,
	}, "app flags": {
		c: func(c cmd.Command) *cmd.Command {
			c.Flags = []*cmd.Flag{sf, bf}
			return &c
		}(*c),
		want: func(c cli.App) *cli.App {
			c.Flags = []cli.Flag{wSF, wBF}
			return &c
		}(*want),
	}, "subcommand": {
		c: func(c cmd.Command) *cmd.Command {
			c.Subcommands = []*cmd.Command{sc}
			return &c
		}(*c),
		want: func(c cli.App) *cli.App {
			c.Commands = []*cli.Command{wantSub}
			return &c
		}(*want),
	}, "subcommand flags": {
		c: func(c cmd.Command) *cmd.Command {
			s := *sc
			s.Flags = []*cmd.Flag{sf}
			c.Subcommands = []*cmd.Command{&s}
			return &c
		}(*c),
		want: func(c cli.App) *cli.App {
			s := *wantSub
			s.Flags = []cli.Flag{wSF}
			c.Commands = []*cli.Command{&s}
			return &c
		}(*want),
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := NewRoot(tt.c)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMapCommand(t *testing.T) {
	t.Parallel()
	var (
		ctx  = context.Background()
		args = []string{"foo", "bar"}
		want = terror.Error("snap")

		c = testdata.Command("", "")
		m = testu.NewFuncMock(t, "Exec")
	)
	c.Exec = func(ctx context.Context, args []string) (err error) {
		return m.Called(ctx, args).Error(0)
	}
	m.On(ctx, args).
		Return(want)
	var (
		fs   = &flag.FlagSet{}
		cCtx = cli.NewContext(&cli.App{}, fs, nil)
	)

	cc := MapCommand(c)

	r := require.New(t)
	r.NoError(fs.Parse(args))

	got := cc.Action(cCtx)

	r.ErrorIs(want, got)
	m.AssertExpectations(t)
}
