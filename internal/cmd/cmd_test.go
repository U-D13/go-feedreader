// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd_test

import (
	"flag"
	"testing"

	. "github.com/antichris/go-feedreader/internal/cmd"
	"github.com/stretchr/testify/require"
)

func TestCommand_FillFlagSet(t *testing.T) {
	t.Parallel()
	var (
		s = "string"
		// XXX Reusing the same pointer causes races in parallel tests.
		s2 = "string2"
	)
	for n, tt := range map[string]struct {
		Command
		wCount int
		wPanic string
	}{
		"flag without value pointer": {
			Command: Command{Flags: []*Flag{{}}},
			wPanic:  "flag value pointer must be set",
		},
		"no flags": {
			Command: Command{},
		},
		"one flag": {
			Command: Command{Flags: []*Flag{{Pointer: &s}}},
			wCount:  1,
		},
		"two flags": {
			Command: Command{Flags: []*Flag{
				{Name: "1", Pointer: &s2},
				{Name: "2", Pointer: &s2},
			}},
			wCount: 2,
		},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			fs := newFlagSet()

			run := func() { tt.FillFlagSet(fs) }

			r := require.New(t)
			if tt.wPanic != "" {
				r.PanicsWithValue(tt.wPanic, run)
				return
			}
			r.NotPanics(run)

			var count int
			fs.VisitAll(func(f *flag.Flag) { count++ })
			r.Equal(tt.wCount, count)
			// TODO Verify flag data.
		})
	}
}

func TestLookupFlag(t *testing.T) {
	t.Parallel()
	const name = "foo"
	var (
		flag  = &Flag{Name: name}
		flags = []*Flag{{}, {Name: "bar"}, flag}
	)
	for n, tt := range map[string]struct {
		name string
		want *Flag
		wOk  bool
	}{
		"failure": {wOk: false, name: "qux"},
		"success": {wOk: true, name: name, want: flag},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()

			got, ok := LookupFlag(flags, tt.name)

			r := require.New(t)
			r.Equal(tt.wOk, ok)
			r.Equal(tt.want, got)
		})
	}
}

func TestFlag_AddToFlagSet(t *testing.T) {
	t.Parallel()
	const (
		d = "description"
		n = "name"
	)
	var (
		nb *bool
		ni *int
		ns *string
		b  = true
		i  = 21
		s  = "string"
		// XXX Reusing the same pointer causes races in parallel tests.
		s2 = "string2"
	)
	newStringFlag := func(p *string, n string, d string) *flag.Flag {
		fs := newFlagSet()
		fs.StringVar(p, n, *p, d)
		return fs.Lookup(n)
	}
	for n, tt := range map[string]struct {
		Flag
		wFlag  *flag.Flag
		wPanic string
	}{
		"no value pointer": {
			wPanic: "flag value pointer must be set",
		},
		"unsupported pointer": {
			Flag:   Flag{Pointer: (*Flag)(nil)},
			wPanic: "unsupported flag value type: *cmd.Flag",
		},
		"nil bool pointer": {
			Flag:   Flag{Pointer: nb},
			wPanic: XerrNilPtr(nb),
		},
		"nil int pointer": {
			Flag:   Flag{Pointer: ni},
			wPanic: XerrNilPtr(ni),
		},
		"nil string pointer": {
			Flag:   Flag{Pointer: ns},
			wPanic: XerrNilPtr(ns),
		},
		"bool flag": {
			Flag: Flag{
				Name:        n,
				Description: d,
				Pointer:     &b,
			},
			wFlag: func() *flag.Flag {
				fs := newFlagSet()
				fs.BoolVar(&b, n, b, d)
				return fs.Lookup(n)
			}(),
		},
		"int flag": {
			Flag: Flag{
				Name:        n,
				Description: d,
				Pointer:     &i,
			},
			wFlag: func() *flag.Flag {
				fs := newFlagSet()
				fs.IntVar(&i, n, i, d)
				return fs.Lookup(n)
			}(),
		},
		"string flag": {
			Flag: Flag{
				Name:        n,
				Description: d,
				Pointer:     &s,
			},
			wFlag: newStringFlag(&s, n, d),
		},
		"string prefer short flag": {
			Flag: Flag{
				Description: d,
				Shorthand:   n,
				PreferShort: true,
				Pointer:     &s2,
			},
			wFlag: newStringFlag(&s2, n, d),
		},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			fs := newFlagSet()

			run := func() { tt.AddToFlagSet(fs) }

			r := require.New(t)
			if tt.wPanic != "" {
				r.PanicsWithValue(tt.wPanic, run)
				return
			}
			r.NotPanics(run)

			r.NotNil(tt.wFlag, "wFlag must be set")

			f := fs.Lookup(tt.Name)
			if tt.PreferShort {
				f = fs.Lookup(tt.Shorthand)
			}
			r.Equal(tt.wFlag.Name, f.Name)
			r.Equal(tt.wFlag.Usage, f.Usage)
			r.Equal(tt.wFlag.Value, f.Value)
		})
	}
}

func newFlagSet() *flag.FlagSet {
	return flag.NewFlagSet("", flag.ContinueOnError)
}
