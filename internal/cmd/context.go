// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmd

import "context"

// FIXME Seriosuly, do we even really need the following?

// GetCommand from ctx.
func GetCommand(ctx context.Context) (c *Command, ok bool) {
	v := ctx.Value(commandCTXKey)
	if v != nil {
		c, ok = v.(*Command)
	}
	return
}

// WithCommand returns copy of parent with c.
func WithCommand(parent context.Context, c *Command) context.Context {
	return context.WithValue(parent, commandCTXKey, c)
}

type commandCTXKeyT struct{}

var commandCTXKey commandCTXKeyT
