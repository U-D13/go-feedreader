// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package http_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/antichris/go-feedreader/internal/util/http"
	"github.com/stretchr/testify/require"
)

func TestError(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		code   int
		want   string
		wPanic string
	}{"invalid": {
		wPanic: "invalid WriteHeader code 0",
	}, "Internal Server Error": {
		code: 500,
		want: "500 Internal Server Error\n",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			w := httptest.NewRecorder()

			run := func() { Error(w, tt.code) }

			r := require.New(t)
			if tt.wPanic != "" {
				r.PanicsWithValue(tt.wPanic, run)
				return
			}
			r.NotPanics(run)
			r.Equal(tt.want, w.Body.String())
		})
	}
}

func TestLeanError(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		code   int
		wPanic string
	}{"invalid": {
		wPanic: "invalid WriteHeader code 0",
	}, "Internal Server Error": {
		code: 500,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			w := httptest.NewRecorder()

			run := func() { LeanError(w, tt.code) }

			r := require.New(t)
			if tt.wPanic != "" {
				r.PanicsWithValue(tt.wPanic, run)
				return
			}
			r.NotPanics(run)
			r.Empty(w.Body.String())
		})
	}
}

func TestHasHeader(t *testing.T) {
	t.Parallel()
	const key = "X-Foo"
	for n, tt := range map[string]struct {
		want bool
		h    http.Header
	}{
		"nil headers": {want: false},
		"absent":      {false, http.Header{}},
		"nil":         {false, http.Header{key: nil}},
		"no values":   {false, http.Header{key: []string{}}},
		"empty value": {true, http.Header{key: []string{""}}},
		"nominal":     {true, http.Header{key: []string{"value"}}},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := HasHeader(tt.h, key)
			require.Equal(t, tt.want, got)
		})
	}
}
