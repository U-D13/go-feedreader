// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package http

import (
	"fmt"
	"net/http"
)

// Error responds to the request with the specified HTTP status code and
// its corresponding default message.
// For when you have to say something but you've got nothing else to say.
func Error(w http.ResponseWriter, code int) {
	http.Error(w, fmt.Sprintf("%d %s", code, http.StatusText(code)), code)
}

// LeanError responds to the request with the specified HTTP status
// code.
// No additional headers or content is sent, making it an extra lean
// response.
func LeanError(w http.ResponseWriter, code int) { w.WriteHeader(code) }

// HasHeader returns whether h has any values associated with the given
// key.
//
// It is case insensitive; [http.CanonicalMIMEHeaderKey] is used to
// canonicalize the provided key. Get assumes that all keys are stored
// in canonical form.
// To use non-canonical keys, access the map directly.
func HasHeader(h http.Header, key string) bool {
	return len(h.Values(key)) > 0
}
