// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package fs

import (
	"io/fs"
)

// MustSub wraps [fs.Sub] to panic instead of returning error.
//
// This is intended to be used in package variable initializations,
// similar to [template.Must]:
//
//	//go:embed resources/*
//	var resourceFS embed.FS
//	var templates = MustSub(resourceFS, "template")
func MustSub(fsys fs.FS, path string) fs.FS {
	sub, err := fs.Sub(fsys, path)
	if err != nil {
		panic(err)
	}
	return sub
}

// MustSubAll performs [MustSub] on multiple file systems, returning the
// results as a slice.
func MustSubAll(path string, fsys ...fs.FS) []fs.FS {
	s := make([]fs.FS, len(fsys))
	for i, v := range fsys {
		s[i] = MustSub(v, path)
	}
	return s
}

// MustReadFile wraps [fs.ReadFile] to panic instead of returning
// error.
//
// This is intended to be used in package variable initializations,
// similar to [template.Must]:
//
//	//go:embed resources/*
//	var resourceFS embed.FS
//	var indexContents = MustReadFile(resourceFS, "index.html")
func MustReadFile(fsys fs.FS, path string) []byte {
	b, err := fs.ReadFile(fsys, path)
	if err != nil {
		panic(err)
	}
	return b
}
