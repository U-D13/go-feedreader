// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package fs_test

import (
	"io/fs"
	"testing"
	"testing/fstest"

	. "github.com/antichris/go-feedreader/internal/util/fs"
	"github.com/stretchr/testify/require"
)

func TestMustSubAll(t *testing.T) {
	t.Parallel()
	f := []fs.FS{
		fstest.MapFS{
			"foo/bar": &fstest.MapFile{},
		},
	}
	type test struct {
		path   string
		wPanic string
		want   []fs.FS
	}
	for n, tt := range map[string]test{
		"invalid path": {
			path:   "../qux",
			wPanic: "sub ../qux: invalid name",
		},
		"nominal": func() test {
			const p = "foo"
			w, _ := fs.Sub(f[0], p)
			return test{
				path: p,
				want: []fs.FS{w},
			}
		}(),
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got []fs.FS
			run := func() { got = MustSubAll(tt.path, f...) }
			if tt.wPanic != "" {
				require.PanicsWithError(t, tt.wPanic, run)
				return
			}
			require.NotPanics(t, run)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMustReadFile(t *testing.T) {
	t.Parallel()
	b := []byte("foo")
	f := fstest.MapFS{
		"bar": &fstest.MapFile{Data: b},
	}
	for n, tt := range map[string]struct {
		path   string
		wPanic string
		want   []byte
	}{"invalid path": {
		path:   "qux",
		wPanic: "open qux: file does not exist",
	}, "nominal": {
		path: "bar",
		want: b,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			var got []byte
			run := func() { got = MustReadFile(f, tt.path) }
			if tt.wPanic != "" {
				require.PanicsWithError(t, tt.wPanic, run)
				return
			}
			require.NotPanics(t, run)
			require.Equal(t, tt.want, got)
		})
	}
}
