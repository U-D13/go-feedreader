// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package testu

// This file "re-exports" the relevant types from the [mock] package

import "github.com/antichris/go-feedreader/internal/util/testu/mock"

// NewMock instance with the [testing.T] already set.
var NewMock = mock.NewMock

// NewFuncMock instance with the [testing.T] and function name set.
//
//	m := NewFuncMock(t, "myFunc")
var NewFuncMock = mock.NewFuncMock

// FuncMock simplifies using [mock.Mock] to mock functions.
type FuncMock = mock.FuncMock
