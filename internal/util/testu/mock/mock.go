// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package mock implements utilities for mocking with testify [mock].
//
// Fun fact: it has to have this seemingly conflicting name to work
// around how the testify [mock] (or rather, the assert package it
// depends on) does call stack unwinding for message formatting
// (see testify [assert.CallerInfo] for details).
package mock

import (
	"fmt"
	"sync"

	"github.com/stretchr/testify/mock"
)

// NewMock instance with the [testing.T] already set.
func NewMock(t TestingT) *mock.Mock {
	m := &mock.Mock{}
	m.Test(t)
	return m
}

// NewFuncMock instance with the [testing.T] and function name set.
//
//	m := NewFuncMock(t, "myFunc")
func NewFuncMock(t TestingT, name string) *FuncMock {
	m := &FuncMock{FuncName: name}
	m.SetTest(t)
	return m
}

// FuncMock simplifies using [mock.Mock] to mock functions.
type FuncMock struct {
	// FuncName is the name of the function that is being mocked.
	FuncName string // TODO Consider hiding this behind an accessor.
	t        TestingT
	mu       sync.Mutex
	mockMock
}

// AssertCalled asserts that the function was called.
//
// It can produce a false result when an argument is a pointer type and
// the underlying value changed after calling the mocked function.
func (m *FuncMock) AssertCalled(t TestingT, arguments ...any) bool {
	if h, ok := t.(tHelper); ok {
		h.Helper()
	}
	return m.mockMock.AssertCalled(t, m.FuncName, arguments...)
}

// AssertNotCalled asserts that the function was not called.
//
// It can produce a false result when an argument is a pointer type and
// the underlying value changed after calling the mocked function.
func (m *FuncMock) AssertNotCalled(t TestingT, arguments ...any) bool {
	if h, ok := t.(tHelper); ok {
		h.Helper()
	}
	return m.mockMock.AssertNotCalled(t, m.FuncName, arguments...)
}

// AssertNumberOfCalls asserts that the function was called
// expectedCalls times.
func (m *FuncMock) AssertNumberOfCalls(t TestingT, expectedCalls int) bool {
	if h, ok := t.(tHelper); ok {
		h.Helper()
	}
	return m.mockMock.AssertNumberOfCalls(t, m.FuncName, expectedCalls)
}

// IsMethodCallable returns false if the function was called with the
// given arguments more than `Repeatability` times.
//
// The name is awkward, but this is to keep the [FuncMock]'s API highly
// specific by overriding the [mock.Mock]'s API.
func (m *FuncMock) IsMethodCallable(t TestingT, arguments ...any) bool {
	if h, ok := t.(tHelper); ok {
		h.Helper()
	}
	return m.mockMock.IsMethodCallable(t, m.FuncName, arguments...)
}

// String provides a %v format string for Mock.
//
// Note: this is used implicitly by [mock.Arguments.Diff] if a Mock is
// passed.
// It exists because Go's default %v formatting traverses the struct
// without acquiring the mutex, which is detected by go test -race.
func (m *FuncMock) String() string { return fmt.Sprintf("%[1]T<%[1]p>", m) }

// Test is the TestingT instance that has been assigned to this mock
// object.
func (m *FuncMock) Test() TestingT {
	m.mu.Lock()
	defer m.mu.Unlock()
	return m.t
}

// Called tells the mock object that the function has been called, and
// gets a slice of arguments to return.
//
// Panics if the call is unexpected (i.e. not preceded by appropriate
// [FuncMock.On], [FuncMock.Return] calls).
// If [mock.Call.WaitFor] is set, blocks until the channel is closed or
// receives a message.
//
// It is recommended to use this instead of [FuncMock.MethodCalled].
//
// Example:
//
//	myFunc := func(arg1, arg2 any) error {
//		return m.Called(arg1, arg2).Error(0)
//	}
func (m *FuncMock) Called(arguments ...any) mock.Arguments {
	if h, ok := m.t.(tHelper); ok {
		h.Helper()
	}

	return m.mockMock.MethodCalled(m.FuncName, arguments...)
}

// MethodCalled tells the mock object that the function has been called,
// and gets a slice of arguments to return.
//
// See [FuncMock.Called] for more details, since this is just an alias
// for that.
// For the same reason it is recommended also to use that over of this.
//
// The name is awkward, but this is to keep the [FuncMock]'s API highly
// specific by overriding the [mock.Mock]'s API.
func (m *FuncMock) MethodCalled(arguments ...any) mock.Arguments {
	if h, ok := m.t.(tHelper); ok {
		h.Helper()
	}

	return m.Called(arguments...)
}

// On starts a description of an expected call of the function.
//
//	m.On(arg1, arg2)
func (m *FuncMock) On(arguments ...any) *mock.Call {
	return m.mockMock.On(m.FuncName, arguments...)
}

// SetTest struct variable of the mock object.
func (m *FuncMock) SetTest(t TestingT) {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.t = t
	m.mockMock.Test(t)
}

// TestingT is an interface wrapper around [*testing.T]
type TestingT = mock.TestingT

// mockMock is an unexported alias for embedding [mock.Mock].
type mockMock = mock.Mock

type tHelper interface {
	Helper()
}
