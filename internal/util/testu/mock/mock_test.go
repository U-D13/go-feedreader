// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package mock_test

import (
	"fmt"
	"testing"

	. "github.com/antichris/go-feedreader/internal/util/testu/mock"
	"github.com/antichris/go-feedreader/terror"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestNewMock(t *testing.T) {
	t.Parallel()
	const n = "foo"

	tm := &testingT{}
	tm.Test(t)
	tm.On("Errorf",
		"\nassert: mock: I don't know what to return because the method call "+
			"was unexpected.\n\tEither do Mock.On(\"%s\").Return(...) first, "+
			"or remove the %s() call.\n\tThis method was unexpected:\n"+
			"\t\t%s\n\tat: %s",
		mock.MatchedBy(func(s []any) bool {
			return s[0] == n && s[1] == n && s[2] == n+"()\n\t\t"
		}))
	tm.On("FailNow")

	require.PanicsWithValue(t, p{}, func() {
		v := NewMock(tm)
		v.MethodCalled(n)
	})
	tm.AssertExpectations(t)
}

func TestNewFuncMock(t *testing.T) {
	t.Parallel()
	const errSnap = terror.Error("snap")
	wErr := errSnap
	m := NewFuncMock(t, "myFunc")

	myFunc := func(arg1, arg2 any) error {
		// XXX Users should do [Called], but this expands coverage.
		return m.MethodCalled(arg1, arg2).Error(0)
	}
	m.On(1, "a").Return(nil)
	m.On("b", 2).Return(errSnap)

	r := require.New(t)

	r.True(m.AssertNotCalled(m.Test(), 1, "a"))
	r.True(m.IsMethodCallable(m.Test(), 1, "a"))
	r.True(m.IsMethodCallable(m.Test(), "b", 2))
	r.False(m.IsMethodCallable(m.Test(), "c", "c"))

	err := myFunc(1, "a")

	r.NoError(err)
	r.True(m.AssertCalled(m.Test(), 1, "a"))
	r.True(m.AssertNotCalled(m.Test(), "b", 2))
	r.True(m.IsMethodCallable(m.Test(), "b", 2))

	err = myFunc("b", 2)

	r.ErrorIs(err, wErr)
	r.True(m.AssertCalled(m.Test(), "b", 2))
	r.True(m.AssertNumberOfCalls(m.Test(), 2))
	r.False(m.IsMethodCallable(m.Test(), 1, "a"))
	r.False(m.IsMethodCallable(m.Test(), "b", 2))

	r.Equal(fmt.Sprintf("%[1]T<%[1]p>", m), m.String())
}

var _ mock.TestingT = (*testingT)(nil)

type testingT struct{ mock.Mock }

func (t *testingT) Logf(format string, args ...any)   { t.Called(format, args) }
func (t *testingT) Errorf(format string, args ...any) { t.Called(format, args) }
func (t *testingT) FailNow() {
	t.Called()
	panic(p{})
}

type p struct{}
