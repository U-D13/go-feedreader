// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package testu_test

import (
	"testing"

	. "github.com/antichris/go-feedreader/internal/util/testu"
	"github.com/stretchr/testify/require"
)

func TestSameFunc(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		want   bool
		f1, f2 any
	}{
		"both nil":       {false, nil, nil},
		"both not func":  {false, false, false},
		"one nil":        {false, SameFunc, nil},
		"one not a func": {false, SameFunc, false},
		"one nil func":   {false, SameFunc, (func())(nil)},
		"different":      {false, SameFunc, TestSameFunc},
		"different type": {true, SameFunc, Hello(SameFunc)},
		"both nil func":  {true, (func())(nil), (func())(nil)},
		"nominal":        {true, SameFunc, SameFunc},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := SameFunc(tt.f1, tt.f2)
			require.Equal(t, tt.want, got)
		})
	}
}

type Hello func(f1, f2 AnyFunc) (r bool)

func (f Hello) Hello() { print("hello") }
