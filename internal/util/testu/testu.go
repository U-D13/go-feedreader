// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package testu

// SameFunc returns whether f1 is the same function as f2.
//
// False is also returned if either (or both) is not a function.
func SameFunc(f1, f2 AnyFunc) (r bool) {
	if !IsValidFunc(f1) || !IsValidFunc(f2) {
		return
	}
	return eface(f1).word == eface(f2).word
}

// AnyFunc is any function type.
type AnyFunc any

// IsValidFunc returns false if f is not a valid function type.
func IsValidFunc(f AnyFunc) (r bool) {
	e := eface(f)
	if e.typ == nil {
		return
	}
	return e.typ.Kind() == funcKind
}
