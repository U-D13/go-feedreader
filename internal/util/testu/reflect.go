// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package testu

import (
	"reflect"
	"unsafe"
)

func eface(f any) *emptyInterface {
	return (*emptyInterface)(unsafe.Pointer(&f))
}

const funcKind = reflect.Func

// As long as the types below are in sync with [reflect], everything
// should work fine.

// emptyInterface is the header for an interface{} value.
type emptyInterface struct {
	typ  *rtype
	word unsafe.Pointer
}

// rtype is the common implementation of most values.
// It is embedded in other struct types.
//
// rtype must be kept in sync with ../runtime/type.go:/^type._type.
//
// There is no technical reason for the fields to have blank names other
// than to prevent linters from complaining about them being unused.
type rtype struct {
	_    uintptr // size
	_    uintptr // ptrdata     // number of bytes in the type that can contain pointers
	_    uint32  // hash        // hash of type; avoids computation in hash tables
	_    tflag   // tflag       // extra type information flags
	_    uint8   // align       // alignment of variable with this type
	_    uint8   // fieldAlign  // alignment of struct field with this type
	kind uint8   // enumeration for C
	// equal
	// function for comparing objects of this type
	// (ptr to object A, ptr to object B) -> ==?
	_ func(unsafe.Pointer, unsafe.Pointer) bool
	_ *byte   // gcdata    // garbage collection data
	_ nameOff // str       // string form
	_ typeOff // ptrToThis // type for pointer to this type, may be zero
}

func (t *rtype) Kind() reflect.Kind { return reflect.Kind(t.kind & kindMask) }

type (
	// tflag is used by an rtype to signal what extra type information is
	// available in the memory directly following the rtype value.
	//
	// tflag values must be kept in sync with copies in:
	//
	//	cmd/compile/internal/reflectdata/reflect.go
	//	cmd/link/internal/ld/decodesym.go
	//	runtime/type.go
	tflag = uint8

	nameOff = int32 // offset to a name
	typeOff = int32 // offset to an *rtype
)

const kindMask = (1 << 5) - 1
