// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package strings

import (
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

// TrimTrailingSlash, if present, from s.
// If s doesn't end with a slash, it is returned unchanged.
func TrimTrailingSlash(s string) string { return strings.TrimSuffix(s, "/") }

// EnsureSuffix adds trailing slash to s, if it is not already present.
// Otherwise, s is returned unchanged.
func EnsureTrailingSlash(s string) string { return EnsureSuffix(s, "/") }

// EnsureSuffix adds suffix to s, if it is not already present.
// Otherwise, s is returned unchanged.
func EnsureSuffix(s, suffix string) string {
	if strings.HasSuffix(s, suffix) {
		return s
	}
	return s + suffix
}

// SentenceQuoted fuses the powers of [Sentence] and [MapSlice] with
// [strconv.Quote] to turn slices of strings into sentences, where each
// of the elements is quoted.
//
// See the documentation of the respective functions for details.
func SentenceQuoted(elems []string, conj string) string {
	return Sentence(MapSlice(elems, strconv.Quote), conj)
}

// Sentence formed by comma-separating elems up to the last one, which
// is separated by the given conjunction.
//
// If conj is an empty string, it is replaced by a comma (",").
// If conj starts with a unicode letter, it will be separated from the
// previous element by a space.
// If conj does not end with a unicode space, it will be separated from
// the last element by a space.
//
// Example:
//
//	elems := []string{"A rabbi", "a priest", "a minister"}
//	fmt.Printf(
//		"%s walk into a bar.\n%s",
//		strings.Sentence(elems, "and"),
//		"The bartender looks at them and says,\n"+
//			`"What is this, a joke?"`,
//	)
//	// Output:
//	// A rabbi, a priest and a minister walk into a bar.
//	// The bartender looks at them and says,
//	// "What is this, a joke?"
func Sentence(elems []string, conj string) string {
	l := len(elems)
	switch l {
	case 0:
		return ""
	case 1:
		return elems[0]
	}

	var csp, css bool // Whether conjunction requires space prefix/suffix.
	switch {
	case len(conj) == 0:
		conj = ", "
	default:
		r, _ := utf8.DecodeRuneInString(conj)
		csp = unicode.IsLetter(r)
		r, _ = utf8.DecodeLastRuneInString(conj)
		css = !unicode.IsSpace(r)
	}

	l--
	n := len(conj) + 2*(l-1)
	if csp {
		n++
	}
	if css {
		n++
	}
	for _, v := range elems {
		n += len(v)
	}

	var b strings.Builder
	b.Grow(n)
	b.WriteString(elems[0])
	for _, v := range elems[1:l] {
		b.WriteString(", ")
		b.WriteString(v)
	}
	if csp {
		b.WriteByte(' ')
	}
	b.WriteString(conj)
	if css {
		b.WriteByte(' ')
	}
	b.WriteString(elems[l])

	return b.String()
}

// MapSlice of elems using mapping function into a new slice.
func MapSlice(elems []string, mapping func(string) string) []string {
	l := len(elems)
	switch l {
	case 0:
		return nil
	case 1:
		return []string{mapping(elems[0])}
	}
	s := make([]string, l)
	for i, v := range elems {
		// TODO Consider parallelizing.
		s[i] = mapping(v)
	}
	return s
}
