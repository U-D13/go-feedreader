// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package strings_test

import (
	"fmt"
	"testing"

	. "github.com/antichris/go-feedreader/internal/util/strings"
	"github.com/stretchr/testify/require"
)

func TestTrimTrailingSlash(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		s    string
		want string
	}{
		"empty":   {},
		"no-op":   {s, s},
		"nominal": {ss, s},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := TrimTrailingSlash(tt.s)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestEnsureTrailingSlash(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		s    string
		want string
	}{
		"empty":   {"", "/"},
		"no-op":   {ss, ss},
		"nominal": {s, ss},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			got := EnsureTrailingSlash(tt.s)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestSentenceQuoted(t *testing.T) {
	t.Parallel()
	const nbsp = "\u00a0"
	for _, tt := range []struct {
		s    []string
		conj string
		want string
	}{
		{[]string{}, "and", ""},
		{[]string{s}, "and", `"foo"`},
		{[]string{s, s2}, "and", `"foo" and "bar"`},
		{[]string{s, s2, s3}, "", `"foo", "bar", "qux"`},
		{[]string{s, s2, s3}, ";", `"foo", "bar"; "qux"`},
		{[]string{s, s2, s3}, " &", `"foo", "bar" & "qux"`},
		{[]string{s, s2, s3}, ", or", `"foo", "bar", or "qux"`},
		{[]string{s, s2, s3}, nbsp, `"foo", "bar"` + nbsp + `"qux"`},
		{[]string{s, s2, s3}, "and" + nbsp, `"foo", "bar" and` + nbsp + `"qux"`},
	} {
		tt := tt
		t.Run(fmt.Sprintf("s=%q/conj=%q", tt.s, tt.conj), func(t *testing.T) {
			t.Parallel()
			got := SentenceQuoted(tt.s, tt.conj)
			require.Equal(t, tt.want, got)
		})
	}
}

const (
	s, ss      = "foo", s + "/"
	s2, s3, s4 = "bar", "qux", "corge"
)
