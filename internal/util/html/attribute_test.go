// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package html_test

import (
	"strings"
	"testing"

	. "github.com/antichris/go-feedreader/internal/util/html"
	"github.com/stretchr/testify/require"
	"golang.org/x/net/html"
)

func TestGetAttrValue(t *testing.T) {
	t.Parallel()
	node := sampleNode(t)
	for n, tt := range map[string]struct {
		name string
		wV   string
		wOk  bool
	}{
		"absent": {"style", "", false},
		"lang":   {"lang", "en", true},
		"data":   {"data-foo", "bar", true},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			gotV, gotOk := GetAttrValue(node, tt.name)

			r := require.New(t)
			r.Equal(tt.wOk, gotOk)
			r.Equal(tt.wV, gotV)
		})
	}
}

func TestSetAttrValue(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		name string
		v    string
	}{
		"new":  {"style", "color:red"},
		"lang": {"lang", "en-US"},
		"data": {"data-foo", "qux"},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			n := sampleNode(t)

			SetAttrValue(n, tt.name, tt.v)
			gotV, ok := GetAttrValue(n, tt.name)

			r := require.New(t)
			r.True(ok)
			r.Equal(tt.v, gotV)
		})
	}
}

func TestTransformAttrValue(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		name string
		f    func(string) string
		wOk  bool
		wV   string
	}{
		"absent": {name: "style"},
		"lang": {
			name: "lang",
			f:    func(s string) string { return s + "-US" },
			wOk:  true,
			wV:   "en-US",
		},
		"data": {"data-foo", strings.ToUpper, true, "BAR"},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			n := sampleNode(t)

			gotOk := TransformAttrValue(n, tt.name, tt.f)

			r := require.New(t)
			r.Equal(tt.wOk, gotOk)
			if !gotOk {
				return
			}

			gotV, ok := GetAttrValue(n, tt.name)
			r.True(ok)
			r.Equal(tt.wV, gotV)
		})
	}
}

func TestFindAttrIndex(t *testing.T) {
	t.Parallel()
	node := sampleNode(t)

	for n, tt := range map[string]struct {
		name string
		wI   int
		wOk  bool
	}{
		"absent": {"style", 0, false},
		"lang":   {"lang", 0, true},
		"data":   {"data-foo", 1, true},
	} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			gotI, gotOk := FindAttrIndex(node, tt.name)

			r := require.New(t)
			r.Equal(tt.wOk, gotOk)
			r.Equal(tt.wI, gotI)
		})
	}
}

func sampleNode(t *testing.T) *html.Node {
	return parseHTML(t, `<html lang=en data-foo=bar>`)
}
