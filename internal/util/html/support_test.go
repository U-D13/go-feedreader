// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package html_test

import (
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func parseHTML(t *testing.T, s string) *html.Node {
	n, err := html.ParseFragment(strings.NewReader(s), nil)
	if err != nil {
		t.Fatalf("parsing HTML fragment: %v", err)
	}
	return n[0]
}
