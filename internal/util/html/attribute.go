// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package html

import "golang.org/x/net/html"

// GetAttrValue with the given name from the attribute list of n. If no
// attribute with such name is present, ok is false.
func GetAttrValue(n *html.Node, name string) (v string, ok bool) {
	i, ok := FindAttrIndex(n, name)
	if !ok {
		return
	}
	v = n.Attr[i].Val
	return
}

// SetAttrValue with the given name in the attribute list of n. If no
// attribute with such name is present, a new one is appended at the
// end of the attribute list.
func SetAttrValue(n *html.Node, name, v string) {
	i, ok := FindAttrIndex(n, name)
	if !ok {
		i = len(n.Attr)
		n.Attr = append(n.Attr, html.Attribute{Key: name})
	}
	n.Attr[i].Val = v
}

// TransformAttrValue with the given name in the attribute list of n by
// replacing it with what is returned by f; ok is true, if successful,
// false otherwise.
func TransformAttrValue(
	n *html.Node,
	name string,
	f func(string) string,
) (ok bool) {
	return TransformAttr(n, name, func(a html.Attribute) html.Attribute {
		a.Val = f(a.Val)
		return a
	})
}

// TransformAttr with the given name in the attribute list of n by
// replacing it with what is returned by f; ok is true, if successful,
// false otherwise.
func TransformAttr(
	n *html.Node,
	name string,
	f func(html.Attribute) html.Attribute,
) (ok bool) {
	i, ok := FindAttrIndex(n, name)
	if !ok {
		return
	}
	n.Attr[i] = f(n.Attr[i])
	return
}

// FindAttrIndex returns the index of attribute with the given name in
// the attribute list of n; ok is true, if successful, false otherwise.
func FindAttrIndex(n *html.Node, name string) (index int, ok bool) {
	for i := range n.Attr {
		if ok = n.Attr[i].Key == name; ok {
			index = i
			return
		}
	}
	return
}
