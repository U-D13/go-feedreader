// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package html

import (
	"io"

	"golang.org/x/net/html"
)

// ParseFragment parses a fragment of HTML and returns the nodes that
// were found.
//
// It wraps [html.ParseFragment] providing a context node, and has the
// same intricacies as Parse.
//
// TODO Consider removal.
func ParseFragment(r io.Reader) ([]*html.Node, error) {
	root := &html.Node{
		Type: html.ElementNode,
		Data: "root",
	}
	return html.ParseFragment(r, root)
}

// Walk the Node tree executing fn for each one. If fn returns an error,
// that is returned immediately, aborting the walk.
func Walk(n *html.Node, fn func(*html.Node) error) (err error) {
	if err = fn(n); err != nil {
		return
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if err = Walk(c, fn); err != nil {
			return
		}
	}
	return
}
