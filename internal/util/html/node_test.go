// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package html_test

import (
	"bytes"
	"errors"
	"strings"
	"testing"

	. "github.com/antichris/go-feedreader/internal/util/html"
	"github.com/stretchr/testify/require"
	"golang.org/x/net/html"
)

func TestParseFragment(t *testing.T) {
	t.Parallel()
	const (
		foo = "<foo><bar>qux</bar></foo>"
		div = "<div></div>"
	)
	r := strings.NewReader(foo + div)

	s, err := ParseFragment(r)

	rq := require.New(t)
	rq.NoError(err)
	rq.Equal(foo, renderNode(t, s[0]))
	rq.Equal(div, renderNode(t, s[1]))
}

func TestWalk(t *testing.T) {
	t.Parallel()
	const (
		attr      = "foo"
		wantValue = "bar"
		wantHTML  = `<div ` + attr + `="` + wantValue + `"><b></b></div>`
	)
	node := parseHTML(t, "<div foo>"+wantHTML+"</div><div qux></div>")

	var gotN *html.Node
	err := Walk(node, func(n *html.Node) error {
		gotN = n
		if n.Data != "div" {
			return nil
		}
		if v, ok := GetAttrValue(n, attr); ok && v == wantValue {
			return errSnap
		}
		return nil
	})

	r := require.New(t)
	r.ErrorIs(err, errSnap)
	r.Equal(wantHTML, renderNode(t, gotN))
}

var errSnap = errors.New("snap")

func renderNode(t *testing.T, n *html.Node) string {
	b := &bytes.Buffer{}
	err := html.Render(b, n)
	if err != nil {
		t.Fatalf("rendering HTML node: %v", err)
	}
	return b.String()
}
