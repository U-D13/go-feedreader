## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at https://mozilla.org/MPL/2.0/.

toolsGo ?= internal/tools/tools.go
toolsDir ?= bin/tools

toolPkgs := $(shell go list -f '{{join .Imports " "}}' ${toolsGo})
## e.g. bin/tools/stringer
toolCmds := $(foreach tool,$(notdir ${toolPkgs}),${toolsDir}/${tool})
## e.g. stringerCmd := bin/tools/stringer
$(foreach cmd,${toolCmds},$(eval $(notdir ${cmd})Cmd := ${cmd}))

go.mod: ${toolsGo}
	go mod tidy
##	TODO Is the following actually necessary?
	touch go.mod

${toolCmds}: go.mod
	go build \
		-trimpath \
		-ldflags='-s -w' \
		-o \
		$@ $(filter %/$(@F),${toolPkgs})

tools: ${toolCmds}
.PHONY: tools
