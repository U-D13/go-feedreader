// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader_test

import (
	"context"
	"database/sql"
	"io"
	"log"
	"strings"
	"testing"

	. "github.com/antichris/go-feedreader"
	"github.com/antichris/go-feedreader/auth"
	authMocks "github.com/antichris/go-feedreader/auth/mocks"
	"github.com/antichris/go-feedreader/goa/gen/user"
	userPkg "github.com/antichris/go-feedreader/user"
	"github.com/antichris/go-feedreader/user/registration"
	uRegMocks "github.com/antichris/go-feedreader/user/registration/mocks"
	uStoreMocks "github.com/antichris/go-feedreader/user/storage"
	"github.com/antichris/go-feedreader/user/testdata"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_usersrvc_APIKeyAuth(t *testing.T) {
	t.Parallel()
	requireAuthServiceAPIKeyAuthCall(t, func(a auth.Service) Auther {
		return newUsersrvc(nil, a, nil, nil)
	})
}

func Test_usersrvc_IsEmailAvailableForNew(t *testing.T) {
	t.Parallel()
	p := email
	for n, tt := range map[string]struct {
		lookup error
		want   error
	}{"lookup error": {
		lookup: errSnap,
		want:   errSnap,
	}, "conflict": {
		lookup: nil,
		want:   &user.ConflictError{},
	}, "nominal": {
		lookup: userPkg.ErrNotFound,
		want:   nil,
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			svc, us, _ := newMockedUsersrvc(t)
			us.EXPECT().
				GetByEmail(ctx, p).
				Return(userPkg.User{}, tt.lookup).
				AnyTimes()

			got := svc.IsEmailAvailableForNew(ctx, p)

			r := require.New(t)
			if tt.want != nil {
				r.ErrorIs(got, tt.want)
				return
			}
			r.NoError(got)
		})
	}
}

func Test_usersrvc_Create(t *testing.T) {
	t.Parallel()
	var (
		u          = testdata.User
		emailToken = *u.EmailToken
		passHash   = u.PasswordHash

		p = &user.CreateUser{
			FirstName: u.FirstName,
			LastName:  u.LastName,
			Email:     u.Email,
			Password:  "password",
		}
	)
	mockHashPass := func(
		c *gomock.Controller, svc *usersrvc,
		password, passHash string, err error,
	) *gomock.Call {
		a := authMocks.NewMockService(c)
		svc.XsetAuth(a)
		return a.EXPECT().
			HashPassword([]byte(password)).
			Return([]byte(passHash), err)
	}
	mockTokenGen := func(svc *usersrvc, emailToken string, mtgErr error) {
		svc.XsetTokenGen(mailTokenGeneratorFunc(func() (string, error) {
			return emailToken, mtgErr
		}))
	}
	expectCreate := func(
		st *MockStorageTx, p *user.CreateUser,
		passHash, emailToken string, err error,
	) *gomock.Call {
		return st.EXPECT().Create(
			ctx, p.Email, passHash, p.FirstName, p.LastName, emailToken,
		).Return(err)
	}
	mockMail := func(
		c *gomock.Controller, svc *usersrvc,
		u userPkg.User, emailToken string, err error,
	) *gomock.Call {
		m := uRegMocks.NewMockMailer(c)
		svc.XsetMailer(m)
		return m.EXPECT().
			Mail(u.EmailAddr(), u.FirstName, emailToken).
			Return(err)
	}
	type test struct {
		passH error
		token error
		store error
		mail  error
		want  error
	}
	prep := func(t *testing.T, m storageTxMocking, arg any) (v any) {
		tt, ok := arg.(test)
		if !ok {
			t.Fatalf("tt not passed along: want %T got %T", tt, arg)
		}
		svc := newUsersrvcWMockStorage(m.storage)

		mockHashPass(m.ctrl, svc, p.Password, passHash, tt.passH)

		if tt.passH != nil {
			m.calls.withTx.Times(0)
			m.calls.rollback.Times(0)
		}

		mockTokenGen(svc, emailToken, tt.token)
		expectCreate(m.tx, p, passHash, emailToken, tt.store).
			AnyTimes()
		mockMail(m.ctrl, svc, u, emailToken, tt.mail).
			AnyTimes()

		return svc
	}
	exec := func(t *testing.T, v any) (err error) {
		return ensureUsersvc(t, v).
			Create(ctx, p)
	}
	tests := map[string]test{"pass hasher error": {
		passH: errSnap,
		want:  errSnap,
	}, "token gen error": {
		token: errSnap,
		want:  errSnap,
	}, "store error": {
		store: errSnap,
		want:  errSnap,
	}, "mailer error": {
		mail: errSnap,
		want: errSnap,
	}, "nominal": {
		want: nil,
	}}
	for n, tt := range tests {
		t.Run(n, storageTxTestFn(storageTxTest{
			want: tt.want,
		}, prep, exec, ctx, tt))
	}
	testStorageTx(t, prep, exec, ctx, tests["nominal"])
}

func Test_usersrvc_Activate(t *testing.T) {
	t.Parallel()
	var (
		tk = string(tk)
		p  = &user.ActivatePayload{Token: &tk}

		uInactive = userPkg.User{ID: userID, Status: userPkg.Default}
	)
	type test struct {
		u      userPkg.User
		lookup error
		store  error
		want   error
	}
	prep := func(
		t *testing.T,
		m storageTxMocking,
		arg any,
	) (v any) {
		tt, ok := arg.(test)
		if !ok {
			t.Fatalf("tt not passed along: want %T got %T", tt, arg)
		}
		svc := newUsersrvcWMockStorage(m.storage)

		sr := m.storage.EXPECT()
		sr.GetByEmailToken(ctx, *p.Token).
			Return(tt.u, tt.lookup).
			AnyTimes()
		sr.SetStatus(ctx, tt.u.ID, userPkg.Active).
			Return(tt.store).
			AnyTimes()

		return svc
	}
	exec := func(t *testing.T, v any) (err error) {
		return ensureUsersvc(t, v).
			Activate(ctx, p)
	}

	tests := map[string]test{"lookup error": {
		lookup: errSnap,
		want:   errSnap,
	}, "not found": {
		lookup: userPkg.ErrNotFound,
		want:   errUserNotFound,
	}, "store error": {
		u:     uInactive,
		store: errSnap,
		want:  errSnap,
	}, "already active": {
		u:    userPkg.User{ID: userID, Status: userPkg.Active},
		want: nil,
	}, "nominal": {
		u:    uInactive,
		want: nil,
	}}
	for n, tt := range tests {
		t.Run(n, storageTxTestFn(storageTxTest{
			want: tt.want,
		}, prep, exec, ctx, tt))
	}
	tt := tests["nominal"]

	testStorageTx(t, prep, exec, ctx, tt)
}

func Test_usersrvc_Introspect(t *testing.T) {
	t.Parallel()

	var (
		p = &user.IntrospectPayload{}
		u = userPkg.User{ID: userID}

		uidCtx = ctxWithUserID(ctx)
	)
	for n, tt := range map[string]struct {
		ctx     context.Context
		lookup  error
		want    user.ShowUser
		wantErr error
	}{"no user ID": {
		ctx:     ctx,
		wantErr: userInternalErr(auth.ErrNoContextUserID),
	}, "nominal": {
		ctx: uidCtx,
		want: user.ShowUser{
			ID:        userID,
			CreatedAt: "0001-01-01T00:00:00Z",
			UpdatedAt: "0001-01-01T00:00:00Z",
		},
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			svc, us, _ := newMockedUsersrvc(t)
			us.EXPECT().
				GetByID(tt.ctx, u.ID).
				Return(u, tt.lookup).
				AnyTimes()

			got, gotErr := svc.Introspect(tt.ctx, p)

			r := require.New(t)
			if tt.wantErr != nil {
				r.Equal(gotErr, tt.wantErr)
				return
			}
			r.Equal(nil, gotErr)
			r.Equal(&tt.want, got)
		})
	}
}

func Test_usersrvc_Show(t *testing.T) {
	t.Parallel()

	var (
		u = userPkg.User{ID: userID}

		uidCtx = ctxWithUserID(ctx)
	)
	for n, tt := range map[string]struct {
		ctx     context.Context
		p       *user.ShowPayload
		lookup  error
		wLookup userPkg.User
		want    user.ShowUser
		wantErr error
	}{"no user ID": {
		ctx:     ctx,
		wantErr: userInternalErr(auth.ErrNoContextUserID),
	}, "non-self user ID": {
		ctx: uidCtx,
		p:   &user.ShowPayload{ID: userID + 1},
		wantErr: &user.UnauthorizedError{
			Message: "You can only view your own user data",
		},
	}, "lookup error": {
		ctx:     uidCtx,
		p:       &user.ShowPayload{ID: userID},
		lookup:  errSnap,
		wantErr: errSnap,
	}, "not found": {
		ctx:     uidCtx,
		p:       &user.ShowPayload{ID: userID},
		lookup:  userPkg.ErrNotFound,
		wantErr: errUserNotFound,
	}, "nominal": {
		ctx: uidCtx,
		p:   &user.ShowPayload{ID: userID},
		want: user.ShowUser{
			ID:        userID,
			CreatedAt: "0001-01-01T00:00:00Z",
			UpdatedAt: "0001-01-01T00:00:00Z",
		},
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			svc, us, _ := newMockedUsersrvc(t)
			us.EXPECT().
				GetByID(tt.ctx, u.ID).
				Return(u, tt.lookup).
				AnyTimes()

			got, gotErr := svc.Show(tt.ctx, tt.p)

			r := require.New(t)
			if tt.wantErr != nil {
				r.Equal(gotErr, tt.wantErr)
				return
			}
			r.Equal(nil, gotErr)
			r.Equal(&tt.want, got)
		})
	}
}

func Test_userUnauthErr(t *testing.T) {
	t.Parallel()
	want := &user.UnauthorizedError{Message: errSnap.Error()}
	got := userUnauthErr(errSnap)
	require.Equal(t, want, got)
}

func Test_userBadReqErr(t *testing.T) {
	t.Parallel()
	want := &user.BadRequestError{Message: errSnap.Error()}
	got := userBadReqErr(errSnap)
	require.Equal(t, want, got)
}

func Test_userInternalErr(t *testing.T) {
	t.Parallel()
	want := &user.InternalError{Message: errSnap.Error()}
	got := userInternalErr(errSnap)
	require.Equal(t, want, got)
}

func Test_readerMailTokenGen_Generate(t *testing.T) {
	t.Parallel()
	for n, tt := range map[string]struct {
		want string
		wErr error
	}{"error": {
		wErr: io.EOF,
	}, "nominal": {
		want: "12345678911234567892123456789312",
	}} {
		tt := tt
		t.Run(n, func(t *testing.T) {
			t.Parallel()
			g := XnewReaderMailTokenGen(strings.NewReader(tt.want))

			got, err := g.Generate()

			r := require.New(t)
			if tt.wErr != nil {
				r.Equal(err, tt.wErr)
				return
			}
			r.NoError(err)
			r.Equal(tt.want, got)
		})
	}
}

func newUsersrvc(
	l *log.Logger, a auth.Service, us userPkg.TxableStorage,
	m registration.Mailer,
) *usersrvc {
	return NewUser(l, a, us, m).(*usersrvc)
}

func newMockedUsersrvc(t *testing.T) (
	*usersrvc, *MockTxableStorage, *gomock.Controller,
) {
	c := gomock.NewController(t)
	us := mockUserTxableStorage(c)
	svc := newUsersrvcWMockStorage(us)
	return svc, us, c
}

func newUsersrvcWMockStorage(us *MockTxableStorage) *usersrvc {
	w := io.Discard
	// TODO Assert on logger output.
	// w := &strings.Builder{}
	return newUsersrvc(newLogger(w), nil, us, nil)
}

func mockUserTxableStorage(
	c *gomock.Controller,
) *MockTxableStorage {
	return uStoreMocks.NewMockTxableStorage(c)
}

type mailTokenGeneratorFunc func() (string, error)

func (f mailTokenGeneratorFunc) Generate() (string, error) { return f() }

type (
	MockTxableStorage = uStoreMocks.MockTxableStorage
	MockStorageTx     = uStoreMocks.MockStorageTx
)

// testStorageTx verifies proper transaction use.
func testStorageTx(
	t *testing.T,
	prep storageTxPrepFn,
	exec func(t *testing.T, v any) error,
	ctx context.Context,
	arg any,
) {
	t.Run("with Tx", func(t *testing.T) {
		t.Parallel()
		tests := map[string]storageTxTest{"begin Tx error": {
			withTx: errSnap,
			want:   errSnap,
		}, "commit error": {
			commit: errSnap,
			want:   errSnap,
		}, "commit panic": {
			panic:  errSnap,
			wPanic: errSnap.Error(),
		}, "rollback panic": {
			rollback: errSnap,
			wPanic:   errSnap.Error(),
		}, "commit and rollback panic": {
			panic:    errSnap,
			rollback: errSnap,
			wPanic:   "rollback after snap: snap",
		}, "rollback committed": {
			rollback: sql.ErrTxDone,
			want:     nil,
		}, "nominal": {
			want: nil,
		}}
		for n, tt := range tests {
			t.Run(n, storageTxTestFn(tt, prep, exec, ctx, arg))
		}
	})
}

func storageTxTestFn(
	tt storageTxTest,
	prep storageTxPrepFn,
	exec func(t *testing.T, v any) (err error),
	ctx context.Context,
	arg any,
) func(t *testing.T) {
	return func(t *testing.T) {
		t.Parallel()

		c := gomock.NewController(t)
		us := mockUserTxableStorage(c)
		tx := uStoreMocks.NewMockStorageTx(c)

		txr := tx.EXPECT()

		withTx := us.EXPECT().
			WithSQLTx(ctx, nil).
			Return(tx, tt.withTx)
		rollback := txr.
			Rollback().
			Return(tt.rollback)
		commit := txr.
			Commit().
			Return(tt.commit).
			AnyTimes()
		if tt.panic != nil {
			commit.Do(func() {
				panic(tt.panic)
			})
		}
		if tt.withTx != nil {
			commit.Times(0)
			rollback.Times(0)
		}

		v := prep(t, storageTxMocking{c, us, tx, &storageTxMockCalls{
			withTx:   withTx,
			rollback: rollback,
			commit:   commit,
		}}, arg)

		var got error
		run := func() { got = exec(t, v) }

		r := require.New(t)
		if tt.wPanic != "" {
			r.PanicsWithError(tt.wPanic, run)
			return
		}
		r.NotPanics(run)
		if tt.want != nil {
			r.ErrorIs(got, tt.want)
			return
		}
		r.NoError(got)
	}
}

type (
	storageTxTest struct {
		withTx   error
		rollback error
		commit   error
		panic    any
		want     error
		wPanic   string
	}
	storageTxPrepFn func(
		t *testing.T,
		m storageTxMocking,
		arg any,
	) (v any)
	storageTxMocking struct {
		ctrl    *gomock.Controller
		storage *MockTxableStorage
		tx      *MockStorageTx
		calls   *storageTxMockCalls
	}
	storageTxMockCalls struct {
		withTx, rollback, commit *gomock.Call
	}
)

func ensureUsersvc(t *testing.T, v any) *usersrvc {
	svc, ok := v.(*usersrvc)
	if !ok {
		t.Fatalf("user service not passed along: want %T got %T", svc, v)
	}
	return svc
}
