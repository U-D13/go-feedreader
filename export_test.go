// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader

import (
	"io"
	"log"
	"time"

	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/feed"
	"github.com/antichris/go-feedreader/token"
	"github.com/antichris/go-feedreader/user"
	"github.com/antichris/go-feedreader/user/registration"
)

type (
	Xfeedsrvc  = feedsrvc
	Xtokensrvc = tokensrvc
	Xusersrvc  = usersrvc
)

const XfeedURL = feedURL

var (
	XgetFeedURL      = getFeedURL
	XfeedUnauthErr   = feedUnauthErr
	XfeedInternalErr = feedInternalErr

	XtokenInternalErr = tokenInternalErr
	XtokenBadReqErr   = tokenBadReqErr
	XtokenUnauthErr   = tokenUnauthErr
	XerrInvalidCreds  = errInvalidCreds

	XuserInternalErr = userInternalErr
	XuserBadReqErr   = userBadReqErr
	XuserUnauthErr   = userUnauthErr
	XerrUserNotFound = errUserNotFound
)

func (x *feedsrvc) XsetLogger(v *log.Logger) *feedsrvc       { x.logger = v; return x }
func (x *feedsrvc) XsetAuth(v auth.Service) *feedsrvc        { x.auth = v; return x }
func (x *feedsrvc) XsetFeedService(v feed.Service) *feedsrvc { x.s = v; return x }
func (x *feedsrvc) XsetGetURL(v URLSource) *feedsrvc         { x.getURL = v; return x }

func (x *tokensrvc) XsetAuth(v auth.Service) *tokensrvc          { x.auth = v; return x }
func (x *tokensrvc) XsetLogger(v *log.Logger) *tokensrvc         { x.logger = v; return x }
func (x *tokensrvc) XsetTokenService(v token.Service) *tokensrvc { x.s = v; return x }
func (x *tokensrvc) XsetUserStorage(v user.Storage) *tokensrvc   { x.us = v; return x }
func (x *tokensrvc) XsetDelay(v time.Duration) *tokensrvc        { x.delay = v; return x }

func (x *usersrvc) XsetAuth(v auth.Service) *usersrvc          { x.auth = v; return x }
func (x *usersrvc) XsetLogger(v *log.Logger) *usersrvc         { x.logger = v; return x }
func (x *usersrvc) XsetStorage(v user.TxableStorage) *usersrvc { x.sto = v; return x }
func (x *usersrvc) XsetMailer(v registration.Mailer) *usersrvc { x.mailer = v; return x }

func (x *usersrvc) XsetTokenGen(v mailTokenGenerator) *usersrvc { x.mtg = v; return x }

func XnewReaderMailTokenGen(r io.Reader) readerMailTokenGen {
	return readerMailTokenGen{r}
}
