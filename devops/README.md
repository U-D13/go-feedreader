# DevOps resources

The structure of this directory might change in the future if/when support for other tools (such as Kubernetes) is added.

## Docker Compose

The Compose files here can be used as rough guides for rolling your own deployment specs.

The deployment will run three separate service instances of Feed Reader:

|  service | url
|---------:|-------------------------------------------
|     `ui` | <https://feedreader.localhost:4430>
|    `api` | <https://api.feedreader.localhost:4431>
| `static` | <https://static.feedreader.localhost:4432>

An Inbucket instance is included, with the web UI listening at port `9000` (i.e., <http://localhost:9000>).

The [base compose file][base] assumes you have all the necessary certificates in the [project root directory](..). Adjust accordingly.

There is a significant amount of duplication among the `api` service command specs of the various Compose files currently; this should go away once Feed Reader supports configuration via environment variables.

The examples here all assume you're going to run `compose` from this directory.

### SQLite

Apply [`compose.override.yaml`][override] over the [base].

```sh
docker-compose \
	--project-directory .. \
	-f compose.yaml \
	-f compose.override.yaml \
	up
```

As a matter of fact, `docker-compose` does this automatically when invoked in the same directory as the compose files, so the following would suffice:

```sh
docker-compose --project-directory .. up
```

### PostgreSQL

Apply [`compose.db.yaml`][db] and [`compose.postgres.yaml`][pgsql] over the [base], like so:

```sh
docker-compose \
	--project-directory .. \
	-f compose.yaml \
	-f compose.db.yaml \
	-f compose.postgres.yaml \
	up
```

### MySQL

Apply [`compose.db.yaml`][db] and [`compose.mysql.yaml`][mysql] over the [base], like so:

```sh
docker-compose \
	--project-directory .. \
	-f compose.yaml \
	-f compose.db.yaml \
	-f compose.mysql.yaml \
	up
```

### Shell script

In addition to the Compose files, this directory also includes a shell script to simplify managing the deployment: [`compose`](compose).

It passes all the arguments it has been given after it sets the project root directory and a set of Compose files. It also [detects][v-detect] if you have [Compose V2] available, preferring that over the legacy Python script. Before all that, it switches the directory to where it is located, so it can be invoked from anywhere, not just the project root or the `devops` directory.

It defaults to SQLite, but you can pass it `postgres` or `mysql` (or, for short, `pg` or `my`, respectively) as the first argument to use the respective Compose file overrides.

So that

```sh
docker-compose \
	--project-directory .. \
	-f compose.yaml \
	-f compose.db.yaml \
	-f compose.postgres.yaml \
	run --rm migrate help serve api
```

becomes

```sh
./compose pg run --rm migrate help serve api
```

#### Compose version detection

The shell script first checks the `DOCKER_COMPOSE_IS` environment variable. If it is set to `go` or `py`, it will use the corresponding command for Compose. Failing that, it will make the same choice after looking at the contents of `/tmp/docker-compose-is`, unless they are absent. In which case it will try running the `version` subcommand of both versions of Compose. The script will then write `go` or `py` to `/tmp/docker-compose-is` depending on which command succeeded, and will use the respective command from then on. If both attempts fail, it prints a message and returns with an error code.


[base]: compose.yaml
[override]: compose.override.yaml
[db]: compose.db.yaml
[pgsql]: compose.postgres.yaml
[mysql]: compose.mysql.yaml

[v-detect]: #compose-version-detection
	"DevOps resources ❭ Docker Compose ❭ Shell script ❭ Compose version detection"

[Compose V2]: https://docs.docker.com/compose/compose-v2/
	"Compose V2 Overview | Docker Documentation"
