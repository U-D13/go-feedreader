// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package feedreader

import (
	"bufio"
	"context"
	"io/fs"
	"log"
	"strings"

	"github.com/antichris/go-feedreader/auth"
	"github.com/antichris/go-feedreader/feed"
	gfeed "github.com/antichris/go-feedreader/goa/gen/feed"
	"github.com/mmcdole/gofeed"
	"goa.design/goa/v3/security"
)

var _ gfeed.Service = (*feedsrvc)(nil)

// Feed service example implementation.
// The example methods log the requests and return zero values.
type feedsrvc struct {
	logger *log.Logger
	auth   auth.Service
	s      feed.Service
	getURL URLSource
}

// APIKeyAuth implements the authorization logic for service "Feed" for the
// "rJWT" security scheme.
func (s *feedsrvc) APIKeyAuth(
	ctx context.Context, key string, scheme *security.APIKeyScheme,
) (context.Context, error) {
	return s.auth.APIKeyAuth(ctx, key, feedUnauthErr, feedUnauthErr)
}

// Serves feed contents.
func (s *feedsrvc) Index(
	ctx context.Context, p *gfeed.IndexPayload,
) (res []*gfeed.Item, err error) {
	s.logger.Print("feed.Index")

	url := s.getURL(ctx)
	s.logger.Printf("url=%s", url)

	res, err = s.s.Fetch(ctx, url)
	if err != nil {
		s.logger.Print(err)
		err = feedInternalErr(err)
	}
	return
}

type (
	FeedParser interface {
		ParseURLWithContext(
			feedURL string, ctx context.Context,
		) (feed *gofeed.Feed, err error)
	}
	URLSource func(ctx context.Context) string
)

func getFeedURL(fsys fs.FS, fileName string) string {
	f, err := fsys.Open(fileName)
	if err != nil {
		return feedURL
	}
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		t := strings.TrimSpace(s.Text())
		if t == "" {
			continue
		}
		if t[0] != '#' {
			return t
		}
	}
	return feedURL
}

const (
	feedURLFile = "feed-url"
	feedURL     = "https://www.theregister.co.uk/software/headlines.atom"
)

func feedUnauthErr(e error) error {
	return &gfeed.UnauthorizedError{Message: e.Error()}
}

func feedInternalErr(e error) error {
	return &gfeed.InternalError{Message: e.Error()}
}
